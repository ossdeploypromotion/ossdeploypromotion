'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _class, _temp; /**
                    * Created by W540 on 1/16/2017.
                    */

// import Calendar from './calendar'


var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDatepicker = require('react-datepicker');

var _reactDatepicker2 = _interopRequireDefault(_reactDatepicker);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var CustomeDatePicker = (_temp = _class = function (_Component) {
  (0, _inherits3.default)(CustomeDatePicker, _Component);

  function CustomeDatePicker() {
    (0, _classCallCheck3.default)(this, CustomeDatePicker);

    var _this = (0, _possibleConstructorReturn3.default)(this, (CustomeDatePicker.__proto__ || Object.getPrototypeOf(CustomeDatePicker)).call(this));

    _this.handleChange = _this.handleChange.bind(_this);
    return _this;
  }

  (0, _createClass3.default)(CustomeDatePicker, [{
    key: 'handleChange',
    value: function handleChange(date) {
      if (this.props.onChange) {
        this.props.onChange(date.toISOString());
      }
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        { className: 'input-group' },
        _react2.default.createElement(_reactDatepicker2.default, (0, _extends3.default)({
          placeholder: 'Nh\u1EADp ng\xE0y',
          className: 'form-control',
          dateFormat: 'DD/MM/YYYY',
          locale: 'vi',
          selected: (0, _moment2.default)(this.props.value),
          onChange: this.props.onChange,
          highlightDates: this.props.highlightDates
        }, this.props)),
        _react2.default.createElement(
          'div',
          { className: 'input-icon' },
          _react2.default.createElement('i', { className: 'fa fa-calendar' })
        )
      );
    }
  }]);
  return CustomeDatePicker;
}(_react.Component), _class.propTypes = {
  placeholder: _react.PropTypes.string
}, _temp);
exports.default = CustomeDatePicker;