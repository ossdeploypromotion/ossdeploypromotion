'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Popover = require('material-ui/Popover');

var _Popover2 = _interopRequireDefault(_Popover);

var _List = require('material-ui/List');

var _List2 = _interopRequireDefault(_List);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var CompanyToggle = function (_Component) {
  (0, _inherits3.default)(CompanyToggle, _Component);

  function CompanyToggle(props) {
    (0, _classCallCheck3.default)(this, CompanyToggle);

    var _this = (0, _possibleConstructorReturn3.default)(this, (CompanyToggle.__proto__ || Object.getPrototypeOf(CompanyToggle)).call(this, props));

    _this.handleTouchTap = function (event) {
      // This prevents ghost click.
      event.preventDefault();

      _this.setState({
        open: true,
        anchorEl: event.currentTarget
      });
    };

    _this.handleRequestClose = function () {
      _this.setState({
        open: false
      });
    };

    _this.state = {
      open: false
    };
    return _this;
  }

  (0, _createClass3.default)(CompanyToggle, [{
    key: 'render',
    value: function render() {
      var _this2 = this;

      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(
          'a',
          { className: 'company-toggle', onClick: this.handleTouchTap },
          _react2.default.createElement('i', { className: 'fa fa-angle-down' })
        ),
        _react2.default.createElement(
          _Popover2.default,
          {
            className: 'popoverCompany',
            open: this.state.open,
            anchorEl: this.state.anchorEl,
            anchorOrigin: { horizontal: 'right', vertical: 'bottom' },
            targetOrigin: { horizontal: 'right', vertical: 'top' },
            onRequestClose: this.handleRequestClose,
            animation: _Popover.PopoverAnimationVertical
          },
          _react2.default.createElement(
            _List2.default,
            null,
            this.props.listCompany.map(function (c) {
              return _react2.default.createElement(_List.ListItem, { primaryText: c.name, value: c.id, style: { fontSize: '14px', color: _this2.props.companyId == c.id ? '#03A9F4' : 'black' }, hoverColor: '#E1F5FE',
                onTouchTap: function onTouchTap() {
                  _this2.handleRequestClose();
                  _this2.props.changeCompany(c.id);
                } });
            })
          )
        )
      );
    }
  }]);
  return CompanyToggle;
}(_react.Component);

exports.default = CompanyToggle;