'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _config = require('config');

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var DropdownMenu = function (_Component) {
  (0, _inherits3.default)(DropdownMenu, _Component);

  function DropdownMenu() {
    (0, _classCallCheck3.default)(this, DropdownMenu);
    return (0, _possibleConstructorReturn3.default)(this, (DropdownMenu.__proto__ || Object.getPrototypeOf(DropdownMenu)).apply(this, arguments));
  }

  (0, _createClass3.default)(DropdownMenu, [{
    key: 'render',
    value: function render() {
      var listMenu = this.props.listMenu;

      var menuClass = "dropdown-wrapper";
      var listMenuHtml = [];
      listMenu.forEach(function (menu, index) {
        listMenuHtml.push(_react2.default.createElement(
          'div',
          { className: 'menu' },
          _react2.default.createElement('div', { className: 'barLeft' }),
          _react2.default.createElement(
            'span',
            { className: 'menu-icon' },
            index
          ),
          _react2.default.createElement(
            'span',
            { className: 'menu-title' },
            menu.title
          ),
          _react2.default.createElement(SubMenu, { menu: menu })
        ));
      });
      return _react2.default.createElement(
        'div',
        { className: menuClass },
        _react2.default.createElement(
          'div',
          { className: 'parentMenu' },
          listMenuHtml
        )
      );
    }
  }]);
  return DropdownMenu;
}(_react.Component);

var SubMenu = function SubMenu(_ref) {
  var menu = _ref.menu;

  var imageSplit = menu.imageBackground && menu.imageBackground.split("/");
  return _react2.default.createElement(
    'div',
    { className: 'childMenu' },
    _react2.default.createElement(
      'div',
      { className: 'child-wrapper', style: imageSplit && imageSplit.length > 1 && {
          background: 'url(' + (_config2.default.apiEndPoint + imageSplit[0] + "/" + encodeURIComponent(imageSplit[1])) + ')',
          backgroundSize: "100% 100%"
        } },
      _react2.default.createElement(
        'div',
        { className: 'row' },
        _react2.default.createElement(
          'div',
          { className: 'col-md-6' },
          menu.subMenu && menu.subMenu.map(function (sub) {
            return _react2.default.createElement(
              'div',
              { className: 'sub-menu' },
              _react2.default.createElement(
                'a',
                { className: 'subMenuLink', href: sub.url },
                sub.title
              )
            );
          })
        ),
        _react2.default.createElement(
          'div',
          { className: 'col-md-6' },
          menu.listGroup && menu.listGroup.map(function (group) {
            return _react2.default.createElement(
              'div',
              { className: 'group-wrapper' },
              _react2.default.createElement(
                'div',
                { className: 'group-title' },
                group.name
              ),
              group.listChild && group.listChild.map(function (sub) {
                return _react2.default.createElement(
                  'div',
                  { className: 'child-group' },
                  _react2.default.createElement(
                    'a',
                    { className: 'subMenuLink', href: sub.url },
                    sub.title
                  )
                );
              })
            );
          })
        )
      )
    )
  );
};

exports.default = DropdownMenu;