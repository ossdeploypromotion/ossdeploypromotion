'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _config = require('config');

var _config2 = _interopRequireDefault(_config);

var _Select = require('./ReactSelectMenuCustom/Select');

var _Select2 = _interopRequireDefault(_Select);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MENUTYPE = 1;
var MAILTYPE = 2;
var USERTYPE = 3;

var DropdownSearch = function (_Component) {
  (0, _inherits3.default)(DropdownSearch, _Component);

  function DropdownSearch() {
    (0, _classCallCheck3.default)(this, DropdownSearch);
    return (0, _possibleConstructorReturn3.default)(this, (DropdownSearch.__proto__ || Object.getPrototypeOf(DropdownSearch)).apply(this, arguments));
  }

  (0, _createClass3.default)(DropdownSearch, [{
    key: 'onChange',
    value: function onChange(option) {
      if (option) window.location.href = option.value;
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var options = [];
      if (this.props.searchResult) {
        var _props$searchResult = this.props.searchResult,
            listMenu = _props$searchResult.listMenu,
            listEmail = _props$searchResult.listEmail,
            listUser = _props$searchResult.listUser;

        options = [{
          label: 'TÍNH NĂNG',
          options: listMenu.map(function (menu, index) {
            return {
              label: menu.title,
              value: menu.url,
              iconType: MENUTYPE
            };
          })
        }, {
          label: 'MAIL',
          options: listEmail.map(function (mail, index) {
            return {
              label: mail.title,
              value: _this2.props.ossServerDomain + _this2.props.mailLink + mail.id,
              iconType: MAILTYPE
            };
          })
        }, {
          label: 'NHÂN VIÊN',
          options: listUser.map(function (user, index) {
            return {
              label: user.fullname + ' - ' + user.username,
              value: _this2.props.ossServerDomain + _this2.props.employeeLink + user.username,
              iconType: USERTYPE
            };
          })
        }];
      }
      var renderer = function renderer(obj) {
        if (obj.options && obj.options.length > 0) return _react2.default.createElement(
          'span',
          { style: { fontWeight: 'normal' } },
          obj.label
        );
        var icon = '';
        switch (obj.iconType) {
          case MENUTYPE:
            icon = 'fa-cog';
            break;
          case MAILTYPE:
            icon = 'fa-envelope';
            break;
          case USERTYPE:
            icon = 'fa-user-circle';
            break;
        }
        return _react2.default.createElement(
          'div',
          { className: 'search-result-option' },
          _react2.default.createElement('i', { className: 'fa ' + icon }),
          '\xA0\xA0',
          obj.label
        );
      };
      return _react2.default.createElement(_Select2.default, {
        onChange: this.onChange,
        onInputChange: this.props.searchValue,
        options: options,
        backspaceRemoves: false,
        autosize: false,
        optionRenderer: renderer,
        placeholder: '',
        isLoading: this.props.isLoading,
        openOnFocus: true,
        autofocus: true,
        onBlur: function onBlur() {
          return _this2.props.showSearch(false);
        }
      });
    }
  }]);
  return DropdownSearch;
}(_react.Component);

exports.default = DropdownSearch;