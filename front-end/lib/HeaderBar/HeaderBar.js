'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.BreadcrumbHeader = exports.createHeaderBar = exports.default = undefined;

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _DropdownMenu = require('./DropdownMenu');

var _DropdownMenu2 = _interopRequireDefault(_DropdownMenu);

var _DropdownSearch = require('./DropdownSearch');

var _DropdownSearch2 = _interopRequireDefault(_DropdownSearch);

var _CompanyToggle = require('./CompanyToggle');

var _CompanyToggle2 = _interopRequireDefault(_CompanyToggle);

var _Popover = require('material-ui/Popover');

var _Popover2 = _interopRequireDefault(_Popover);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var HeaderBar = function (_Component) {
  (0, _inherits3.default)(HeaderBar, _Component);

  function HeaderBar(props) {
    (0, _classCallCheck3.default)(this, HeaderBar);

    var _this = (0, _possibleConstructorReturn3.default)(this, (HeaderBar.__proto__ || Object.getPrototypeOf(HeaderBar)).call(this, props));

    _this.handleTouchTap = function (event) {
      // This prevents ghost click.
      event.preventDefault();
      _this.setState({
        openUserInfo: true,
        anchorEl: event.currentTarget
      });
    };

    _this.handleRequestClose = function () {
      _this.setState({
        openUserInfo: false
      });
    };

    _this.state = {
      isLoading: false,
      showSearch: false
    };
    _this.searchValue = _this.searchValue.bind(_this);
    _this.renderHeaderBar = _this.renderHeaderBar.bind(_this);
    _this.showSearch = _this.showSearch.bind(_this);
    return _this;
  }

  (0, _createClass3.default)(HeaderBar, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      var _this2 = this;

      updateHeaderBreadCrumb = this.renderHeaderBar;
      this.props.client.get(this.props.apiEndPoint + 'leftmenu').then(function (response) {
        return _this2.setState({ listMenu: response.body });
      }).catch(function (err) {
        console.error(err);
      });
      this.props.client.get(this.props.apiEndPoint + 'api/users/listcompany').then(function (response) {
        return _this2.setState({ listCompany: response.body });
      }).catch(function (err) {
        console.error(err);
      });
      this.props.client.get(this.props.apiEndPoint + 'api/authenticated-user').then(function (response) {
        return _this2.setState({ userInfo: response.body });
      }).catch(function (err) {
        console.error(err);
      });
      this.props.client.get(this.props.apiEndPoint + 'gettotalnewmail').then(function (response) {
        return _this2.setState({ totalNewMail: response.body });
      }).catch(function (err) {
        console.error(err);
      });
    }
  }, {
    key: 'renderHeaderBar',
    value: function renderHeaderBar(listBreadCrumb) {
      this.setState({ breadCrumbList: listBreadCrumb });
    }
  }, {
    key: 'searchValue',
    value: function searchValue(value) {
      var _this3 = this;

      this.setState({ valueSearch: value });
      clearTimeout(this.timeOut);
      this.timeOut = setTimeout(function () {
        _this3.setState({ isLoading: true });
        _this3.props.client.get(_this3.props.apiEndPoint + 'api/searchAll', { params: { searchVal: _this3.state.valueSearch } }).then(function (response) {
          _this3.setState({ searchResult: response.body });
          _this3.setState({ isLoading: false });
        }).catch(function (err) {
          console.err(err);
          _this3.setState({ isLoading: false });
        });
      }, 500);
    }
  }, {
    key: 'showSearch',
    value: function showSearch(isShow) {
      this.setState({ showSearch: isShow });
    }
  }, {
    key: 'render',
    value: function render() {
      var _this4 = this;

      var userInfo = this.state.userInfo && this.state.userInfo.userInfo;
      //const {logo, companyId} = userInfo && userInfo.company && {logo : userInfo.company.logo,companyId : userInfo.company.id};
      var logo = userInfo && userInfo.company && userInfo.company.logo;
      var companyId = userInfo && userInfo.company && userInfo.company.id;
      return _react2.default.createElement(
        'div',
        { className: 'header-menu' },
        _react2.default.createElement(
          'div',
          { className: 'container' },
          _react2.default.createElement(
            'ul',
            { className: 'menu-list' },
            _react2.default.createElement(
              'li',
              { className: 'menuShow' },
              _react2.default.createElement(
                'div',
                { className: 'hoverMenuArea' },
                _react2.default.createElement(
                  'div',
                  { className: 'hambuger-icon' },
                  this.state.listMenu && _react2.default.createElement(_DropdownMenu2.default, { listMenu: this.state.listMenu }),
                  _react2.default.createElement(
                    'i',
                    { className: 'material-icons menu-icon' },
                    'menu'
                  )
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'company-logo' },
                  _react2.default.createElement('img', { src: logo ? logo : require('styles/img/tchlogo.jpg') })
                )
              ),
              this.state.listCompany && _react2.default.createElement(_CompanyToggle2.default, { listCompany: this.state.listCompany, changeCompany: this.props.changeCompany, companyId: companyId })
            ),
            _react2.default.createElement(
              'li',
              { className: 'extra-action' },
              _react2.default.createElement(
                'ul',
                { className: 'extra-action' },
                _react2.default.createElement(
                  'li',
                  { className: 'extend-able' },
                  this.state.showSearch == true ? _react2.default.createElement(
                    'div',
                    { className: 'search-bar' },
                    _react2.default.createElement(
                      'i',
                      { className: 'material-icons search-bar-icon' },
                      'search'
                    ),
                    _react2.default.createElement(
                      'div',
                      { className: 'search-result-spotlight' },
                      _react2.default.createElement(_DropdownSearch2.default, { searchResult: this.state.searchResult, searchValue: this.searchValue,
                        isLoading: this.state.isLoading,
                        ossServerDomain: this.props.ossServerDomain, mailLink: this.props.mailLink,
                        employeeLink: this.props.employeeLink, showSearch: this.showSearch })
                    )
                  ) : _react2.default.createElement(
                    'div',
                    { className: 'breabCrumbContainer' },
                    this.state.breadCrumbList && _react2.default.createElement(
                      'ul',
                      { className: 'breadCrumbList' },
                      this.state.breadCrumbList.map(function (b) {
                        var result = [];
                        //result.push(<li className="breadCrumbNext"></li>)
                        result.push(_react2.default.createElement(
                          'li',
                          { className: 'breadCrumbEach' },
                          _react2.default.createElement(
                            'a',
                            { href: b.url },
                            b.title
                          )
                        ));
                        return result;
                      })
                    )
                  )
                ),
                _react2.default.createElement(
                  'li',
                  { className: '' },
                  _react2.default.createElement(
                    'ul',
                    { className: 'right-action' },
                    _react2.default.createElement(
                      'li',
                      { style: { display: this.state.showSearch == true && 'none' } },
                      _react2.default.createElement(
                        'a',
                        { href: 'javascript:void(0)', onClick: function onClick() {
                            return _this4.showSearch(true);
                          } },
                        _react2.default.createElement(
                          'i',
                          { className: 'material-icons hearder-icon' },
                          'search'
                        )
                      )
                    ),
                    _react2.default.createElement(
                      'li',
                      { className: 'dropdown-action' },
                      _react2.default.createElement(
                        'a',
                        { href: this.props.ossServerDomain + this.props.inboxLink },
                        _react2.default.createElement(
                          'i',
                          { className: 'material-icons hearder-icon' },
                          'email'
                        ),
                        this.state.totalNewMail && this.state.totalNewMail > 0 ? _react2.default.createElement(
                          'span',
                          { className: 'label label-noti c-notify-count-email' },
                          this.state.totalNewMail
                        ) : ""
                      )
                    ),
                    _react2.default.createElement(
                      'li',
                      { className: 'dropdown-action' },
                      _react2.default.createElement(
                        'a',
                        null,
                        _react2.default.createElement(
                          'i',
                          { className: 'material-icons hearder-icon' },
                          'notifications'
                        )
                      )
                    ),
                    _react2.default.createElement(
                      'li',
                      null,
                      _react2.default.createElement(
                        'a',
                        {
                          href: userInfo ? this.props.ossServerDomain + this.props.employeeLink + userInfo.username : '#' },
                        _react2.default.createElement('img', { className: 'user-avatar',
                          src: userInfo && userInfo.photo })
                      )
                    )
                  )
                )
              )
            )
          )
        )
      );
    }
  }]);
  return HeaderBar;
}(_react.Component);

exports.default = HeaderBar;


var updateHeaderBreadCrumb = function updateHeaderBreadCrumb(listBreadCrumb) {
  HeaderBarInstant.renderHeaderBar(listBreadCrumb);
};
var HeaderBarInstant = void 0;
var createHeaderBar = exports.createHeaderBar = function createHeaderBar(props) {
  if (!HeaderBarInstant) {
    HeaderBarInstant = _react2.default.createElement(HeaderBar, props);
  }
  return HeaderBarInstant;
};

var BreadcrumbHeader = exports.BreadcrumbHeader = function (_Component2) {
  (0, _inherits3.default)(BreadcrumbHeader, _Component2);

  function BreadcrumbHeader() {
    (0, _classCallCheck3.default)(this, BreadcrumbHeader);
    return (0, _possibleConstructorReturn3.default)(this, (BreadcrumbHeader.__proto__ || Object.getPrototypeOf(BreadcrumbHeader)).apply(this, arguments));
  }

  (0, _createClass3.default)(BreadcrumbHeader, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      var listBreadCrumb = this.props.listBreadCrumb;
      updateHeaderBreadCrumb(listBreadCrumb);
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement('div', null);
    }
  }]);
  return BreadcrumbHeader;
}(_react.Component);

BreadcrumbHeader.propTypes = { listBreadCrumb: _propTypes2.default.array.isRequired };