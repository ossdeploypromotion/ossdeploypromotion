'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _MuiThemeProvider = require('material-ui/styles/MuiThemeProvider');

var _MuiThemeProvider2 = _interopRequireDefault(_MuiThemeProvider);

var _index = require('../index');

var _HeaderBar = require('../HeaderBar/HeaderBar');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MasterLayout = function (_Component) {
  (0, _inherits3.default)(MasterLayout, _Component);

  function MasterLayout() {
    (0, _classCallCheck3.default)(this, MasterLayout);
    return (0, _possibleConstructorReturn3.default)(this, (MasterLayout.__proto__ || Object.getPrototypeOf(MasterLayout)).apply(this, arguments));
  }

  (0, _createClass3.default)(MasterLayout, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        _MuiThemeProvider2.default,
        null,
        _react2.default.createElement(
          'div',
          { className: 'wrapper' },
          (0, _HeaderBar.createHeaderBar)({
            client: this.props.getClient(),
            apiEndPoint: this.props.apiEndPoint,
            changeCompany: this.props.changeCompany,
            breadCrumbList: this.props.breadCrumbList,
            ossServerDomain: this.props.ossServerDomain,
            mailLink: 'message/readmailjson?id=',
            employeeLink: 'employee/publicview?employeeid=',
            inboxLink: 'message/mailjson'
          }),
          _react2.default.createElement(
            'div',
            { className: 'container' },
            _react2.default.createElement(
              'div',
              { className: 'bellow-wrapper' },
              this.props.children
            )
          ),
          _react2.default.createElement(_index.ToastContainer, null)
        )
      );
    }
  }]);
  return MasterLayout;
}(_react.Component); /**
                      * Created by W540 on 6/7/2017.
                      */


exports.default = MasterLayout;

MasterLayout.propTypes = {
  getClient: _propTypes2.default.func.isRequired,
  apiEndPoint: _propTypes2.default.string.isRequired,
  changeCompany: _propTypes2.default.string.isRequired
};