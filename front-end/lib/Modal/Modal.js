'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.default = undefined;

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactModal = require('react-modal');

var _reactModal2 = _interopRequireDefault(_reactModal);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ModalReact = function (_Component) {
	(0, _inherits3.default)(ModalReact, _Component);

	function ModalReact() {
		(0, _classCallCheck3.default)(this, ModalReact);
		return (0, _possibleConstructorReturn3.default)(this, (ModalReact.__proto__ || Object.getPrototypeOf(ModalReact)).apply(this, arguments));
	}

	(0, _createClass3.default)(ModalReact, [{
		key: 'render',
		value: function render() {
			return _react2.default.createElement(_reactModal2.default, null);
		}
	}]);
	return ModalReact;
}(_react.Component);

exports.default = ModalReact;