'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _toConsumableArray2 = require('babel-runtime/helpers/toConsumableArray');

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _defineProperty2 = require('babel-runtime/helpers/defineProperty');

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _components = require('components');

var _reactBootstrap = require('react-bootstrap');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var CheckSelect = function (_Component) {
  (0, _inherits3.default)(CheckSelect, _Component);

  function CheckSelect() {
    (0, _classCallCheck3.default)(this, CheckSelect);

    var _this = (0, _possibleConstructorReturn3.default)(this, (CheckSelect.__proto__ || Object.getPrototypeOf(CheckSelect)).call(this));

    _this.renderOption = function (option) {
      var valueMap = (_this.props.value || []).reduce(function (result, item) {
        return Object.assign({}, result, (0, _defineProperty3.default)({}, item.value, item));
      }, {});
      if (option.value == 'all') {
        return _react2.default.createElement(
          'div',
          { className: 'flex-box' },
          _react2.default.createElement(
            'div',
            null,
            option.label
          )
        );
      }
      return _react2.default.createElement(
        'div',
        { className: 'flex-box space-between' },
        _react2.default.createElement(
          'div',
          null,
          option.label
        ),
        _react2.default.createElement(
          'div',
          null,
          valueMap[option.value] ? _react2.default.createElement('i', { className: 'fa fa-check' }) : _react2.default.createElement('span', null)
        )
      );
    };

    _this.onChange = function (option) {
      console.log(option);
      if (option.key == 'all') {} else {
        _this.props.onChange && _this.props.onChange(option);
      }
    };

    _this.onToggle = function (open) {
      if (_this._inputWasClicked) {
        _this._inputWasClicked = false;
        return;
      }
      _this.setState({ open: open });
    };

    _this.inputWasClicked = function () {
      _this._inputWasClicked = true;
    };

    _this.setValue = function (e) {
      var value = e.target.value;

      _this.setState({ value: value });
    };

    _this.onRowClick = function (item, valueMap) {
      if (valueMap[item.value]) {
        var newOp = Object.values(valueMap);
        newOp.splice(newOp.indexOf(valueMap[item.value]), 1);
        _this.props.onChange(newOp);
      } else {
        _this.props.onChange([].concat((0, _toConsumableArray3.default)(_this.props.value || []), [item.value]));
      }
    };

    _this.checkAll = function (valueMap) {
      if ((_this.props.value || []).length == _this.props.options.length) {
        _this.props.onChange([]);
      } else {
        _this.props.onChange(_this.props.options.map(function (item) {
          return item.value;
        }));
      }
    };

    _this.state = {};
    return _this;
  }
  // renderValue = (values) => {
  //   const status = (this.props.value || []);
  //   return(<div>Có {status.length} trạng thái</div>)
  // }


  (0, _createClass3.default)(CheckSelect, [{
    key: 'render',
    value: function render() {
      var _this2 = this;

      var valueMap = (this.props.value || []).reduce(function (result, item) {
        return Object.assign({}, result, (0, _defineProperty3.default)({}, item, item));
      }, {});
      var total = (this.props.value || []).length;
      return _react2.default.createElement(
        _reactBootstrap.DropdownButton,
        { className: 'check-selected', style: this.props.style, open: this.state.open, onToggle: this.onToggle, bsStyle: 'default',
          title: total + ' Tr\u1EA1ng th\xE1i l\u1ECDc', id: 'dropdown-basic-sale-status', enforceFocus: false },
        _react2.default.createElement(
          'div',
          { className: 'check-select-wrap', style: this.props.style },
          _react2.default.createElement(
            'div',
            null,
            _react2.default.createElement('input', { className: 'action-control', placeholder: 'T\xECm',
              onSelect: this.inputWasClicked,
              onChange: this.setValue
            }),
            _react2.default.createElement(
              'div',
              { className: 'option-select', onClick: function onClick() {
                  _this2.checkAll(valueMap);
                } },
              'Ch\u1ECDn t\u1EA5t c\u1EA3'
            ),
            (this.props.options || []).map(function (item) {
              return _react2.default.createElement(
                'div',
                { className: 'option-select', onClick: function onClick() {
                    _this2.onRowClick(item, valueMap);
                  } },
                _react2.default.createElement(
                  'div',
                  { className: 'flex-box space-between' },
                  _react2.default.createElement(
                    'div',
                    null,
                    item.label
                  ),
                  _react2.default.createElement(
                    'div',
                    null,
                    valueMap[item.value] ? _react2.default.createElement('i', { className: 'fa fa-check' }) : _react2.default.createElement('span', null)
                  )
                )
              );
            })
          )
        )
      );
    }
  }]);
  return CheckSelect;
}(_react.Component); /**
                      * Created by W540 on 5/11/2017.
                      */


exports.default = CheckSelect;