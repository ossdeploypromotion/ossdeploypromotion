'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactSelect = require('react-select');

var _reactSelect2 = _interopRequireDefault(_reactSelect);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MetaSelect = function (_Component) {
  (0, _inherits3.default)(MetaSelect, _Component);

  function MetaSelect() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, MetaSelect);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = MetaSelect.__proto__ || Object.getPrototypeOf(MetaSelect)).call.apply(_ref, [this].concat(args))), _this), _this.renderOption = function (option) {
      return _react2.default.createElement(
        'div',
        { className: 'd-flex' },
        _react2.default.createElement(
          'div',
          { className: 'p-1' },
          _react2.default.createElement('img', { className: 'select-info-image', src: option.image })
        ),
        _react2.default.createElement(
          'div',
          { className: 'flex-1' },
          _react2.default.createElement(
            'div',
            { className: 'p-1' },
            option.label
          ),
          _react2.default.createElement(
            'div',
            { className: 'p-1' },
            option.meta
          )
        )
      );
    }, _this.renderValue = function (option) {
      return _react2.default.createElement(
        'div',
        { className: 'd-flex' },
        _react2.default.createElement(
          'div',
          { className: 'flex-1' },
          option.label
        )
      );
    }, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(MetaSelect, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(_reactSelect2.default, (0, _extends3.default)({
        optionRenderer: this.renderOption,
        valueRenderer: this.renderValue
      }, this.props));
    }
  }]);
  return MetaSelect;
}(_react.Component);

exports.default = MetaSelect;