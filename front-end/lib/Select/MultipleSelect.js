'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactSelect = require('react-select');

var _reactSelect2 = _interopRequireDefault(_reactSelect);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MultipleSelect = function (_Component) {
  (0, _inherits3.default)(MultipleSelect, _Component);

  function MultipleSelect() {
    (0, _classCallCheck3.default)(this, MultipleSelect);
    return (0, _possibleConstructorReturn3.default)(this, (MultipleSelect.__proto__ || Object.getPrototypeOf(MultipleSelect)).apply(this, arguments));
  }

  (0, _createClass3.default)(MultipleSelect, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(_reactSelect2.default, (0, _extends3.default)({ className: 'oss-select' }, this.props, { multi: true }));
    }
  }]);
  return MultipleSelect;
}(_react.Component);

exports.default = MultipleSelect;