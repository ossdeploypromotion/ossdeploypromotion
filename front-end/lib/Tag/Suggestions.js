"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _classCallCheck2 = require("babel-runtime/helpers/classCallCheck");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _possibleConstructorReturn2 = require("babel-runtime/helpers/possibleConstructorReturn");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require("babel-runtime/helpers/inherits");

var _inherits3 = _interopRequireDefault(_inherits2);

var _class, _temp2;

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = require("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _isEqual = require("lodash/isEqual");

var _isEqual2 = _interopRequireDefault(_isEqual);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var maybeScrollSuggestionIntoView = function maybeScrollSuggestionIntoView(suggestionEl, suggestionsContainer) {
  var containerHeight = suggestionsContainer.offsetHeight;
  var suggestionHeight = suggestionEl.offsetHeight;
  var relativeSuggestionTop = suggestionEl.offsetTop - suggestionsContainer.scrollTop;

  if (relativeSuggestionTop + suggestionHeight >= containerHeight) {
    suggestionsContainer.scrollTop += relativeSuggestionTop - containerHeight + suggestionHeight;
  } else if (relativeSuggestionTop < 0) {
    suggestionsContainer.scrollTop += relativeSuggestionTop;
  }
};

var Suggestions = (_temp2 = _class = function (_Component) {
  (0, _inherits3.default)(Suggestions, _Component);

  function Suggestions() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, Suggestions);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = Suggestions.__proto__ || Object.getPrototypeOf(Suggestions)).call.apply(_ref, [this].concat(args))), _this), _this.shouldComponentUpdate = function (nextProps) {
      var _this2 = _this,
          props = _this2.props;

      var shouldRenderSuggestions = props.shouldRenderSuggestions || _this.shouldRenderSuggestions;
      return !(0, _isEqual2.default)(props.suggestions, nextProps.suggestions) || shouldRenderSuggestions(nextProps.query) || shouldRenderSuggestions(nextProps.query) != shouldRenderSuggestions(props.query);
    }, _this.componentDidUpdate = function (prevProps) {
      var suggestionsContainer = _this.refs.suggestionsContainer;
      var _this$props = _this.props,
          selectedIndex = _this$props.selectedIndex,
          classNames = _this$props.classNames;


      if (suggestionsContainer && prevProps.selectedIndex !== selectedIndex) {
        var activeSuggestion = suggestionsContainer.querySelector(classNames.activeSuggestion);

        if (activeSuggestion) {
          maybeScrollSuggestionIntoView(activeSuggestion, suggestionsContainer);
        }
      }
    }, _this.markIt = function (input, query) {
      var escapedRegex = query.trim().replace(/[-\\^$*+?.()|[\]{}]/g, "\\$&");
      return {
        __html: input.replace(RegExp(escapedRegex, "gi"), "<mark>$&</mark>")
      };
    }, _this.shouldRenderSuggestions = function (query) {
      var _this3 = _this,
          props = _this3.props;

      var minQueryLength = props.minQueryLength || 2;
      return query.length >= minQueryLength;
    }, _this.render = function () {
      var _this4 = _this,
          props = _this4.props;

      var suggestions = props.suggestions.map(function (item, i) {
        return _react2.default.createElement(
          "li",
          {
            key: i,
            onMouseDown: props.handleClick.bind(null, i),
            onMouseOver: props.handleHover.bind(null, i),
            className: i == props.selectedIndex ? props.classNames.activeSuggestion : "" },
          _react2.default.createElement("span", { dangerouslySetInnerHTML: this.markIt(item, props.query) })
        );
      }.bind(_this));

      // use the override, if provided
      var shouldRenderSuggestions = props.shouldRenderSuggestions || _this.shouldRenderSuggestions;
      if (suggestions.length === 0 || !shouldRenderSuggestions(props.query)) {
        return null;
      }

      return _react2.default.createElement(
        "div",
        {
          ref: "suggestionsContainer",
          className: _this.props.classNames.suggestions },
        _react2.default.createElement(
          "ul",
          null,
          " ",
          suggestions,
          " "
        )
      );
    }, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  return Suggestions;
}(_react.Component), _class.propTypes = {
  query: _propTypes2.default.string.isRequired,
  selectedIndex: _propTypes2.default.number.isRequired,
  suggestions: _propTypes2.default.array.isRequired,
  handleClick: _propTypes2.default.func.isRequired,
  handleHover: _propTypes2.default.func.isRequired,
  minQueryLength: _propTypes2.default.number,
  shouldRenderSuggestions: _propTypes2.default.func,
  classNames: _propTypes2.default.object
}, _temp2);
exports.default = Suggestions;