'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _ReactTags = require('./ReactTags');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var TagInput = function (_Component) {
  (0, _inherits3.default)(TagInput, _Component);

  function TagInput(props) {
    (0, _classCallCheck3.default)(this, TagInput);

    var _this = (0, _possibleConstructorReturn3.default)(this, (TagInput.__proto__ || Object.getPrototypeOf(TagInput)).call(this, props));

    _this.state = {};
    _this.handleDelete = _this.handleDelete.bind(_this);
    _this.handleAddition = _this.handleAddition.bind(_this);
    _this.handleDrag = _this.handleDrag.bind(_this);
    _this.onFocus = _this.onFocus.bind(_this);
    _this.handleInputBlur = _this.handleInputBlur.bind(_this);
    return _this;
  }

  (0, _createClass3.default)(TagInput, [{
    key: 'handleDelete',
    value: function handleDelete(i) {
      var tags = this.props.value || [];
      tags.splice(i, 1);
      //this.setState({tags: tags});
      this.props.onChange(tags);
    }
  }, {
    key: 'handleAddition',
    value: function handleAddition(tag) {
      var tags = this.props.value || [];
      tags.push({
        id: tags.length + 1,
        text: tag
      });
      //this.setState({tags: tags});
      this.props.onChange(tags);
    }
  }, {
    key: 'handleDrag',
    value: function handleDrag(tag, currPos, newPos) {
      var tags = this.props.value || [];
      // mutate array
      tags.splice(currPos, 1);
      tags.splice(newPos, 0, tag);

      // re-render
      this.props.onChange(tags
      //this.setState({ tags: tags });
      );
    }
  }, {
    key: 'handleInputBlur',
    value: function handleInputBlur() {
      this.setState({ focusing: false });
    }
  }, {
    key: 'onFocus',
    value: function onFocus() {
      this.setState({ focusing: true });
    }
  }, {
    key: 'render',
    value: function render() {
      var _props = this.props,
          value = _props.value,
          suggestions = _props.suggestions;

      return _react2.default.createElement(_ReactTags.WithContext, { tags: value,
        classNames: {
          tags: 'tagsClass ' + (this.state.focusing ? 'focusing' : ''),
          tagInput: 'tagInputClass',
          tagInputField: 'tagInputFieldClass',
          selected: 'selectedClass',
          tag: 'tagClass',
          remove: 'removeClass',
          suggestions: 'suggestionsClass',
          activeSuggestion: 'activeSuggestionClass'
        },
        autofocus: true,
        handleInputBlur: this.handleInputBlur,
        onFocus: this.onFocus,
        suggestions: suggestions,

        handleDelete: this.handleDelete,
        handleAddition: this.handleAddition,
        handleDrag: this.handleDrag });
    }
  }]);
  return TagInput;
}(_react.Component);

exports.default = TagInput;