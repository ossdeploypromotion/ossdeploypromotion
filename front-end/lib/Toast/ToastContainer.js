'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Toast = require('utils/Toast');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ReactToastr = require('react-toastr');
var ToastContainer = ReactToastr.ToastContainer;

var ToastMessageFactory = _react2.default.createFactory(ReactToastr.ToastMessage.animation);

var OSSToastContainer = function (_Component) {
  (0, _inherits3.default)(OSSToastContainer, _Component);

  function OSSToastContainer() {
    (0, _classCallCheck3.default)(this, OSSToastContainer);

    var _this = (0, _possibleConstructorReturn3.default)(this, (OSSToastContainer.__proto__ || Object.getPrototypeOf(OSSToastContainer)).call(this));

    _this.toastMessage = _this.toastMessage.bind(_this);
    return _this;
  }

  (0, _createClass3.default)(OSSToastContainer, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      (0, _Toast.registerToastHandle)(this.toastMessage);
    }
  }, {
    key: 'toastMessage',
    value: function toastMessage(message, type, option) {
      var timeout = option && option.timeout;
      this.refs.container[type]("Thông báo", message, {
        timeOut: timeout || 1000,
        extendedTimeOut: 500,
        closeButton: true
      });
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(ToastContainer, { ref: 'container',
          preventDuplicates: false,
          toastMessageFactory: ToastMessageFactory,
          className: 'toast-top-right' })
      );
    }
  }]);
  return OSSToastContainer;
}(_react.Component);

exports.default = OSSToastContainer;