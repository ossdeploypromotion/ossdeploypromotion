"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (field) {
  return (// Define stateless component to render input and errors
    _react2.default.createElement(
      "div",
      { className: "checkbox-control" },
      _react2.default.createElement("input", { type: "checkbox", id: field.id, name: "check", checked: field.checked }),
      _react2.default.createElement("label", { "for": field.id })
    )
  );
}; /**
    * Created by W540 on 1/6/2017.
    */