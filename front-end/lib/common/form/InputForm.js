'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (field) {
  return (// Define stateless component to render input and errors
    _react2.default.createElement(
      'div',
      { className: field.meta.touched && field.meta.error && 'has-error' },
      _react2.default.createElement('input', (0, _extends3.default)({}, field.input, { id: field.id, className: 'form-control', placeholder: field.placeholder, type: field.type ? field.type : 'text', style: field.style })),
      _react2.default.createElement(
        'div',
        { className: 'state-error-wrapper' },
        field.meta.touched && field.meta.error && _react2.default.createElement(
          'div',
          { className: 'state-error-message' },
          field.meta.error
        )
      )
    )
  );
}; /**
    * Created by cthanh on 05/09/2016.
    */