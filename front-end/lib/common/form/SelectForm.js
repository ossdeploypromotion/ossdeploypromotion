"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = require("babel-runtime/helpers/extends");

var _extends3 = _interopRequireDefault(_extends2);

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (field) {
  // Define stateless component to render input and errors
  var items = field.items || [];
  return _react2.default.createElement(
    "div",
    null,
    _react2.default.createElement(
      "select",
      (0, _extends3.default)({}, field.input, { id: field.id, className: "input-control", placeholder: field.placeHolder, style: field.style || {} }),
      items
    ),
    _react2.default.createElement(
      "div",
      { className: "state-error-wrapper" },
      field.meta.touched && field.meta.error && _react2.default.createElement(
        "div",
        { className: "state-error-message" },
        field.meta.error
      )
    )
  );
};