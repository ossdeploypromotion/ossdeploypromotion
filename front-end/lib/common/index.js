'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CheckboxForm = exports.SelectForm = exports.InputForm = undefined;

var _InputForm2 = require('./form/InputForm');

var _InputForm3 = _interopRequireDefault(_InputForm2);

var _SelectForm2 = require('./form/SelectForm');

var _SelectForm3 = _interopRequireDefault(_SelectForm2);

var _CheckBoxForm = require('./form/CheckBoxForm');

var _CheckBoxForm2 = _interopRequireDefault(_CheckBoxForm);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.InputForm = _InputForm3.default; /**
                                          * Created by W540 on 1/6/2017.
                                          */

exports.SelectForm = _SelectForm3.default;
exports.CheckboxForm = _CheckBoxForm2.default;