'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.RadioButtonGroup = exports.RadioButton = undefined;

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _RadioButton = require('material-ui/RadioButton');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var RadioButton = exports.RadioButton = function (_Component) {
  (0, _inherits3.default)(RadioButton, _Component);

  function RadioButton() {
    (0, _classCallCheck3.default)(this, RadioButton);
    return (0, _possibleConstructorReturn3.default)(this, (RadioButton.__proto__ || Object.getPrototypeOf(RadioButton)).apply(this, arguments));
  }

  (0, _createClass3.default)(RadioButton, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(_RadioButton.RadioButton, (0, _extends3.default)({}, this.props, {
        style: Object.assign({}, this.props.style)
      }));
    }
  }]);
  return RadioButton;
}(_react.Component);

var RadioButtonGroup = exports.RadioButtonGroup = function (_Component2) {
  (0, _inherits3.default)(RadioButtonGroup, _Component2);

  function RadioButtonGroup() {
    (0, _classCallCheck3.default)(this, RadioButtonGroup);
    return (0, _possibleConstructorReturn3.default)(this, (RadioButtonGroup.__proto__ || Object.getPrototypeOf(RadioButtonGroup)).apply(this, arguments));
  }

  (0, _createClass3.default)(RadioButtonGroup, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        _RadioButton.RadioButtonGroup,
        (0, _extends3.default)({}, this.props, {
          style: Object.assign({}, this.props.style)
        }),
        this.props.children
      );
    }
  }]);
  return RadioButtonGroup;
}(_react.Component);