'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.FlatButton = exports.default = undefined;

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _objectWithoutProperties2 = require('babel-runtime/helpers/objectWithoutProperties');

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _RaisedButton = require('material-ui/RaisedButton');

var _RaisedButton2 = _interopRequireDefault(_RaisedButton);

var _FlatButton = require('material-ui/FlatButton');

var _FlatButton2 = _interopRequireDefault(_FlatButton);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var RaisedButton = function (_Component) {
  (0, _inherits3.default)(RaisedButton, _Component);

  function RaisedButton() {
    (0, _classCallCheck3.default)(this, RaisedButton);
    return (0, _possibleConstructorReturn3.default)(this, (RaisedButton.__proto__ || Object.getPrototypeOf(RaisedButton)).apply(this, arguments));
  }

  (0, _createClass3.default)(RaisedButton, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          primary = _props.primary,
          positive = _props.positive,
          style = _props.style,
          rest = (0, _objectWithoutProperties3.default)(_props, ['primary', 'positive', 'style']);

      var defaultsStyle = {
        backgroundColor: '#F5F5F5',
        borderRadius: 5,
        color: '#757575'
      };
      if (primary || positive) {
        defaultsStyle.backgroundColor = '#03A9F4';
        defaultsStyle.color = '#ffffff';
      }
      var Icon = this.props.iconName && _react2.default.createElement(
        'i',
        { className: 'material-icons', style: { color: defaultsStyle.color } },
        this.props.iconName
      );
      return _react2.default.createElement(_RaisedButton2.default, (0, _extends3.default)({
        backgroundColor: defaultsStyle.backgroundColor,
        labelColor: defaultsStyle.color,
        style: { color: defaultsStyle.color },
        icon: Icon,
        labelStyle: { padding: 8 }
      }, rest));
    }
  }]);
  return RaisedButton;
}(_react.Component); /**
                      * Created by W540 on 6/8/2017.
                      */


exports.default = RaisedButton;

var FlatButton = exports.FlatButton = function (_Component2) {
  (0, _inherits3.default)(FlatButton, _Component2);

  function FlatButton() {
    (0, _classCallCheck3.default)(this, FlatButton);
    return (0, _possibleConstructorReturn3.default)(this, (FlatButton.__proto__ || Object.getPrototypeOf(FlatButton)).apply(this, arguments));
  }

  (0, _createClass3.default)(FlatButton, [{
    key: 'render',
    value: function render() {
      var _props2 = this.props,
          primary = _props2.primary,
          positive = _props2.positive,
          style = _props2.style,
          rest = (0, _objectWithoutProperties3.default)(_props2, ['primary', 'positive', 'style']);

      var defaultsStyle = {
        backgroundColor: '#F5F5F5',
        borderRadius: 5,
        color: '#757575'
      };
      if (primary || positive) {
        defaultsStyle.backgroundColor = '#03A9F4';
        defaultsStyle.color = '#ffffff';
      }
      var Icon = this.props.iconName && _react2.default.createElement(
        'i',
        { className: 'material-icons', style: { color: defaultsStyle.color } },
        this.props.iconName
      );
      return _react2.default.createElement(_FlatButton2.default, (0, _extends3.default)({
        labelColor: defaultsStyle.color,
        style: { color: defaultsStyle.color },
        icon: Icon,
        labelStyle: { padding: 8 }
      }, rest));
    }
  }]);
  return FlatButton;
}(_react.Component);