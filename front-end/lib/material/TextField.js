'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _TextField = require('material-ui/TextField');

var _TextField2 = _interopRequireDefault(_TextField);

var _TextFieldLabel = require('material-ui/TextField/TextFieldLabel');

var _TextFieldLabel2 = _interopRequireDefault(_TextFieldLabel);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var TextFieldMaterial = function (_Component) {
  (0, _inherits3.default)(TextFieldMaterial, _Component);

  function TextFieldMaterial() {
    (0, _classCallCheck3.default)(this, TextFieldMaterial);
    return (0, _possibleConstructorReturn3.default)(this, (TextFieldMaterial.__proto__ || Object.getPrototypeOf(TextFieldMaterial)).apply(this, arguments));
  }

  (0, _createClass3.default)(TextFieldMaterial, [{
    key: 'render',
    value: function render() {
      var color = this.props.successText ? { color: '#8BC34A' } : this.props.helpText ? { color: '#9E9E9E' } : {};
      return _react2.default.createElement(_TextField2.default, (0, _extends3.default)({
        errorText: this.props.successText || this.props.helpText
      }, this.props, {
        errorStyle: color,
        style: Object.assign({
          width: 'inherit',
          height: 75
        }, this.props.style, color),
        floatingLabelStyle: color
      }));
    }
  }]);
  return TextFieldMaterial;
}(_react.Component);

exports.default = TextFieldMaterial;