'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDom = require('react-dom');

var _reactDom2 = _interopRequireDefault(_reactDom);

var _rcSwitch = require('rc-switch');

var _rcSwitch2 = _interopRequireDefault(_rcSwitch);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ToggleMaterial = function (_Component) {
  (0, _inherits3.default)(ToggleMaterial, _Component);

  function ToggleMaterial() {
    (0, _classCallCheck3.default)(this, ToggleMaterial);

    var _this = (0, _possibleConstructorReturn3.default)(this, (ToggleMaterial.__proto__ || Object.getPrototypeOf(ToggleMaterial)).call(this));

    _this.state = { disabled: false };
    _this.onToggle = _this.onToggle.bind(_this);
    return _this;
  }

  (0, _createClass3.default)(ToggleMaterial, [{
    key: 'onToggle',
    value: function onToggle() {
      this.setState({ disabled: !this.state.disabled });
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(_rcSwitch2.default, this.props)

      //  const status = this.props.longToggle ? {
      // 	labelStyle:{display: 'inline-block', width: '120px'},
      // 	thumbStyle:{backgroundColor: '#fff', color: '#03A9F4', border: '1px solid #03A9F4'},
      //  trackStyle:{backgroundColor: '#03A9F4', width:140},
      //  thumbSwitchedStyle:{backgroundColor: '#fff', },
      //  trackSwitchedStyle:{backgroundColor: '#03A9F4', width:140},
      //  elementStyle:{width:140}
      // } : {}
      ;
    }
  }]);
  return ToggleMaterial;
}(_react.Component);

exports.default = ToggleMaterial;