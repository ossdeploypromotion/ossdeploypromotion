/**
 * Created by W540 on 3/1/2017.
 */
let config = {}
if(__DEV__){
  __webpack_public_path__ = window.appContext
  config = {
    appContext: window.appContext,
    apiEndPoint: 'http://localhost:8080/promotion/',
    ossServerDomain: window.ssoServerUrl,
    employeeLink: 'employee/publicview?employeeid=',
    mailLink: 'message/readmailjson?id=',
    inboxLink: 'message/mailjson',
    ssoDomain:'http://oss.seedcom.vn/testsso',
    logout:'http://oss.seedcom.vn/testsso/logout',
    apiAuthenEndPoint:':8080/promotion/accesstoken',
    enableTimeout:false,
    maxUploadSize:100000000
  }
}
if (__PROD__ ){
  __webpack_public_path__ = window.appContext
  config = {
    appContext:window.appContext,
    apiEndPoint:window.appContext+'promotion/',
    ossServerDomain: window.ssoServerUrl,
    mailLink: 'message/readmailjson?id=',
    inboxLink: 'message/mailjson',
    employeeLink: 'employee/publicview?employeeid=',
    ssoDomain:window.ssoServerUrl,
    apiAuthenEndPoint:window.appContext+'api/accesstoken',
    logout:window.ssoServerUrl+'/logout',
    enableTimeout: true,
    maxUploadSize:100000000
  }
}
// if (__PRODTEST__ ){
//   config = {
//     appContext:"/okrtest/",
//     apiEndPoint:"/okrapitest/",
//     ssoDomain:'http://oss.seedcom.vn/testsso',
//     apiAuthenEndPoint:'http://localhost:8080/okrapitest/accesstoken',
//     logout:'http://oss.seedcom.vn/testsso/logout',
//     enableTimeout:true
//   }
// }
export default config;
