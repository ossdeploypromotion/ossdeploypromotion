import React, {Component} from 'react'
import Section from './Section'
import AddCategory from './AddCategory'
import ChangeCategory from './ChangeCategory'
import ViewCategory from './ViewCategory'
import {Dialog, RaisedButton, FlatButton, TextField, Checkbox, Toggle, Select, InfoSelect, SearchBox, MultipleSelect, TagInput, DatePicker, RadioButtonGroup, RadioButton} from 'components'
import '../Promotion.css'

export default class Accordion extends Component{
	constructor(props) {
	    super(props)
      	this.state = {
      		isOpen: false,
      		open: false,
      		select:{}
      	}
  	}
  	toggleHidden = () => {
	    this.setState({
	      	isOpen: !this.state.isOpen
	    })
  	}
  	isHidden = () => {
	    this.setState({
	      	open: !this.state.open
	    })
  	}
	render(){
		const sampleOptionData = [{label:'Option 1',value:1},{label:'Option 2',value:2},{label:'Option 3',value:3}]
		const style = {
	  		marginRight: 20,
	  		minWidth: 80
  		};
		return(
			<div className="main">
		        <Section title={
		        	<div>
		        		<div style={{width: '50%', display:'block'}}>Banner</div>
		        		<span className="edittable"><i className="material-icons">&#xE254;</i></span>
		        	</div>
		        }>
		        	<form onSubmit={(e)=>{
						e.preventDefault()
						this.toggleHidden() }}>

			        	{this.state.isOpen ?
			        		<ChangeCategory onClick={this.toggleHidden}/> :
				        	<AddCategory onClick={this.toggleHidden}/>
				        }
				    </form>
				    <form onSubmit={(e)=>{
						e.preventDefault()
						this.isHidden() }}>
						{this.state.open ?
							<ChangeCategory onClick={this.isHidden}/> :
					        <ViewCategory onClick={this.isHidden}/>
					    }
				    </form>
		        	<div className="row bContent">
		        		<div className="pull-left col-md-9">
		        			<h6>Biển quảng cáo windows display</h6>
		        			<div className="cnthm">
		        				Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec finibus tellus vel nisi ullamcorper, et dignissim nisl bibendum. Sed venenatis enim arcu, a tristique diam blandit in. Nam non sapien dictum, blandit neque in, consequat eros. Aenean fringilla magna urna, et rhoncus ligula laoreet a.
		        			</div>
		        			<div className="attach">
				                <div className="file">
				                  	<img src="https://www.papamikecreations.com/uploads/image/301-agenda.png"/>
				                  	<div className="file-attach--overlay"></div>
                    				<i className="material-icons">close</i>
				                </div>
				                 <div className="file">
				                  	<img src="http://www.trilbybookclub.co.uk/resources/public/assets/press/TB_icon_100.png"/>
				                  	<div className="file-attach--overlay"></div>
                    				<i className="material-icons">close</i>
				                </div>
				                <div className="file">
				                  	<img src="https://www.papamikecreations.com/uploads/image/301-agenda-1.png"/>
				                  	<div className="file-attach--overlay"></div>
                    				<i className="material-icons">close</i>
				                </div>
				                <div className="file">
				                  	<img src="https://www.papamikecreations.com/uploads/image/301-alarm-clock-1.png"/>
				                  	<div className="file-attach--overlay"></div>
                    				<i className="material-icons">close</i>
				                </div>
				                <div className="file">
				                  	<img src="https://www.generalkinematics.com/wp-content/uploads/2014/01/recycling-symbol-390x381.png"/>
				                  	<div className="file-attach--overlay"></div>
                    				<i className="material-icons">close</i>
				                </div>
				            </div>
		        		</div>
		        		<div className="pull-right col-md-3">
		        			<h5>54 cửa hàng</h5>
		        		</div>
		        	</div>
		        </Section>
		        <Section title="POSM">
		        </Section>
		        <Section title="Section Title Three">
		        	Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet nemo harum voluptas aliquid rem possimus nostrum excepturi!
		        </Section>
      		</div>
		)
	}
}
