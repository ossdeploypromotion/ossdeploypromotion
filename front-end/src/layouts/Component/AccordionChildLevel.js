import React, {Component} from 'react'
import SectionChildLevel from './SectionChildLevel'
import {Dialog, RaisedButton, FlatButton, TextField, Checkbox, Toggle, Select, InfoSelect, SearchBox, MultipleSelect, TagInput, DatePicker, RadioButtonGroup, RadioButton} from 'components'
import '../react-tabs.css';
import '../Promotion.css'

export default class AccordionChildLevel extends Component{
	constructor(props) {
	    super(props)
      	this.state = {
      		open: false
      	}
  	}
	render(){
		return(
			<div className="main">
				<SectionChildLevel title={
	    			<div className="row">
						<div className="col-md-1 text-right titleBold">1.1</div>
	    				<div className="col-md-4 titleBold">Banner mặt tiền</div>
	    				<div className="col-md-4"></div>
	    				<div className="col-md-3"><span className="cricle sYellow"></span>1/2 hạng mục</div>
	    			</div>
	    		}>
	    			<div className="row contentChild">
						<div className="col-md-1 text-right titleBold pr0">1.1.1</div>
	    				<div className="col-md-4 pl29 titleBold">
	    					Banner mặt tiền trái
	    					<div className="detailSmall">
	    						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec finibus tellus vel nisi ullamcorper, et dignissim...
	    						<a className="seemore" href="">Xem thêm</a>
	    					</div>
	    				</div>
	    				<div className="col-md-4 pl0">
	    					<p>Hình ảnh đã triển khai thực tế từ cửa hàng</p>
				    		<div className="attach attachSmall">
				                <div className="file fileSmall">
				                  	<img src="https://www.papamikecreations.com/uploads/image/301-agenda.png"/>
				                  	<div className="file-attach--overlay"></div>
	                				<i className="material-icons">close</i>
				                </div>
				                 <div className="file fileSmall">
				                  	<img src="http://www.trilbybookclub.co.uk/resources/public/assets/press/TB_icon_100.png"/>
				                  	<div className="file-attach--overlay"></div>
	                				<i className="material-icons">close</i>
				                </div>
			                </div>
	    				</div>
	    				<div className="col-md-3 pl8"><span className="cricle green"></span>23/7/2017 - 03:48 PM bởi Nguyễn Thị Hồng Thắm</div>
	    			</div>
	    			<div className="row contentChild">
						<div className="col-md-1 text-right titleBold pr0">1.1.2</div>
	    				<div className="col-md-4 pl29 titleBold">
	    					Banner mặt tiền phải
	    					<div className="detailSmall">
	    						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec finibus tellus vel nisi ullamcorper, et dignissim...
	    						<a className="seemore" href="">Xem thêm</a>
	    					</div>
	    				</div>
	    				<div className="col-md-4 pl0">
	    					<p>Hình ảnh đã triển khai thực tế từ cửa hàng</p>
	    				</div>
	    				<div className="col-md-3 pl8"><span className="cricle green"></span>23/7/2017 - 03:48 PM bởi Nguyễn Thị Hồng Thắm</div>
	    			</div>
				</SectionChildLevel>
				<SectionChildLevel title={
	    			<div className="row">
						<div className="col-md-1 text-right titleBold">2</div>
	    				<div className="col-md-4 titleBold">CH001 - 400 Nguyễn Trãi - HCM</div>
	    				<div className="col-md-4"></div>
	    				<div className="col-md-3"><span className="cricle sYellow"></span>0/2 nhóm hạng mụcc</div>
	    			</div>
	    		}>
	    			<div className="row contentChild">
						<div className="col-md-1 text-right titleBold pr0">1.1.1</div>
	    				<div className="col-md-4 pl29 titleBold">
	    					Banner mặt tiền trái
	    					<div className="detailSmall">
	    						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec finibus tellus vel nisi ullamcorper, et dignissim...
	    						<a className="seemore" href="">Xem thêm</a>
	    					</div>
	    				</div>
	    				<div className="col-md-4 pl0">
	    					<p>Hình ảnh đã triển khai thực tế từ cửa hàng</p>
				    		<div className="attach attachSmall">
				                <div className="file fileSmall">
				                  	<img src="https://www.papamikecreations.com/uploads/image/301-agenda.png"/>
				                  	<div className="file-attach--overlay"></div>
	                				<i className="material-icons">close</i>
				                </div>
				                 <div className="file fileSmall">
				                  	<img src="http://www.trilbybookclub.co.uk/resources/public/assets/press/TB_icon_100.png"/>
				                  	<div className="file-attach--overlay"></div>
	                				<i className="material-icons">close</i>
				                </div>
			                </div>
	    				</div>
	    				<div className="col-md-3 pl8"><span className="cricle green"></span>23/7/2017 - 03:48 PM bởi Nguyễn Thị Hồng Thắm</div>
	    			</div>
				</SectionChildLevel>
			</div>
		)
	}
}
