import React, {Component} from 'react'
import '../Promotion.css'

export default class AddCategory extends Component{
	render(){
		return(
			<div className="boxaddhm" onClick={this.props.onClick}>
				<h6><span className="edittable"><i className="material-icons">add</i></span>Thêm hạng mục</h6>
        	</div>
		)
	}
}
//thằng con gọi đến thằng cha thông qua props
//step1: khi user kích vào elemnt, hành động sẽ đụng vào html của thằng con
//step2: thằng con thông qua hàm this.props.onClick gọi hàm onClick ở thằng cha <AddCategory onClick={this.toggleHidden}/>
//step3: hàm toggleHidden đã được setState (isOpen: true)
