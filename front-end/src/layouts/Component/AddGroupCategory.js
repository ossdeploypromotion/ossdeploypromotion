import React, {Component} from 'react'
import {Dialog, RaisedButton, FlatButton, TextField, Checkbox, Toggle, Select, InfoSelect, SearchBox, MultipleSelect, TagInput, DatePicker, RadioButtonGroup, RadioButton} from 'oss-components'
import '../Promotion.css'

export default class AddGroupCategory extends Component{
	render(){
		const style = {
			normalText: {
				fontSize: 12,
				color: '#9E9E9E'
			},
			underlineStyle: {
				borderColor: '#03A9F4'
			}
		}
		return(
			<div className="boxAddGroup">
				<TextField
        			floatingLabelText="Tên nhóm hạng mục"
        			hintStyle={{fontSize:14, color:'#9E9E9E'}}
        			hintText="Nhập tên nhóm hạng mục"
        			style={{width: '50%'}}
        			floatingLabelFixed={true}/>
        		<div className="tfNote">Nhấn Enter để tạo hoặc Esc để huỷ bỏ</div>
			</div>
		)
	}
}
