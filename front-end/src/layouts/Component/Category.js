import React, {Component} from 'react'
import AddGroupCategory from './AddGroupCategory'
import '../Promotion.css'

export default class Category extends Component{
	constructor(props) {
	    super(props);
      	this.state = { isOpen: false }
  	}
  	toggleHidden = () => {
	    this.setState({
	      	isOpen: !this.state.isOpen
	    })
  	}
	render(){
		return(
			<div className="category">
				<form onSubmit={(e)=>{
					e.preventDefault()//nếu trong form có những input value thì sẽ gọi hàm onSubmit() xuống form; Nếu k có [e.preventDefault() thì sẽ reload lại trang; Khi bấm submit trong <form></form> thì form sẽ gởi yêu cầu lên và reload lại trang ]
					this.toggleHidden();
				}}>
					{this.state.isOpen ?
						<AddGroupCategory onClick={this.toggleHidden}/> :
						<div className='boxdefault' onClick={this.toggleHidden}>
							<h6><i className="material-icons">&#xE145;</i>Thêm nhóm hạng mục</h6>
						</div>

				}</form>
			</div>
		)
	}
}
