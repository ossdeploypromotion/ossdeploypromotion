import React, {Component} from 'react'
import {Dialog, RaisedButton, FlatButton, TextField, Checkbox, Toggle, Select, InfoSelect, SearchBox, MultipleSelect, TagInput, DatePicker, RadioButtonGroup, RadioButton} from 'oss-components'
import '../Promotion.css'

export default class ChangeCategory extends Component{
	constructor(props) {
	    super(props)
      	this.state = {
      		select:{}
      	}
  	}
	render(){
		const sampleOptionData = [{label:'Option 1',value:1},{label:'Option 2',value:2},{label:'Option 3',value:3}]
		const style = {
	  		marginRight: 20,
	  		minWidth: 80
  		};
		return(
			<div className="row bContent" >
	    		<div className="pull-left col-md-9" onClick={this.toggleHidden}>
		        	<div className="grhangmuc">
		        		<TextField
		        			floatingLabelText="Tên hạng mục"
		        			hintStyle={{fontSize:14, color:'#000'}}
		        			hintText="Sắp xếp sản phẩm nổi bật"
		        			style={{width: '100%'}}
		        			floatingLabelFixed={true}/>
		        	</div>
		        	<div className="grhangmuc">
		        		<label>Nội dung hạng mục</label>
		        		<TextField
					      	hintText="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec finibus tellus vel nisi ullamcorper, et dignissim nisl bibendum. Sed venenatis enim arcu, a tristique diam blandit in. Nam non sapien dictum, blandit neque in, consequat eros. Aenean fringilla magna urna, et rhoncus ligula laoreet a."
					      	hintStyle={{fontSize:14, color:'#000', marginTop:0, height:70}}
					      	style={{width:'100%',marginTop:5}}
					    /><br />
		        	</div>
		        	<div className="attach">
		                <div className="file">
		                  	<img src="http://www.trilbybookclub.co.uk/resources/public/assets/press/TB_icon_100.png"/>
		                  	<div className="file-attach--overlay"></div>
	        				<i className="material-icons">close</i>
		                </div>
		                <div className="file">
		                  	<img src="https://i.stack.imgur.com/lTReY.png"/>
		                  	<div className="file-attach--overlay"></div>
	        				<i className="material-icons">close</i>
		                </div>
		                <div className="file">
		                  	<img src="https://www.papamikecreations.com/uploads/image/301-alarm-clock-1.png"/>
		                  	<div className="file-attach--overlay"></div>
	        				<i className="material-icons">close</i>
		                </div>
		                <div className="file-attach--new">
		                    <i className="fa fa-plus"></i>
		                    <span>Thêm hình ảnh hoặc tập tin</span>
		                    <input accept="image/*" type="file" multiple="multiple" />
		                </div>
		            </div>
		            <div className="grhangmuc">
		        		<label className="col-md-12">Cửa hàng áp dụng</label>
		        		<div className="col-md-4 smr0">
			        		<MultipleSelect
				              	value={this.state.select.multi}
				              	onChange={(options)=> {
				                	this.setState({select:{...this.state.select,multi:options}})}}
				              	placeholder={'54 cửa hàng đã được chọn'} options={sampleOptionData}/>
				        </div>
		        	</div>
		        	<div className="lineButton">
		        		<RaisedButton
		        			onTouchTap={this.props.onClick}
					      	label="Huỷ bỏ"
					      	labelStyle={{fontSize:14, textTransform: 'capitalize', borderRadius: 2, color: '#757575'}}
					      	icon={<i className="material-icons" style={{color:'#757575', fontSize:14 }}>close</i>}
					      	style={style}/>
	      				<RaisedButton
					      	label="Áp dụng"
					      	primary={true}
					      	labelStyle={{color:'#fff', fontSize:14, textTransform: 'capitalize'}}
					      	icon={<i className="material-icons" style={{color:'#fff'}}>check</i>}
					      	style={style}/>
		        	</div>
		        </div>
		        <div className="pull-right col-md-3">

		        </div>
	        </div>

		)
	}
}
