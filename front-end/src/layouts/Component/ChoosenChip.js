import React from 'react'
import Chip from 'material-ui/Chip';

export default class ChoosenChip extends React.Component {
	constructor(props) {
    	super(props);
    	this.state = {chipData: [
	      	{key: 0, label: 'CH001 - 400 Nguyễn Trãi - HCM'},
	      	{key: 1, label: 'CH003 - 209-211 Quang Trung - Gò Vấp - HCM'},
	      	{key: 2, label: 'CH009 - 803 Lũy Bán Bích - HCM'},
	      	{key: 3, label: 'CH002 - 4/32 Quang Trung - Hóc Môn - HCM'},
    	]};
	    this.styles = {
	      	chip: {
	        	marginBottom: 10,
	        	width: '100%',
	        	borderRadius: 0,
	        	background: '#FAFAFA',
	        	color: '#000000'
	      	},
	      	wrapper: {
	        	display: 'flex',
	        	flexWrap: 'wrap',
      		}
      	}
    };

    handleRequestDelete = (key) => {
	    if (key === 3) {
	      	alert('Why would you want to delete React?! :)');
	      	return;
	    }
	    this.chipData = this.state.chipData;
	    const chipToDelete = this.chipData.map((chip) => chip.key).indexOf(key);
	    this.chipData.splice(chipToDelete, 1);
	    this.setState({chipData: this.chipData});
  	};

  	renderChip(data) {
    	return (
      		<Chip
		        key={data.key}
		        onRequestDelete={() => this.handleRequestDelete(data.key)}
		        style={this.styles.chip}
      		>
        		{data.label}
      		</Chip>
    	);
  	}

  	render() {
	    return (
	      	<div style={this.styles.wrapper}>
	        	{this.state.chipData.map(this.renderChip, this)}
	      	</div>
	    );
  	}
}