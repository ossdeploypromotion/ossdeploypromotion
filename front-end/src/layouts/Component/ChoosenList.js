import React, {Component} from 'react'
import {Dialog, RaisedButton, FlatButton} from 'oss-components'
import ChoosenListChil from './ChoosenListChil'
import { Scrollbars } from 'react-custom-scrollbars'
import '../Promotion.css'

export default class ChoosenList extends Component{
	state = {
	    open: false,
  	};

  	handleOpen = () => {
    	this.setState({open: true});
  	};

  	handleClose = () => {
    	this.setState({open: false});
  	};
	render(){
		const style = {
			buttons: {
				background: '#fff',
				boxShadow: 0,
				top: 0,
				right: 0,
				marginTop: 13,
				marginRight: 20,
	    		minWidth: 0,
			    position: 'absolute'
			},
			smDialog: {
				width: 464
			}
		}
		const cListChil = [
		  	{
		    	id: 1,
		    	title: 'CH001 - 400 Nguyễn Trãi - HCM'
		  	},
		  	{
		    	id: 2,
		    	title: 'CH003 - 209-211 Quang Trung - Gò Vấp - HCM'
		  	},
		  	{
		    	id: 3,
		    	title: 'CH009 - 803 Lũy Bán Bích - HCM'
		  	},
		  	{
		    	id: 4,
		    	title: 'CH002 - 4/32 Quang Trung - Hóc Môn - HCM'
		  	},
		  	{
		    	id: 5,
		    	title: 'CH007 - 352 Huỳnh Tấn Phát - HCM'
		  	},
		  	{
		    	id: 6,
		    	title: 'CH012 - 61 Lê Văn Việt - HCM'
		  	},
		  	{
		    	id: 7,
		    	title: 'CH013 - 144/5 Nguyễn Ảnh Thủ - HCM'
		  	},
		  	{
		    	id: 8,
		    	title: 'CH006 - 22 Tân Mai - HAN'
		  	},
		  	{
		    	id: 9,
		    	title: 'CH005 - 46 Hàng Da - HAN'
		  	},
		  	{
		    	id: 10,
		    	title: 'CH013 - 144/5 Nguyễn Ảnh Thủ - HCM'
		  	}
		];
		return(
			<div>
				<h6 label="Dialog" onTouchTap={this.handleOpen}>Đã chọn <span>2</span> cửa hàng</h6>
				<Dialog
		          	title=
		          		{
		          			<div className="row">
		          				<h6 className="pull-left">Danh sách các cửa hàng áp dụng</h6>

	          					<RaisedButton
				      				icon={<i className="material-icons" style={{color:'#757575', borderRadius: 2}}>close</i>}
				      				buttonStyle={{background: '#fff', color: '#9E9E9E'}}
				      				style={style.buttons}
				      				disableTouchRipple={true}
				      				onTouchTap={this.handleClose}/>
		          			</div>}
		          	modal={true}
		          	open={this.state.open}
		          	onRequestClose={this.handleClose}
		          	autoScrollBodyContent={false}
		          	contentStyle={{width: 464}}
		          	bodyStyle={{paddingRight: 40}}
		        	>
		        	<Scrollbars
		        		style={{height:300}}>
	        			<ChoosenListChil
	        				cListChil={cListChil} />
		        	</Scrollbars>
        		</Dialog>
			</div>
		)
	}
}
