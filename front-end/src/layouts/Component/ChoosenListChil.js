import React, {Component, PropTypes} from 'react'

export default class ChoosenListChil extends Component{
	static propTypes = {
    	cListChil: PropTypes.array
  	}

  	static defaultProps = {
    	cListChil: []
  	}
	render(){
		return(
			<div>
				<ul className="cListChil">
					{
						this.props.cListChil.map((listChil, key) => {
		  					return <li key={key}><span>{listChil.id}.</span>{listChil.title}</li>
		  				})
		  			}
		  		</ul>
			</div>
		)
	}
}