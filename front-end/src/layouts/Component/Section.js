import React, {Component} from 'react'
import { Scrollbars } from 'react-custom-scrollbars'
import '../Promotion.css'

export default class Section extends Component{
	constructor(){
		super();
		this.state = {
			open: false,
			className: "section",
		}
	}
	handleClick = () => {
    	if(this.state.open) {
      		this.setState({
        		open: false,
        		className: "section",
  			});
    	}else{
      		this.setState({
        	open: true,
        	className: "section open"
      		});
    	}
	}
	render(){
		return(
			<div className={this.state.className}>
		        <div className="togglebtn"></div>
		        <div className="sectionhead" onClick={this.handleClick}>
		        	<h6>{this.props.title}</h6>
		        </div>
		        <div className="articlewrap">
		          	<div className="article">
            			{this.props.children}
		          	</div>
		        </div>
      		</div>
		)
	}
}
//state là phải cấp phát bộ nhớ
//props làm chức năng là sợi dây giữa 2 cha con;
//props về mặt html thì giống attibute; js thì giống params
