import React, {Component} from 'react'
import {Dialog, RaisedButton, FlatButton, TextField, Checkbox, Toggle, Select, InfoSelect, SearchBox, MultipleSelect, TagInput, DatePicker, RadioButtonGroup, RadioButton} from 'oss-components'
import '../react-tabs.css';
import '../Promotion.css'

export default class SectionChildLevel extends Component{
	constructor(){
		super();
		this.state = {
			open: false,
			className: "sectionChild",
		}
	}
	handleClick = () => {
    	if(this.state.open) {
      		this.setState({
        		open: false,
        		className: "sectionChild",
  			});
    	}else{
      		this.setState({
        	open: true,
        	className: "sectionChild open"
      		});
    	}
	}
	render(){
		return(
			<div className={this.state.className}>
		        <div className="togglebtn"></div>
		        <div className="sectionhead" onClick={this.handleClick}>
		        	<h6 className="headTitle">{this.props.title}</h6>
		        </div>
		        <div className="articlewrap">
		          	<div className="article">
            			{this.props.children}
		          	</div>
		        </div>
      		</div>
		)
	}
}
