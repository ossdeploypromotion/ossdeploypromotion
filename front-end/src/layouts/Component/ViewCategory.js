import React, {Component} from 'react'
import {Dialog, RaisedButton, FlatButton} from 'oss-components'
import Control from '../Control'
import '../Promotion.css'

export default class ViewCategory extends Component{

	state = {
	    open: false,
	    data:[
	    {
	    	label:'CH003 - 209-211 Quang Trung - Gò Vấp - HCM',
	    	checked:false
	    },{
	    	label:'CH003 - 209-211 Quang Trung - Gò Vấp - HCM',
	    	checked:false
	    },{
	    	label:'CH003 - 209-211 Quang Trung - Gò Vấp - HCM',
	    	checked:false
	    },{
	    	label:'CH003 - 209-211 Quang Trung - Gò Vấp - HCM',
	    	checked:false
	    },{
	    	label:'CH003 - 209-211 Quang Trung - Gò Vấp - HCM',
	    	checked:false
	    },{
	    	label:'CH003 - 209-211 Quang Trung - Gò Vấp - HCM',
	    	checked:false
	    },{
	    	label:'CH003 - 209-211 Quang Trung - Gò Vấp - HCM',
	    	checked:false
	    },{
	    	label:'CH003 - 209-211 Quang Trung - Gò Vấp - HCM',
	    	checked:false
	    },{
	    	label:'CH003 - 209-211 Quang Trung - Gò Vấp - HCM',
	    	checked:false
	    },{
	    	label:'CH003 - 209-211 Quang Trung - Gò Vấp - HCM',
	    	checked:false
	    },{
	    	label:'CH003 - 209-211 Quang Trung - Gò Vấp - HCM',
	    	checked:false
	    },{
	    	label:'CH003 - 209-211 Quang Trung - Gò Vấp - HCM',
	    	checked:false
	    },
	    ]
  	}

  	handleOpen = () => {
    	this.setState({open: true});
  	}

  	handleClose = () => {
    	this.setState({open: false});
  	}

  	handleChange = (event) => {
  		alert('aaa')
	    this.props.handleChange(event)
  	}
  	//chọn checked hết 1 mảng
  	allCheck = () =>{
  		// debugger
  		// step1: lấy mảng data ra
  		// step2: lấy item trong mảng
  		const data = this.state.data.map(item => {
  			// debugger
  			item.checked = true;
  			return item
  		});

  		this.setState({data})
  	}
  	//chọn unchecked hết 1 mảng
  	unCheck = () => {
  		const data = this.state.data.map(item => {
  			// debugger
  			item.checked = false;
  			return item
  		});
  		this.setState({data})
  	}
  	//duyệt mảng để checked/uncheck cho từng item trong 1 mảng nếu wanted
  	handleChange = (index) => {
		// debugger
		const data = this.state.data
		data[index].checked = !data[index].checked
		this.setState({data})
	}
	render(){
		const style = {
	  		marginRight: 10,
	  		marginLeft: 10,
	  		minWidth: 80
  		}
		return(
			<div className="row bContent">
	    		<div className="pull-left col-md-9">
	    			<h6>Banner</h6>
	    			<div className="cnthm">
	    				Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec finibus tellus vel nisi ullamcorper, et dignissim nisl bibendum. Sed venenatis enim arcu, a tristique diam blandit in. Nam non sapien dictum, blandit neque in, consequat eros. Aenean fringilla magna urna, et rhoncus ligula laoreet a.
	    			</div>
	    			<div className="attach">
		                <div className="file">
		                  	<img src="https://www.papamikecreations.com/uploads/image/301-agenda.png"/>
		                  	<div className="file-attach--overlay"></div>
	        				<i className="material-icons">close</i>
		                </div>
		                 <div className="file">
		                  	<img src="http://www.trilbybookclub.co.uk/resources/public/assets/press/TB_icon_100.png"/>
		                  	<div className="file-attach--overlay"></div>
	        				<i className="material-icons">close</i>
		                </div>
		                <div className="file">
		                  	<img src="https://www.papamikecreations.com/uploads/image/301-agenda-1.png"/>
		                  	<div className="file-attach--overlay"></div>
	        				<i className="material-icons">close</i>
		                </div>
		                <div className="file">
		                  	<img src="https://www.papamikecreations.com/uploads/image/301-alarm-clock-1.png"/>
		                  	<div className="file-attach--overlay"></div>
	        				<i className="material-icons">close</i>
		                </div>
		                <div className="file">
		                  	<img src="https://www.generalkinematics.com/wp-content/uploads/2014/01/recycling-symbol-390x381.png"/>
		                  	<div className="file-attach--overlay"></div>
	        				<i className="material-icons">close</i>
		                </div>
		            </div>
	    		</div>
	    		<div className="pull-right col-md-2">
	    			<h5 onClick={this.handleOpen}>54 cửa hàng</h5>
	    		</div>
	    		<div className="col-md-1 text-center" onClick={this.props.onClick}>
	    			<h6 className="editBlockHM"><i className="material-icons">&#xE254;</i></h6>
	    		</div>
	    		<Dialog onHandle={this.handleOpen}
		          	title={<div><h3 className="tDialog">Các cửa hàng áp dụng</h3></div>}
		          	actions={
		          		<div>
		          			<div className="pull-left" style={{paddingLeft:20}}>
		          				<FlatButton
		          					onClick={this.allCheck}
		          					label="Chọn tất cả trong danh sách"
		          					labelStyle={{color:'#03A9F4', fontSize:14, textTransform: 'initial'}}/>
		          				<FlatButton
		          					onClick={this.unCheck}
		          					label="Bỏ chọn tất cả trong danh sách trên"
		          					labelStyle={{color:'#03A9F4', fontSize:14, textTransform: 'initial'}}/>
		          			</div>
		          			<div className="pull-right">
		          				<RaisedButton
							      	label="Huỷ bỏ"
							      	onTouchTap={this.handleClose}
							      	labelStyle={{fontSize:14, textTransform: 'capitalize', borderRadius: 2}}
							      	icon={<i className="material-icons" style={{color:'#757575', borderRadius: 2}}>close</i>}
							      	style={style}/>
		          				<RaisedButton
							      	label="Áp dụng"
							      	primary={true}
							      	labelStyle={{color:'#fff', fontSize:14, textTransform: 'capitalize'}}
							      	icon={<i className="material-icons" style={{color:'#fff'}}>check</i>}
							      	style={style}/>
		          			</div>
		          			<div className="clearfix"></div>
		          		</div>
		          	}
		          	modal={true}
		          	open={this.state.open}
		          	onRequestClose={this.handleClose}
		          	autoScrollBodyContent={false}
		        >
		        	<Control data={this.state.data} handleChange={this.handleChange}></Control>
		        </Dialog>
	    	</div>
		)
	}
}

