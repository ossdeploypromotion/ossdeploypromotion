import React, {Component} from 'react'
import {Dialog, RaisedButton, FlatButton, TextField, Checkbox, Toggle, Select, InfoSelect, SearchBox, MultipleSelect, TagInput, DatePicker, RadioButtonGroup, RadioButton} from 'components'
import { Scrollbars } from 'react-custom-scrollbars'
import ChoosenChip from './Component/ChoosenChip'
import ChoosenList from './Component/ChoosenList'
import './Promotion.css'

export default class Control extends Component{
	constructor(){
	    super()
	    this.state = {
	    	select:{}
	    }
  	}
	render(){
		const sampleOptionData = [{label:'Option 1',value:1},{label:'Option 2',value:2},{label:'Option 3',value:3}]
		return(
			<div className="row bControl">
				<div className="col">
					<div className="row">
						<div className="col">
							<label>Khu vực</label>
							<Select
		                    	value={this.state.select.dijit}
		                    	onChange={(option)=>{this.setState({select:{...this.state.select,dijit:option}})}}
		                    	placeholder={'Toàn quốc'}
		                    	options={sampleOptionData}
		                   	/>
		                </div>
		                <div className="col">
		                	<label>Tìm kiếm cửa hàng</label>
              				<SearchBox placeholder={'Nhập tên hoặc mã cửa hàng'}/>
		                </div>
	                </div>
                	<div className="col cbCompany">
                		<Checkbox
                			iconStyle={{marginRight:5,}}
                			labelStyle={{color:'#000', fontSize:14}}
                			label="Tất cả các cửa hàng trong hệ thống"
                			style={{width:'100%'}}/>
                	</div>
                	<div className="col">
                		<Scrollbars
                			style={{ height: 350}}>
                			{this.props.data.map((item,index) => (<Checkbox
                				checked={item.checked}
								onCheck={()=>{
										debugger
										this.props.handleChange(index)
								}}
                				className="cbList"
	                			iconStyle={{marginRight:5,}}
	                			labelStyle={{color:'#000', fontSize:14}}
	                			label={item.label}
	                			style={{width:'100%'}}
                				/>)
                				)}
                		 </Scrollbars>
                	</div>
				</div>
				<div className="col cblistcheck">
					<ChoosenList/>
					<Scrollbars
            			style={{ height: 350}}>
						<ChoosenChip/>
					</Scrollbars>
					<Dialog
						title={<div><h3 className="tDialog">Các cửa hàng áp dụng</h3></div>}
						modal={true}
		          		open={this.state.open}
		          		onRequestClose={this.handleClose}
		          		autoScrollBodyContent={false}
					/>
				</div>
			</div>
		)
	}
}
