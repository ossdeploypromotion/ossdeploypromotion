import React, { Component } from 'react'
import './CoreLayout.scss'
import { fetchUserInfo, fetchUserFollowObjective, fetchFollowUser, fetchFollowDepartment, fetchListCompany, changeUserCompany } from 'reducers/userReducer'
import { loadMenuLink, loadTopLeftMenuLink } from 'reducers/menuReducer'
import { connect } from 'react-redux'
import {getInstance} from 'store/ApiClient'
import { MasterLayout} from 'oss-components'
@connect((state) => ({ user: state.user.user, listCompany:state.user.listCompany, globalLoading: state.globalState.loading }))
export class CoreLayout extends Component {
  constructor () {
    super()
    this.state = {
      miniMenu: true
    }
    this.toogleMenu = this.toogleMenu.bind(this)
    this.changeCompany = this.changeCompany.bind(this)
  }

  toogleMenu () {
    this.setState({ miniMenu: !this.state.miniMenu })
  }

  componentDidMount () {
    this.props.dispatch(fetchUserInfo())
  }

  changeCompany (companyId) {
    this.props.dispatch(changeUserCompany(companyId))
  }

  render () {
    const { location:{ pathname }, params } = this.props
    return (
      <MasterLayout
        getClient={getInstance}
        changeCompany={()=>{}}
        apiEndPoint="http://172.16.0.132/iapi/"
        ossServerDomain="http://oss.seedcom.vn/">
            {this.props.children}
            {this.props.globalLoading &&
              <div className='full-screen-spinner-container'>
                <div className='center-spinner' />
              </div>
            }
      </MasterLayout>
    )
  }
}

CoreLayout.propTypes = {
  children: React.PropTypes.element.isRequired
}

export default CoreLayout
