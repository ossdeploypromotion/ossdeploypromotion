import React from 'react'
import './OKRSideBar.scss'
import {Link} from 'react-router'
import config from 'config'
import {createObjectiveDetailLink,createOkrUserLink,createOkrDepartmentLink} from 'utils/LinkCreator'
import QuarterSelectTool from './QuarterSelectTool'

export default ({params,location,okrGroup,createGroup,user,followObjectives,followUsers,followDepartments}) => {
  const okrtype = params.okrtype;
  const groupId = location.query.groupid;
  const departmentid = location.query.departmentid;
  const {quarter,year} = location.query
  const listGroup = okrGroup.listGroup
  // parse 3 pathname
  const re = new RegExp('^'+config.appContext+'([^\/]+)(\/[^\/]+)?(\/[^\/]+)?');
  const pathSplit = location.pathname.match(re);
  const firstPath = pathSplit[1]
  const finalPath = pathSplit.length > 3 && pathSplit[3];
  return (
    <ul className="okr-sidebar">
      <li style={{background:'#F5F5F5'}}>
        <QuarterSelectTool location={location}/>
      </li>
      <li className="separator">
        Nổi bật
      </li>
      <li className={okrtype == "myokr" && "active"}>
        <Link to={{pathname:`${config.appContext}${firstPath}/myokr${finalPath ? `${finalPath}` : ""}`,query:{quarter,year}}}>
          <img className="user-image" src={user && user.photo}/>
          OKR của tôi
        </Link>
      </li>
      <li className={(okrtype == "department" && !departmentid) && "active"}>
        <Link to={{pathname:`${config.appContext}${firstPath}/department${finalPath ? `${finalPath}` : ""}`,query:{quarter,year}}}><span className="round-icon"><i className="fa fa-calculator"></i></span>
          {user && user.department.name}
        </Link>
      </li>
      <li className={okrtype == "company" && "active"}>
        <Link to={{pathname:`${config.appContext}${firstPath}/company${finalPath ? `${finalPath}` : ""}`,query:{quarter,year}}}>
          <span className="round-icon"><i className="fa fa-calculator"></i></span>
          {user && user.company.name}
        </Link>
      </li>
      <li>
        <div className="pull-left">
          <strong>ĐANG THEO DÕI</strong>
        </div>
        <div className="pull-right">
          <a href="javascript:void(0)" onClick={(e)=>{createGroup()}} className="create-group-link"><i className="fa fa-plus"></i>Tạo nhóm mới</a>
        </div>
      </li>
      <li className="list-pin">
        <ul>
          {listGroup && listGroup.map(group => (
            <li key={group.id} className={okrtype == "group" && groupId==group.id && "active"}>
              <Link to={{pathname:`${config.appContext}${firstPath}/group${finalPath ? `${finalPath}` : ""}`,query:{groupid:group.id,quarter,year}}}>
                <span className="round-icon"><i className="fa fa-users"></i></span>
                {group.name}
              </Link>
            </li>
          ))}
          {followObjectives && Object.values(followObjectives).map (obj => (
            <li key={`obj-${obj.objectiveId}`}>
              <Link to={createObjectiveDetailLink(obj,{quarter: obj.quarter,year:obj.year})}>
                <div className="flex-box flex-center">
                <span className="round-icon">w
                  <i className="fa fa-file-text" aria-hidden="true"></i>
                </span>
                {obj.name}
                </div>
              </Link>
            </li>
          ))}
          {followUsers && Object.values(followUsers).map (follow => (
            <li key={`follow-${follow.followUser.username}`}>
              <Link to={createOkrUserLink(follow.followUser,{quarter,year})}>
                <div className="flex-box flex-center">
                  <img className="user-image" src={follow.followUser.photo}/>
                  <span>{follow.followUser.fullname}</span>
                </div>
              </Link>
            </li>
          ))}
          {followDepartments && Object.values(followDepartments).map (follow => (
              <li key={`follow-dep-${follow.department.id}`}>
                <Link to={createOkrDepartmentLink(follow.department.id, {quarter, year})}>
                <div className="flex-box flex-center">
                <span className="round-icon"><i className="fa fa-calculator"></i></span>
                <span>{follow.department.name}</span>
                </div>
                </Link>
              </li>
          ))}
        </ul>
      </li>
    </ul>
  )
}
