/**
 * Created by W540 on 3/31/2017.
 */
import React, {Component, PropTypes} from 'react'
import {getCurrentYear, getCurrentQuater} from 'utils/DateTime'
import {browserHistory} from 'react-router'
import {Dropdown, MenuItem} from 'react-bootstrap'
import {CustomeToggle} from 'components'
import './QuarterSelectTool.scss'
const currentQuarter = getCurrentQuater();
const currentYear = getCurrentYear();
function createQuarterList(quarter, year) {
  let result = {};
  const intQuarter = parseInt(quarter)
  for (let i = intQuarter; i < intQuarter + 8; i++) {
    const q = i % 4 + 1
    const subYear = parseInt(year) + (parseInt(i / 4) - 1);
    const qyear = `Q${q}-${subYear}`
    result[qyear] = {quarter: q, year: subYear, text: qyear}
  }
  return result;
}
const onSelectQuarter = (location, quaterList) => (value) => {
  const {quarter, year} = quaterList[value]
  browserHistory.push({pathname: location.pathname, query: {...location.query, quarter, year}})
}

export default class QuarterSelectTool extends Component {
  static propTypes = {
    location: PropTypes.object.isRequired
  }

  render() {
    const {quarter, year} = this.props.location.query;
    const quaterList = createQuarterList(currentQuarter, currentYear);
    const onSelected = onSelectQuarter(location, quaterList)
    return (
      <div className="quarter-select">
        <div className="quarter-select-label">Bạn đang thiết lập cho</div>
        <Dropdown className="quarter-select-dropdown" id={`dropdown-select-quarter`}>
          <CustomeToggle bsRole="toggle">
            <div className="quarter-sellect-control">
            {`Q${quarter}-${year}`} <i className="fa fa-caret-down" aria-hidden="true"></i>
            </div>
          </CustomeToggle>
          <Dropdown.Menu  bsRole="menu">
          {
            Object.values(quaterList).map(item =>
                <MenuItem className={`Q${quarter}-${year}`==item.text && 'selected'} key={item.text} onClick={() => onSelected(item.text)}>{item.text}</MenuItem>
              )
          }
          </Dropdown.Menu>

        </Dropdown>
      </div>
    )
  }
}
