import React,{Component} from 'react'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs'
import '../../Sample/Tabs/react-tabs.css'


export default class Promotion extends Component{
	render(){
		return(
			<div className="container">
				<Tabs>
				    <TabList>
				      	<Tab>Cấu hình khuyến mãi</Tab>
			      		<Tab>Các bước triển khai</Tab>
			      		<Tab>Thông báo/thảo luận</Tab>
				    </TabList>

				    <TabPanel>
				      	<h2>Any content 1</h2>
				    </TabPanel>
				    <TabPanel>
				      	<h2>Any content 2</h2>
				    </TabPanel>
				    <TabPanel>
				      	<h2>Any content 3</h2>
				    </TabPanel>
			  	</Tabs>
			</div>
		)
	}
}
