/**
 * Created by W540 on 3/7/2017.
 */
import React,{Component} from 'react'
import {connect} from 'react-redux'
import {browserHistory} from 'react-router'
import {fetchListCompany,changeUserCompany} from 'reducers/userReducer'
import './SelectCompany.scss'
import config from 'config'
@connect(state => ({listCompany: state.user.listCompany}))
export default class SelectCompany extends Component{
  selectCompany(id){
    this.props.dispatch(changeUserCompany(id,() =>{
      browserHistory.push(config.appContext)
    }));
  }
  componentDidMount(){
    this.props.dispatch(fetchListCompany());
  }
  render(){
    return(
      <div className="skin-blue">
        <div className="select-company-wrapper">
          <div className="select-company-header">
            Chọn Công Ty
          </div>
          <ul className="company-list">
            {this.props.listCompany && this.props.listCompany.map(company => (
              <li onClick={() =>this.selectCompany(company.id)}>
                <a href="javascript:void(0)" >{company.name}</a>
                <a href="javascript:void(0)" style={{float:'right'}}><i className="fa fa-caret-right"></i></a>
              </li>
            ))}
          </ul>
        </div>
      </div>
    )
  }
}
