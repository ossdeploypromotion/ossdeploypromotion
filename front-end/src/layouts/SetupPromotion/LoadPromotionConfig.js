import React ,{Component} from 'react'
import {loadPromotionConfig} from 'reducers/PromotionConfigReducer'
export class LoadPromotionConfig extends Component{
  constructor(){
  super();
  this.loadPromotionConfig= this.loadPromotionConfig.bind(this);
  }
  loadPromotionConfig(){
    this.props.dispatch(loadPromotionConfig("hsgd"));
  }
  render(){
    return(
      <div>
        <a href="" onClick={this.loadPromotionConfig}>Load Promotion</a>
      </div>
    );
  }
}
