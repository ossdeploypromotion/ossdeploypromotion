import React,{Component} from 'react'

import './TemplateCategory.scss'

export default class TemplateCategory extends Component{
  render(){
    return (
      <div className='category'>
        <div className='headline'>
          <span className="left"> NHÓM HẠNG MỤC </span>
          <span className="right"> CỬA HÀNG ÁP DỤNG </span>
        </div>

        <div className="add">
          <i className="fa fa-plus"></i>
          <span>Thêm nhóm hạng mục</span>
        </div>

        <div className="add gr-tp">
          <p>Tên nhóm hạng mục</p>
          <input placeholder="Nhập tên nhóm hạng mục"/>
          <span>Nhấn Enter để tạo hoặc Esc để hủy bỏ</span>
        </div>

        <div className="group">
          <div className="title">
          <i className="fa fa-chevron-down"></i>
          <span> Banner</span>
            <i className="fa fa-pencil edit"></i>
          </div>

          <div className="expand">
            <div className="item add">
              <i className="fa fa-plus"></i>
              <span> Thêm hạng mục </span>
              <i className="fa fa-pencil edit"></i>
            </div>

            <div className="item">
              <h5>
                Banner mặt tiền
                <i className="fa fa-pencil edit"></i>
                <a href=""> 54 cửa hàng </a>
              </h5>
              <div className="discription">
                Một người chơi đặt toàn bộ linh kiện của hãng độ Takegawa, Nhật Bản để tạo ra chiếc xe độc và mạnh mẽ.
                Chiếc Honda 67 "lột xác" nhờ phụ tùng đặt hàng suốt 2 năm từ Nhật Bản về Việt Nam. Ngoài bộ khung sườn và lốc máy sơn lại,
                hầu hết các chi tiết trên xe đều mới và chủ yếu do các hãng độ tại Nhật Bản sản xuất như cặp vành, giảm xóc, các chi tiết ốc, đồng hồ, tay lái, xi-nhan.
              </div>
              <div className="attach">
                <div className="file">
                  <img src="http://img.f29.vnecdn.net/2017/06/02/IMG-0468-JPG-7485-1496421457.jpg"/>
                </div>
                <div className="file">
                  <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Flag_of_Canada_%28Pantone%29.svg/1200px-Flag_of_Canada_%28Pantone%29.svg.png"/>
                </div>
                <div className="file">
                  <img src="https://scontent.fsgn3-1.fna.fbcdn.net/v/t1.0-9/18485696_10155291070529860_1206334182802362044_n.jpg?oh=e92370d565153223bc4eb2d76e9597ed&oe=59DBEE15"/>
                </div>
                <div className="file">
                  <img src="http://img.f29.vnecdn.net/2017/06/02/IMG-0468-JPG-7485-1496421457.jpg"/>
                </div>
                <div className="file">
                  <img src="https://scontent.fsgn3-1.fna.fbcdn.net/v/t1.0-9/16473339_875321125943051_4468272224378202260_n.jpg?oh=972f1d5c025775aefc566112f020167a&oe=59A394C0"/>
                </div>
                <div className="file">
                  <img src="http://img.f29.vnecdn.net/2017/06/02/IMG-0468-JPG-7485-1496421457.jpg"/>
                </div>
                <div className="file">
                  <img src="http://img.f29.vnecdn.net/2017/06/02/IMG-0468-JPG-7485-1496421457.jpg"/>
                </div>
                <div className="file">
                  <img src="http://cafefcdn.com/thumb_w/660/2017/6a00d8351b44f853ef01b8d132349e970c-1496478973790.jpg"/>
                </div>
                <div className="file">
                  <img src="http://cafefcdn.com/thumb_w/660/2017/6a00d8351b44f853ef01b8d132349e970c-1496478973790.jpg"/>
                </div>
                <div className="file">
                  <img src="http://cafefcdn.com/thumb_w/660/2017/6a00d8351b44f853ef01b8d132349e970c-1496478973790.jpg"/>
                </div>
                <div className="file">
                  <img src="http://cafefcdn.com/thumb_w/660/2017/6a00d8351b44f853ef01b8d132349e970c-1496478973790.jpg"/>
                </div>
                <div className="file">
                  <img src="http://cafefcdn.com/thumb_w/660/2017/6a00d8351b44f853ef01b8d132349e970c-1496478973790.jpg"/>
                </div>
                <div className="file">
                  <img src="http://cafefcdn.com/thumb_w/660/2017/6a00d8351b44f853ef01b8d132349e970c-1496478973790.jpg"/>
                </div>
                <div className="file">
                  <img src="http://cafefcdn.com/thumb_w/660/2017/6a00d8351b44f853ef01b8d132349e970c-1496478973790.jpg"/>
                </div>
                <div className="file">
                  <img src="http://cafefcdn.com/thumb_w/660/2017/6a00d8351b44f853ef01b8d132349e970c-1496478973790.jpg"/>
                </div>

              </div>
            </div>

            <div className="item">
              <h5>
                Banner mặt tiền
                <i className="fa fa-pencil edit"></i>
                <a href=""> 54 cửa hàng </a>
              </h5>
              <div className="discription">
                Cướp ngân hàng là case study tuyệt vời để nghiên cứu về bản chất kinh tế học tồn tại trong hành vi phạm tội. Đó là những công việc được hoạch định trước, lợi ích được định lượng. Bọn tội phạm cũng phải đối mặt với một tính huống tiến thoái lưỡng nan: mỗi phút chúng ở trong ngân hàng lâu hơn, khả năng chúng bị bắt tăng lên.
                Nếu bạn là một nhà kinh tế học tò mò về những vụ cướp ngân hàng, không có phòng thí nghiệm nào tốt hơn Italy. Từ 2000-2006, quốc gia này đã phải đối mặt với hàng loạt vụ cướp ngân hàng mỗi năm, gần bằng số vụ cướp ngân hàng ở tất cả các quốc gia châu Âu còn lại cộng lại.
              </div>
              <div className="attach">
                <div className="file">
                  <img src="http://cafefcdn.com/thumb_w/660/2017/6a00d8351b44f853ef01b8d132349e970c-1496478973790.jpg"/>
                </div>
              </div>
            </div>

          </div>
        </div>

        <div className="group">
          <div className="title">
            <i className="fa fa-arrow-right"></i>
            <span>POSM</span>
            <i className="fa fa-pencil edit"></i>
          </div>
          <div className="expand">
            <div className="item add">
              <i className="fa fa-plus"></i>
              <span> Thêm hạng mục </span>
              <i className="fa fa-pencil edit"></i>
            </div>

            <div className="item">
              <div className="nothing">
              chưa có hạng mục nào được tạo trong nhóm này
              </div>
            </div>
          </div>

        </div>

        <div className="group">
          <div className="title">
            <i className="fa fa-arrow-right"></i>
            <span>POSM EDIT</span>
            <i className="fa fa-pencil edit"></i>
          </div>
          <div className="expand">
            <div className="item add">
              <i className="fa fa-plus"></i>
              <span> Thêm hạng mục </span>
              <i className="fa fa-pencil edit"></i>
            </div>

            <div className="item modify">
              <form >
                <label>Tên hạng mục</label>
                <input type="text" value={"Sắp xếp sản phẩm nổi bật"} />
                <label>Nội dung hạng mục</label>
                <textarea>
                  xcvxvxc
                </textarea>
                <div className="attach">
                  <div className="file">
                    <img src="http://cafefcdn.com/thumb_w/660/2017/6a00d8351b44f853ef01b8d132349e970c-1496478973790.jpg"/>
                    <div className="file-attach--overlay"></div>
                    <i className="fa fa-times"></i>
                  </div>
                  <div className="file">
                    <img src="https://scontent.fsgn3-1.fna.fbcdn.net/v/t1.0-9/16473339_875321125943051_4468272224378202260_n.jpg?oh=972f1d5c025775aefc566112f020167a&oe=59A394C0"/>
                    <div className="file-attach--overlay"></div>
                    <i className="fa fa-times"></i>
                  </div>
                  <div className="file">
                    <img src="https://scontent.fsgn3-1.fna.fbcdn.net/v/t1.0-9/16473339_875321125943051_4468272224378202260_n.jpg?oh=972f1d5c025775aefc566112f020167a&oe=59A394C0"/>
                    <div className="file-attach--overlay"></div>
                    <i className="fa fa-times"></i>
                  </div>

                  <div className="file-attach--new">
                    <i className="fa fa-plus"></i>
                    <span>Thêm hình ảnh hoặc tập tin</span>
                    <input accept="image/*" type="file" multiple="multiple" />
                  </div>

                </div>

                <label>Cửa hàng áp dụng</label>
                <div className="link">
                  54 cửa hàng được chọn
                </div>

                <div className="footer">
                  <button className="btn btn-cancel" type="button" ><i className="fa fa-times"></i>Hủy bỏ</button>
                  <button className="btn btn-info" type="button" ><i className="fa fa-check"></i>Thiết lập</button>
                </div>
              </form>
            </div>

          </div>
        </div>

      </div>
    )
  }
}
