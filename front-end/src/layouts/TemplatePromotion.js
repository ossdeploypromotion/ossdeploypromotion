import React, {Component} from 'react'
import Accordion from './Component/Accordion'
import Category from './Component/Category'
import './Promotion.css'

export default class TemplatePromotion extends Component{
	render(){
		return(
			<div className="">
				<div className="row lineHeader">
					<div className="pull-left col-md-9">NHÓM HẠNG MỤC</div>
					<div className="pull-right col-md-3">CỬA HÀNG HÀNG ÁP DỤNG</div>
				</div>
				<div className="addBannerGroup">
					<Category/>
				</div>c
				<div className="boxTemplate">
					<Accordion/>
				</div>
			</div>
		)
	}
}
