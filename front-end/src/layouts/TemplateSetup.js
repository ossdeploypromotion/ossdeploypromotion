import React, {Component} from 'react'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs'
import {Dialog, RaisedButton, FlatButton, TextField, Checkbox, Toggle, Select, InfoSelect, SearchBox, MultipleSelect, TagInput, DatePicker, RadioButtonGroup, RadioButton} from 'components'
import Section from './Component/Section'
import SectionChildLevel from './Component/SectionChildLevel'
import AccordionChildLevel from './Component/AccordionChildLevel'
import { Scrollbars } from 'react-custom-scrollbars'
import './react-tabs.css';
import './Promotion.css'

export default class TemplateSetup extends Component{
	constructor(){
		super();
		this.state = { tabIndex: 0 }
	}
	state = { select:{} }
	render(){
		const sampleOptionData = [{label:'Option 1',value:1},{label:'Option 2',value:2},{label:'Option 3',value:3}]
		return(
			<Tabs selectedIndex={this.state.tabIndex} onSelect={tabIndex => this.setState({ tabIndex })}>
			    <TabList>
			      	<Tab>Cấu hình khuyến mãi</Tab>
			      	<Tab>Các bước triển khai</Tab>
			      	<Tab>Thông báo/thảo luận</Tab>
			    </TabList>
			    <TabPanel>
			    	<div className="boxTabs">
				   		<div className="row">
				    		<div className="col-md-6">
				    			<label>Tên chương trình</label>
				    			<div className="resultText">KM Tặng SP Cho Khách Tham Dự Quay Số Mazda</div>
				    		</div>
				    		<div className="col-md-6">
				    			<label>Hạn chót triển khai</label>
				    			<div className="resultText">24/05/2017</div>
				    		</div>
				    		<div className="col-md-12">
				    			<label>Hướng dẫn nhanh</label>
				    			<div className="resultText">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec finibus tellus vel nisi ullamcorper, et dignissim nisl bibendum. Sed venenatis enim arcu, a tristique diam blandit in. Nam non sapien dictum, blandit neque in, consequat eros. Aenean fringilla magna urna, et rhoncus ligula laoreet a.</div>
				    		</div>
				    		<div className="col-md-12">
					    		<div className="attach">
					                <div className="file">
					                  	<img src="https://www.papamikecreations.com/uploads/image/301-agenda.png"/>
					                  	<div className="file-attach--overlay"></div>
		                				<i className="material-icons">close</i>
					                </div>
					                 <div className="file">
					                  	<img src="http://www.trilbybookclub.co.uk/resources/public/assets/press/TB_icon_100.png"/>
					                  	<div className="file-attach--overlay"></div>
		                				<i className="material-icons">close</i>
					                </div>
				                </div>
				            </div>
				    	</div>
				    </div>
				    <div className="boxTabs">
				    	<h6 className="headNote">Vui lòng chọn hạng mục sẽ triển khai cho chương trình này hoặc sử dụng lại hạng mục đã triển khai ở các KM trước đây hoặc  sử dụng tất cả hạng mục chuẩn theo cửa hàng.</h6>
				    	<div className="row">
				    		<div className="col-md-5">
				    			<p className="nTicket">Các hạng mục có thể sử dụng <span>4</span></p>
              					<SearchBox placeholder={'Tìm kiếm hạng mục'}/>
              					<Scrollbars className="scrollBox" style={{height:360}}>
              						<div className="boxSelTwoWay">
              							<h6>Nhóm hạng mục A</h6>
              							<ul>
              								<li>Hạng mục 1</li>
              								<li>Hạng mục 2</li>
              								<li>Hạng mục 4</li>
              								<li>Hạng mục 5</li>
              								<li>Hạng mục 6</li>
              							</ul>
              							<h6>Nhóm hạng mục B</h6>
              							<ul>
              								<li>Hạng mục 1</li>
              								<li>Hạng mục 2</li>
              								<li>Hạng mục 3</li>
              								<li>Hạng mục 4</li>
              								<li>Hạng mục 5</li>
              								<li>Hạng mục 6</li>
              								<li>Hạng mục 7</li>
              							</ul>
              						</div>
              					</Scrollbars>
				    		</div>
				    		<div className="col-md-2" style={{marginTop: 200, textAlign:"center"}}>
				    			<RaisedButton
							      	label="Sử dụng"
							      	labelStyle={{fontSize:14, textTransform: 'inherit', paddingLeft: 5, paddingRight: 10, borderRadius: 2}}
							      	style={{ marginBottom: 10 }}
							      	icon={<i className="material-icons" style={{color: "#757575"}}>keyboard_arrow_right</i>}/>
							    <RaisedButton
							      	label="Xoá bỏ"
							      	labelStyle={{fontSize:14, textTransform: 'inherit', borderRadius: 2}}
							      	icon={<i className="material-icons" style={{color: "#757575"}}>keyboard_arrow_left</i>}/>
				    		</div>
				    		<div className="col-md-5">
				    			<p className="nTicket">Các hạng mục có thể sử dụng <span>10</span></p>
				    			<SearchBox placeholder={'Tìm kiếm hạng mục'}/>
				    			<Scrollbars className="scrollBox" style={{height:360}}>
              						<div className="boxSelTwoWay">
              							<h6>Nhóm hạng mục A</h6>
              							<ul>
              								<li>Hạng mục 1</li>
              								<li>Hạng mục 2</li>
              								<li>Hạng mục 4</li>
              								<li>Hạng mục 5</li>
              								<li>Hạng mục 6</li>
              							</ul>
              						</div>
              					</Scrollbars>
				    		</div>
				    	</div>
				    	<div className="row linebtn">
							<div className="col-md-8">
								<RaisedButton
									style={{marginRight:10}}
							      	label="Hạng mục đã triển khai ở các KM trước đây"
							      	labelStyle={{fontSize:14, textTransform: 'capitalize', borderRadius: 2}}
							      	icon={<i className="material-icons" style={{color: "#757575"}}>access_time</i>}/>
							    <RaisedButton
							      	label="Sử dụng tất cả hạng mục chuẩn theo cửa hàng."
							      	labelStyle={{fontSize:14, textTransform: 'capitalize', borderRadius: 2}}
							      	icon={<i className="material-icons" style={{color: "#757575"}}>account_balance_wallet</i>}/>
							</div>
							<div className="col-md-4">
				    			<RaisedButton
				    				style={{float: 'right'}}
				    				primary={true}
							      	label="Áp dụng cho cừa hàng này"
							      	labelStyle={{fontSize:14, textTransform: 'capitalize', borderRadius: 2}}
							      	icon={<i className="material-icons" style={{color: "#fff"}}>check</i>}/>
							</div>
			    		</div>
				    </div>
			    </TabPanel>
			    <TabPanel>
			    	<div className="boxTabs">
				   		<div className="row">
				    		<div className="col-md-6">
				    			<label>Tên chương trình</label>
				    			<div className="resultText">KM Tặng SP Cho Khách Tham Dự Quay Số Mazda</div>
				    		</div>
				    		<div className="col-md-6">
				    			<label>Hạn chót triển khai</label>
				    			<div className="resultText">24/05/2017</div>
				    		</div>
				    		<div className="col-md-12">
				    			<label>Hướng dẫn nhanh</label>
				    			<div className="resultText">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec finibus tellus vel nisi ullamcorper, et dignissim nisl bibendum. Sed venenatis enim arcu, a tristique diam blandit in. Nam non sapien dictum, blandit neque in, consequat eros. Aenean fringilla magna urna, et rhoncus ligula laoreet a.</div>
				    		</div>
				    		<div className="col-md-12">
					    		<div className="attach">
					                <div className="file">
					                  	<img src="https://www.papamikecreations.com/uploads/image/301-agenda.png"/>
					                  	<div className="file-attach--overlay"></div>
		                				<i className="material-icons">close</i>
					                </div>
					                 <div className="file">
					                  	<img src="http://www.trilbybookclub.co.uk/resources/public/assets/press/TB_icon_100.png"/>
					                  	<div className="file-attach--overlay"></div>
		                				<i className="material-icons">close</i>
					                </div>
				                </div>
				            </div>
				    	</div>
			    	</div>
			    	<div className="boxTabs">
			    		<div className="row">
			    			<div className="col-md-3 searchTabs">
			    				<label className="lbltabs">Khu vực</label>
					            <Select
					            	className="selectTabs"
					            	value={this.state.dijt}
					               	onChange={(option)=>{this.setState({select:{...this.state.select,dijit:option}})}}
					              	placeholder={'Toàn quốc'}
					              	options={sampleOptionData}/>
			    			</div>
			    			<div className="col-md-3 bControl searchTabs" style={{height: 0}}>
			    				<label className="lbltabs">Tìm kiếm cửa hàng</label>
			    				<SearchBox placeholder={'Nhập tên hoặc mã cửa hàng'}/>
			    			</div>
			    			<div className="col-md-6">
			    				<RaisedButton
				    				primary={true}
				    				className="pull-right"
							      	label="Áp dụng cho cừa hàng này"
							      	labelStyle={{fontSize:14, textTransform: 'capitalize', borderRadius: 2}}
							      	style={{marginTop:10}}
							      	icon={<i className="material-icons" style={{color: "#fff"}}>assignment_turned_in</i>}/>
			    			</div>
			    		</div>
			    	</div>
			    	<div className="row lineHeadCollapse">
	    				<div className="col-md-1 text-right colSTT">STT</div>
	    				<div className="col-md-4 colShop">CỬA HÀNG/HẠNG MỤC</div>
	    				<div className="col-md-4 colReport">BÁO CÁO TRIỂN KHAI</div>
	    				<div className="col-md-3 colUpdat">TRIỂN KHAI / CẬP NHẬT LẦN CUỐI</div>
	    			</div>
		    		<Section className="schild" title={
		    			<div className="row">
							<div className="col-md-1 text-right titleBold">1</div>
		    				<div className="col-md-4 titleBold">CH024 - 313 Nguyễn Thị Thập - HCM</div>
		    				<div className="col-md-4">

		    				</div>
		    				<div className="col-md-3"><span className="cricle sYellow"></span>1/2 nhóm</div>
		    			</div>
		    		}>
		    			<AccordionChildLevel/>
	        		</Section>

			    </TabPanel>
			    <TabPanel>
			      <h2>Any content 3</h2>
			    </TabPanel>
		  	</Tabs>
		)
	}
}
