import {GLOBAL_STATE_LOADING_SPNNER, GLOBAL_STATE_HIDE_LOADING_SPNNER} from './globalStateReducer'
import config from 'config'

const LOAD_LUNCH_CONFIG_SUCCESS = 'LOAD_LUNCH_CONFIG_SUCCESS'
const LOAD_LUNCH_CONFIG_FAILED = 'LOAD_LUNCH_CONFIG_FAILED'

export default (state = {}, action) => {
  switch (action.type) {

    case LOAD_LUNCH_CONFIG_SUCCESS: {
      return {
        ...state,
        config: action.body.records
      }
    }
  }
}

export const loadBookingLunch = (month, year) => ({
  types: [GLOBAL_STATE_LOADING_SPNNER, LOAD_LUNCH_CONFIG_SUCCESS, LOAD_LUNCH_CONFIG_FAILED],
  promise: (client) => (client.get(`${config.apiEndPoint}/lunch/config/${month}/${year}`)),
  onSuccess: ({dispatch, getState}) => {
    // const config = getState().lunch.config;
    // dispatch(checklunchmanager());
    // dispatch(loadBookingLunchData(month,year));

    dispatch({type:GLOBAL_STATE_HIDE_LOADING_SPNNER})

  },
});
