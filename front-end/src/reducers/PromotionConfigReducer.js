import {GLOBAL_STATE_LOADING_SPNNER, GLOBAL_STATE_HIDE_LOADING_SPNNER} from './globalStateReducer'
const LOAD_PROMOTION_CONFIG_SUCCESS = 'LOAD_PROMOTION_CONFIG_SUCCESS'
const LOAD_PROMOTION_CONFIG_FAILED = 'LOAD_PROMOTION_CONFIG_FAILED'
import {browserHistory} from 'react-router'

export default (state = {},action) => {
  switch (action.type) {
    case LOAD_PROMOTION_CONFIG_SUCCESS: {
      return {
        ...state,
        ...action.body
      }
    }
    default:{
      return state
    }

  }
}

export const loadPromotionConfig = (promotionId) => ({
  types: [GLOBAL_STATE_LOADING_SPNNER,LOAD_PROMOTION_CONFIG_SUCCESS,LOAD_PROMOTION_CONFIG_FAILED],
  promise: (client) => (client.get(`${config.apiEndPoint}promotionconfig/getPromotionInfor/${promotionId}`)),
  onSuccess: ({dispatch, getState}) => {
      dispatch({type:GLOBAL_STATE_HIDE_LOADING_SPNNER})
  },
});
