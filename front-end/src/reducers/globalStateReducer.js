/**
 * Created by W540 on 2/23/2017.
 */
export const GLOBAL_STATE_LOADING_SPNNER = 'GLOBAL_STATE_LOADING_SPNNER'
export const GLOBAL_STATE_HIDE_LOADING_SPNNER = 'GLOBAL_STATE_HIDE_LOADING_SPNNER'
export default (state={},action) => {
  switch (action.type){
    case GLOBAL_STATE_LOADING_SPNNER:{
      return{
        loading:true
      }
    }
    case GLOBAL_STATE_HIDE_LOADING_SPNNER:{
      return{
        loading:false
      }
    }
    default :{
      return state
    }
  }
}
export const showSpinerLoading = (taskName="default-task") => ({
  type:GLOBAL_STATE_LOADING_SPNNER,
  taskName
})
export const hideSpinerLoading = (taskName="default-task") => ({
  type:GLOBAL_STATE_HIDE_LOADING_SPNNER,
  taskName
})
