/**
 * Created by W540 on 12/30/2016.
 */
import config from 'config'
const MENU_LOANDING = "MENU_LOANDING";
const MENU_LOAD_SUCCESS = "MENU_LOAD_SUCCESS";
const MENU_LOAD_FAILED = "MENU_LOAD_FAILED";
const TOP_MENU_LOAD_SUCCESS = "TOP_MENU_LOAD_SUCCESS";

export default (state = {}, action) =>{
  switch (action.type){
    case MENU_LOAD_SUCCESS:{
      return {
        ...state,
        listMenu:action.body
      }
    }
    case TOP_MENU_LOAD_SUCCESS:{
      return {
        ...state,
        listTopLeftMenu:action.body
      }
    }
    default:{
      return state;
    }
  }
}
export const loadMenuLink = () => ({
  types:[MENU_LOANDING,MENU_LOAD_SUCCESS,MENU_LOAD_FAILED],
  promise:(client) => (client.get(config.apiEndPoint+'api/okr/leftmenu'))
})
export const loadTopLeftMenuLink = () => ({
  types:[MENU_LOANDING,TOP_MENU_LOAD_SUCCESS,MENU_LOAD_FAILED],
  promise:(client) => (client.get(config.apiEndPoint+'api/okr/leftmenu', {params: {leftmenutype: 2}}))
})

