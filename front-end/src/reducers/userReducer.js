/**
 * Created by W540 on 1/3/2017.
 */
import { loadMenuLink } from './menuReducer'
import { makeToast } from 'utils/Toast'
import { reRender } from 'main'
//import { sendEvent } from 'utils/Analytic'
const USER_LOADING = 'USER_LOADING'
const USER_LOAD_ERROR = 'USER_LOAD_ERROR'
const USER_LOAD_INFO_SUCCESS = 'USER_LOAD_INFO_SUCCESS'
const USER_FOLLOW_OBJECTIVE_SUCCESS = 'USER_FOLLOW_OBJECTIVE_SUCCESS'
const USER_UN_FOLLOW_OBJECTIVE_SUCCESS = 'USER_UN_FOLLOW_OBJECTIVE_SUCCESS'
const USER_LOAD_FOLLOW_OBJECTIVE_SUCCESS = 'USER_LOAD_FOLLOW_OBJECTIVE_SUCCESS'
const USER_LOAD_FOLLOW_USER_SUCCESS = 'USER_LOAD_FOLLOW_USER_SUCCESS'
const USER_LOAD_LIST_COMPANY_SUCCESS = 'USER_LOAD_LIST_COMPANY_SUCCESS'
const User_CHANGE_COMPANY_SUCESS = 'User_CHANGE_COMPANY_SUCESS'
const USER_ADD_FOLLOW_USER_SUCCESS = 'USER_ADD_FOLLOW_USER_SUCCESS'
const USER_DELETE_FOLLOW_USER_SUCCESS = 'USER_DELETE_FOLLOW_USER_SUCCESS'
const USER_LOGIN_SUCCESS = 'USER_LOGIN_SUCCESS'
const USER_LOGOUT_SUCCESS = 'USER_LOGOUT_SUCCESS'
const USER_LOAD_FOLLOW_DEPARTMENT_SUCCESS = 'USER_LOAD_FOLLOW_DEPARTMENT_SUCCESS'
const USER_ADD_FOLLOW_DEPARTMENT_SUCCESS = 'USER_ADD_FOLLOW_DEPARTMENT_SUCCESS'
const USER_DELETE_FOLLOW_DEPARTMENT_SUCCESS = 'USER_DELETE_FOLLOW_DEPARTMENT_SUCCESS'
const USER_ADD_PIN_DEPARTMENT_SUCCESS = 'USER_ADD_PIN_DEPARTMENT_SUCCESS'
const USER_DELETE_PIN_DEPARTMENT_SUCCESS = 'USER_DELETE_PIN_DEPARTMENT_SUCCESS'
const FOLLOW_SUCCESS = 'Theo dõi thành công'
const FOLLOW_OBJECTIVE_SUCCESS = 'Theo dõi thành công'
const UNFOLLOW_OBJECTIVE_SUCCESS = 'Bỏ theo dõi thành công'
const UNFOLLOW_SUCCESS = 'Bỏ theo dõi thành công'

import { SubmissionError } from 'redux-form'
import { browserHistory } from 'react-router'
//import { loadUserGroup } from './okrGroupReducer'
import config from 'config'
import { setApiToken } from 'store/ApiClient'
//import { setUserId } from 'utils/Analytic'

export default (state = {}, action) => {
  switch (action.type) {
    case USER_LOAD_INFO_SUCCESS: {
      return {
        ...state,
        ...action.result
      }
    }
    case USER_LOGIN_SUCCESS: {
      return {
        ...state,
        authenticated: true
      }
    }
    case USER_LOGOUT_SUCCESS: {
      return {
        authenticated: false
      }
    }
    case USER_LOAD_FOLLOW_USER_SUCCESS: {
      const followUsers = action.body.reduce((last, item, index) => ({ ...last, [item.followUser.username]:item }), {})
      return {
        ...state,
        followUsers
      }
    }
    case USER_LOAD_FOLLOW_DEPARTMENT_SUCCESS: {
      const followDepartments = action.body.reduce((last, item, index) => ({ ...last, [item.department.id]:item }), {})
      return {
        ...state,
        followDepartments
      }
    }
    case USER_LOAD_FOLLOW_OBJECTIVE_SUCCESS: {
      const followObjectives = action.body.reduce((last, item, index) => ({ ...last, [item.objectiveId]:item }), {})
      return {
        ...state,
        followObjectives
      }
    }
    case USER_LOADING: {
      return {
        ...state,
        loading: true
      }
    }
    case USER_LOAD_LIST_COMPANY_SUCCESS: {
      return {
        ...state,
        listCompany:action.body,
        loading: false
      }
    }
    default: {
      return state
    }
  }
}
export const fetchUserInfo = () => ({ dispatch, getState, client }) => {
  dispatch({ type: USER_LOADING })
  client.get(`${config.apiEndPoint}api/authenticated-user`).then(({ body }) => {
    dispatch({ type: USER_LOAD_INFO_SUCCESS, result: body })
    if (body.user.needSlectCompany) {
      browserHistory.push(`${config.appContext}selectcompany`)
    }
    //sendEvent({ category: 'User', action: 'Get user detail', label: `${body.user.username}-${body.user.company.name}` })
    //setUserId(body.user.username, body.user.fullname)
    // dispatch(fetchUserFollowObjective())
  })
}

export const logIn = ({ userName, Password }) => ({ dispatch, getState, client }) => {
  client.post(`${config.apiEndPoint}api/login`).then(({ body, status, header }) => {
    dispatch({ type: USER_LOGIN_SUCCESS })
  })
}
export const logout = () => ({ dispatch, getState, client }) => {
  localStorage.removeItem('x-auth-token')
  dispatch({ type: USER_LOGOUT_SUCCESS })
  window.location.href = config.logout
}
// redux-form function
const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))
export const submitLogin = (user, dispatch) => {
  return new Promise((resolve, reject) => {
    dispatch({ type: USER_LOADING })
    // return new Promise(resolve => {
    dispatch(({ dispatch, getState, client }) => {
      client.post(`${config.apiEndPoint}api/login`, { data: user }).then(({ body, status, header }) => {
        dispatch({ type: USER_LOGIN_SUCCESS })
        browserHistory.push(config.appContext)
        resolve(body)
      }).catch(({ body, error }) => {
        // throw new SubmissionError({ password: 'Wrong username or password',username: 'Wrong username or password' , _error: 'Login failed!' })
        reject({ password: 'Wrong password', _error: 'Login failed!' })
      })
    })
    // })
  }).then(() => {
  }).catch((error) => {
    throw new SubmissionError(error)
  })
}
export const fetchUserFollowObjective = () => ({ dispatch, getState }) => {
  dispatch({
    types:[USER_LOADING, USER_LOAD_FOLLOW_OBJECTIVE_SUCCESS, USER_LOAD_ERROR],
    promise:(client) => (client.get(`${config.apiEndPoint}api/users/getListFollowObjective`, { params:{} }))
  })
}
export const fetchFollowUser = () => ({
  types:[USER_LOADING, USER_LOAD_FOLLOW_USER_SUCCESS, USER_LOAD_ERROR],
  promise:(client) => (client.get(`${config.apiEndPoint}api/okr/followuser`, { params:{} }))
})
export const fetchFollowDepartment = () => ({
  types:[USER_LOADING, USER_LOAD_FOLLOW_DEPARTMENT_SUCCESS, USER_LOAD_ERROR],
  promise:(client) => (client.get(`${config.apiEndPoint}api/okr/listfollowdepartment`, { params:{} }))
})
export const fetchListCompany = () => ({
  types:[USER_LOADING, USER_LOAD_LIST_COMPANY_SUCCESS, USER_LOAD_ERROR],
  promise:(client) => (client.get(`${config.apiEndPoint}api/users/listcompany`, { params:{} }))
})
export const addFollowUser = (userid) => ({
  types:[USER_LOADING, USER_ADD_FOLLOW_USER_SUCCESS, USER_LOAD_ERROR],
  promise:(client) => (client.post(`${config.apiEndPoint}api/okr/followuser`, { params:{ userid } })),
  onSuccess: ({ dispatch, getState }) => {
    dispatch(fetchFollowUser())
    makeToast(FOLLOW_SUCCESS)
  }
})
export const deleteFollowUser = (userid) => ({
  types:[USER_LOADING, USER_DELETE_FOLLOW_USER_SUCCESS, USER_LOAD_ERROR],
  promise:(client) => (client.del(`${config.apiEndPoint}api/okr/followuser`, { params:{ userid } })),
  onSuccess: ({ dispatch, getState }) => {
    dispatch(fetchFollowUser())
    makeToast(UNFOLLOW_SUCCESS)
  }
})

export const followObjective = (objectiveId) => ({
  types: [USER_LOADING, USER_FOLLOW_OBJECTIVE_SUCCESS, USER_LOAD_ERROR],
  promise: (client) => (client.post(`${config.apiEndPoint}api/okr/objective/follow/${objectiveId}`)),
  onSuccess:({ dispatch, getState }) => {
    dispatch(fetchUserFollowObjective())
    makeToast(FOLLOW_OBJECTIVE_SUCCESS)
  }
})
export const unFollowObjective = (objectiveId) => ({
  types: [USER_LOADING, USER_UN_FOLLOW_OBJECTIVE_SUCCESS, USER_LOAD_ERROR],
  promise: (client) => (client.del(`${config.apiEndPoint}api/okr/objective/follow/${objectiveId}`)),
  onSuccess:({ dispatch, getState }) => {
    dispatch(fetchUserFollowObjective())
    makeToast(UNFOLLOW_OBJECTIVE_SUCCESS)
  }
})

export const addFollowDepartment = (departmentid) => ({
  types:[USER_LOADING, USER_ADD_FOLLOW_DEPARTMENT_SUCCESS, USER_LOAD_ERROR],
  promise:(client) => (client.post(`${config.apiEndPoint}api/okr/followdepartment`, { params:{ departmentid } })),
  onSuccess: ({ dispatch, getState }) => {
    dispatch(fetchFollowDepartment())
    makeToast(FOLLOW_SUCCESS)
  }
})
export const deleteFollowDepartment = (departmentid) => ({
  types:[USER_LOADING, USER_DELETE_FOLLOW_DEPARTMENT_SUCCESS, USER_LOAD_ERROR],
  promise:(client) => (client.del(`${config.apiEndPoint}api/okr/followdepartment`, { params:{ departmentid } })),
  onSuccess: ({ dispatch, getState }) => {
    dispatch(fetchFollowDepartment())
    makeToast(UNFOLLOW_SUCCESS)
  }
})
export const changeUserCompany = (companyId, onSuccess) => ({
  types:[USER_LOADING, User_CHANGE_COMPANY_SUCESS, USER_LOAD_ERROR],
  promise:(client) => (client.post(`${config.apiEndPoint}api/changeusercompany`, { params:{ companyId } })),
  onSuccess: ({ dispatch, getState, result }) => {
    browserHistory.push({ pathname:`${config.appContext}home`,
      query:undefined })
    setApiToken(result.body)
    // dispatch(fetchUserInfo());
    // dispatch(loadMenuLink());
    // dispatch(fetchUserFollowObjective());
    // dispatch(fetchFollowUser());
    // dispatch(fetchListCompany());
    if (onSuccess) {
      onSuccess()
    }
    reRender()
  }
})
