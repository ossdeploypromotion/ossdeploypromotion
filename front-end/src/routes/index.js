// We only need to import the modules necessary for initial render
import CoreLayout from '../layouts/CoreLayout/CoreLayout'
import LoginPage from 'layouts/Login/LoginPage'
import SelectCompany from 'layouts/SelectCompany/SelectCompany'
import { injectReducer } from 'store/reducers'
import { setApiToken } from 'store/ApiClient'
import config from 'config'

/*  Note: Instead of using JSX, we recommend using react-router
 PlainRoute objects to build route definitions.   */
var currentTime = new Date()
function getCurrentQuater () {
  const quarter = parseInt((currentTime.getMonth() + 1) / 4) + 1
  return quarter
}
function getCurrentYear () {
  return currentTime.getFullYear()
}
function checkAndReplaceToQuarterAndYear (nextState, replace, pathName) {
  let { quarter, year } = nextState.location.query
  //const context = config.appContext
  if (pathName) {
    replace({
      pathname: pathName ? pathName : nextState.location.pathname,
      query: nextState.location.query
    })
  }
}
function setAppTokenAndRedirectToHome (nextState, replace) {
  let token = nextState.location.query.token
  setApiToken(token)
}
const context = config.appContext
export const createRoutes = (store) => ({
  path: '/',

      indexRoute: {
        onEnter: (nextState, replace) => {
          checkAndReplaceToQuarterAndYear(nextState, replace, 'home')
        }
      },
      childRoutes: [
        {
          path: 'login',
          component: LoginPage
        },
        {
          path: 'selectcompany',
          component: SelectCompany
        },
        {
          path: 'createtoken',
          onEnter: (nextState, replace) => {
            setAppTokenAndRedirectToHome(nextState, replace)
            checkAndReplaceToQuarterAndYear(nextState, replace, 'home')
          }
        },

        {
          component: CoreLayout,
          childRoutes: [{
            path:'home',
            getComponent (nextState, cb) {
              require.ensure([], (require) => {
                const Announce = require('layouts/Home/Home').default
                // const annReducer = require('reducers/annReducer').default
                // const deptReducer = require('reducers/deptReducer').default
                // injectReducer(store, { key: 'ann', reducer: annReducer })
                // injectReducer(store, { key: 'dept', reducer: deptReducer })
                cb(null, Announce)
              }, 'announce-home')
            }
          },{
            path: 'setuppromotion',
            getComponent (nextState, cb) {
              require.ensure([], (require) => {
                const SetupPromotion = require('layouts/SetupPromotion/SetupPromotion').default
                const SetupPromotionReducer = require('reducers/SetupPromotionReducer').default;
                // injectReducer(store, {key: 'setup', reducer: SetupPromotionReducer})
                cb(null, SetupPromotion)
              }, 'setuppromotion')
            }
          },{
            path: 'annpromotion',
            getComponent (nextState, cb) {
              require.ensure([], (require) => {
                const AnnPromotion = require('layouts/Annoucement/Ann').default
                const AnnPromotionReducer = require('reducers/AnnPromotionReducer').default;
                // injectReducer(store, {key: 'setup', reducer: SetupPromotionReducer})
                cb(null, AnnPromotion)
              }, 'annpromotion')
            }
          },{
            path: 'templatecategory',
            getComponent (nextState, cb) {
              require.ensure([], (require) => {
                const TemplateCategory = require('layouts/TemplateCategory/TemplateCategory').default
                const TemplateCategoryReducer = require('reducers/TemplateCategoryReducer').default;
                // injectReducer(store, {key: 'setup', reducer: SetupPromotionReducer})
                cb(null, TemplateCategory)
              }, 'templatecategory')
            }
          },{
            path: 'loadPromotionConfig',
            getComponent (nextState, cb) {
              require.ensure([], (require) => {
                const LoadPromotionConfig = require('layouts/SetupPromotion/LoadPromotionConfig').default
                const LoadPromotionConfigReducer = require('reducers/PromotionConfigReducer').default;
                injectReducer(store, {key: 'config', reducer: LoadPromotionConfigReducer})
                cb(null, LoadPromotionConfig)
              }, 'loadPromotionConfig')
            }
          }]
        },{
          path:'*',
          onEnter: (nextState, replace) => {
            checkAndReplaceToQuarterAndYear(nextState, replace, 'home')
          }
        }]
})

/*  Note: childRoutes can be chunked or otherwise loaded programmatically
 using getChildRoutes with the following signature:
 getChildRoutes (location, cb) {
 require.ensure([], (require) => {
 cb(null, [
 // Remove imports!
 require('./Counter').default(store)
 ])
 })
 }
 However, this is not necessary for code-splitting! It simply provides
 an API for async route definitions. Your code splitting should occur
 inside the route `getComponent` function, since it is only invoked
 when the route exists and matches.
 */

export default createRoutes
