$(function () {
    //Initialize Select2 Elements
    $(".select2").select2();

    $('.reservation').daterangepicker();
    //Date range picker with time picker
    $('.reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});

    $('.timepicker').timepicker({});
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass: 'iradio_minimal-blue'
    });
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
        checkboxClass: 'icheckbox_minimal-red',
        radioClass: 'iradio_minimal-red'
    });
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green'
    });

    $('.datepicker').datepicker({
        autoclose: true
    });
    // oss menu
    $(".oss-sidebar-menu.mini").hover(function () {
        $(this).removeClass("mini")
    }, function () {
        $(this).addClass("mini")
    })
    $("[menuid]").click(function (e) {
        e.preventDefault();
        var id = $(this).attr("menuid");
        $("[submenuid]").removeClass("active");
        $("[submenuid=" + id + "]").addClass("active");
        $("[menuid]").removeClass("active");
        $("[menuid=" + id + "]").addClass("active");
        $(".oss-sidebar-menu").addClass("mini")
    });                
});

$(document).ready(function(){
    $(".dropdown").on("hide.bs.dropdown", function(){
        $(".fc-action").html('<i class="fa fa-caret-down"></i>');
    });
    $(".dropdown").on("show.bs.dropdown", function(){
        $(".fc-action").html('<i class="fa fa-caret-up"></i>');
    });
});

$(document).on('click', '.panel-heading .panel-title', function(e){
    var $this = $(this);
    if(!$this.hasClass('panel-collapsed')) {
        $this.parents('.panel').find('.panel-body').slideUp();
        $this.addClass('panel-collapsed');
        $this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
    } else {
        $this.parents('.panel').find('.panel-body').slideDown();
        $this.removeClass('panel-collapsed');
        $this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
    }
});

$(document).on('click', '.km-hiddenhm', function(e){
    var $this = $(this);
    if (!$this.hasClass('hidden')){
        $this.parents('.km-panel-container').find('.km-hiddenhm').slideUp();
        $this.addClass('panel-collapsed');
        $this.parents('.km-panel-container').find('.km-showhm').removeClass('hidden').slideDown();   
    } else {}
});
$(document).on('click', '.km-btn-cancel', function(e){
    var $this = $(this);
    if (!$this.hasClass('hidden')){
        $this.parents('.km-panel-container').find('.km-showhm').addClass('hidden').slideUp();
        $this.addClass('panel-collapsed');
        $this.parents('.km-panel-container').find('.km-hiddenhm').slideDown();  
    } else {}
});

$("#optgroup").multiselect();

