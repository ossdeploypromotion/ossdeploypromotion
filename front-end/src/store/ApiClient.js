import superagent from 'superagent';
import {browserHistory} from 'react-router';
import config from 'config'
import {GLOBAL_STATE_HIDE_LOADING_SPNNER} from 'reducers/globalStateReducer';
import {makeToastError} from 'utils/Toast'

//import {LinkCreator} from 'utils'
//import {push } from 'react-router-redux'
// import config from '../config';
const methods = ['get', 'post', 'put', 'patch', 'del'];
const ssoDomain = config.ssoDomain;
const apiDomain = 'http://' + window.location.hostname + config.apiAuthenEndPoint
const enableTimeOut = config.enableTimeout
const ajaxTimeout = 5000
// function formatUrl(path) {
//   const adjustedPath = path[0] !== '/' ? '/' + path : path;
//   if (__SERVER__) {
//     // Prepend host and port of the API server to the path.
//     return 'http://' + config.apiHost + ':' + config.apiPort + adjustedPath;
//   }
//   // Prepend `/api` to relative URL, to proxy to API server.
//   return '/api' + adjustedPath;
// }
let instance;

export default class ApiClient {
  setDispatch(dispatch) {
    this.dispatch = dispatch;
  }

  constructor(req) {

    methods.forEach((method) =>
      this[method] = (path, {params, data, headers, attachFiles,onUploadProgress} = {}) => new Promise((resolve, reject) => {
        //const request = superagent[method](formatUrl(path));
        const request = superagent[method](path);
        if (attachFiles) {
          // headers = {...headers, ["Content-Type"]: 'multipart/form-data'}
          let totalSize = 0;
          attachFiles.forEach(file => {
            totalSize += file.size
            request.attach("file", file)
          })
          onUploadProgress && totalSize > 0 && request.on('progress', function(e) {
            onUploadProgress(e.percent)
            //console.log('Percentage done: ', e.percent);
          })
          if (totalSize > config.maxUploadSize) {
            this.dispatch({type: GLOBAL_STATE_HIDE_LOADING_SPNNER})
            makeToastError("Chỉ được tải lên tệp dưới 100MB", {timeout: 15000})
            return reject({body: {}, err: {}})
          }
          if (params) {
            Object.keys(params).forEach(key => {
              if (params[key] != undefined) {
                request.field(key, params[key]);
              }
            })
          }
        } else {
          if (params) {
            request.query(params);
          }
        }


        let authenToken = localStorage.getItem("x-auth-token");
        headers = {...headers, ["x-auth-token"]: authenToken}

        if (enableTimeOut && (!attachFiles || attachFiles.length == 0)) {
          request.timeout({
            response: ajaxTimeout,
          })
        }

        if (data) {
          //console.log('send request to ' + path);
          request.send(data);
        }

        request.set(headers)
        request.end((err, {status, body, header} = {}) => {
          // console.log('request recieve ' + body + err);
          if (err) {
            console.error(err.message)
          }
          if (status == 401 || status == 403) {
            if (__PROD__) {
              window.location.replace(`${ssoDomain}/redirect.html?redirect=${apiDomain}`);
            } else {
              window.location.replace(`${ssoDomain}/redirect.html?redirect=${apiDomain}`);
              //browserHistory.push(config.appContext+'login')
              return
            }
          }
          if (status == 500) {
            this.dispatch({type: GLOBAL_STATE_HIDE_LOADING_SPNNER})
            makeToastError("Có tính năng lổi trong hệ thống, xin hãy liên hệ với đội phát triễn ANNs", {timeout: 15000})
            return reject({body, err})
          }
          if ((err && err.timeout) || status == 404) {
            this.dispatch({type: GLOBAL_STATE_HIDE_LOADING_SPNNER})
            makeToastError("Máy chủ không phản hồi! xin hãy kiểm tra đường truyền và thử lại", {timeout: 5000})
            return reject({body, err})
          }
          if (header) {
            let authenToken = localStorage.getItem("x-auth-token");
            let newToken = header["x-auth-token"];
            if (newToken && newToken != authenToken) {
              localStorage.setItem("x-auth-token", newToken)
            }
          }

          return err ? reject({body, err}) : resolve({body, status});
        });
      }));
    if (!instance) {
      instance = this;
    }
  }

  /*
   * There's a V8 bug where, when using Babel, exporting classes with only
   * constructors sometimes fails. Until it's patched, this is a solution to
   * "ApiClient is not defined" from issue #14.
   * https://github.com/erikras/react-redux-universal-hot-example/issues/14
   *
   * Relevant Babel bug (but they claim it's V8): https://phabricator.babeljs.io/T2455
   *
   * Remove it at your own risk.
   */
  empty() {
  }
};
export const getInstance = () => (instance);
export const setApiToken = (token) => {
  if (token) {
    localStorage.setItem("x-auth-token", token)
  }
}
