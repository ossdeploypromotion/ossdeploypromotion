/**
 * Created by W540 on 3/13/2017.
 */
var currentTime = new Date()
export function getCurrentQuater() {
  const quarter = parseInt((currentTime.getMonth() + 1) / 4) + 1;
  return quarter;
}
export function getCurrentYear() {
  return currentTime.getFullYear();
}

export const getLastDateOfQuater = ({year,quarter}) => {
  return new Date(year,quarter*3-1,30);
}
export const getMinTimeOfQuarter = ({year,quarter}) => {
  const currentQuarter = getCurrentQuater();
  const currentYear = getCurrentYear();
  if(quarter == currentQuarter && year == currentYear){
    return new Date();
  }
  return new Date(year,(quarter-1)*3,1);
}
function getParameterByName(name, url) {
  if (!url) {
    url = window.location.href;
  }
  name = name.replace(/[\[\]]/g, "\\$&");
  var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
    results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, " "));
}
