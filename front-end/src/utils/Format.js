/**
 * Created by W540 on 2/24/2017.
 */
import React from 'react'
import moment from 'moment'
import {dateLeft} from 'utils/TimeUtility'
export const formatDate = (date) => {
  const d = moment(date);
  const datestring = ("0" + d.date()).slice(-2) + "-" + ("0"+(d.month()+1)).slice(-2) + "-" +
    d.year() ;
  return datestring;
}
export const formatDateAndMonth = (date) => {
  const d = new moment(date);
  const datestring = ("0" + d.date()).slice(-2) + "/" + ("0"+(d.month()+1)).slice(-2)
  return datestring;
}
export const formatObserveRight = (right) => {
  switch (right){
    case 1:{
      return "Tất cả mọi người"
    }
    case 2:{
      return "Cấp trên có thể xem"
    }
    case 3:{
      return "Cùng cấp bộ phận và cấp trên"
    }
    case 4:{
      return "Chỉ mình tôi"
    }
    default:{
      return "Chưa đặt quyền theo dỏi"
    }
  }
}
export const formatDoubleFix2 = (number) => {
  return number && number.toFixed(2)
}
export const getDeadlineLabel = (deadLine) => {
  const dayLeft = dateLeft(deadLine);
  const date = formatDateAndMonth(deadLine);
  const dateClass = dayLeft > 10 ? "far" : dayLeft > 3 ? "normal" : dayLeft > 0 ? "in-coming" : dayLeft > -30 ? "passed" : "far"
  return <span className={`okr-date-label ${dateClass}`}><i className="fa fa-calendar"></i> {date}</span>
}

