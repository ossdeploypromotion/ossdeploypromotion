import React from 'react'
import config from 'config'
export const createObjectiveDetailLink = (objective, query) => {
  return (objective && {
    pathname: config.appContext + 'detail',
    query: { ...query, objectId: objective.objectiveId, quarter: objective.quarter, year: objective.year }
  })
}
export const createOkrUserLink = (user, query, currentUsername) => (currentUsername == user.username ? createMyOkrLink(query) : {
  pathname: config.appContext + 'setup/user/manage', query: { ...query, userid: user.username }
})
export const createMyOkrLink = (query) => ({
  pathname: config.appContext + 'setup/myokr/manage', query
})
export const createOkrStructureLink = ({ username, company, department, quarter, year }) => ({
  pathname: `${config.appContext}okrstructure`, query: { username, company, department, quarter, year }
})
export const createOkrDepartmentLink = (departmentid, query) => ({
  pathname: `${config.appContext}setup/department/manage`, query: { ...query, departmentid }
})
