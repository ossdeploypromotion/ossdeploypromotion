import moment from 'moment'

export const timeAgo = (time) => {
  	var now = moment();
	var timeMoment = moment(time);
	var ago = now.diff(timeMoment,'days');
	if(ago > 2){
		return timeMoment.format("DD-MM-YYYY")
	}
	if (ago>0){
	    return (ago +" ngày trước");
	}
	ago = now.diff(timeMoment,'hours');
	if(ago>0){
		return `${ago} giờ trước`;
	}
	return (now.diff(timeMoment,'minutes') + " phút trước");
}
export const dateLeft = (time)=>{
  var now = moment();
  var timeMoment = moment(time);
  var ago = timeMoment.diff(now,'days');
  return ago
}
