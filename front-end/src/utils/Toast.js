/**
 * Created by W540 on 3/14/2017.
 */
let callToast;
export const makeToast = (message) => {
  callToast(message,"success")
}
export const makeToastError = (message,obtions) => {
  callToast(message,"error",obtions)
}
export const makeToastWarning = (message,obtions) => {
  callToast(message,"warning",obtions)
}
export const registerToastHandle = (toastFunction) => {
  callToast = toastFunction
}
