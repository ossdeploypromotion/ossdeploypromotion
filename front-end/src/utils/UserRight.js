/**
 * Created by W540 on 2/17/2017.
 */

import {store, reRender} from 'main'
const cheatUser = {
  'SC00041': true,
  'SC00038': true
}

var currentTime = new Date()
function getCurrentQuater() {
  const quarter = parseInt((currentTime.getMonth() + 1) / 4) + 1;
  return quarter;
}
function getCurrentYear() {
  return currentTime.getFullYear();
}
const currentQuarter = getCurrentQuater();
const currentYear = getCurrentYear();
const getUser = () => {
  return store.getState().user.user
}
const getManageDepartment = () => {
  return store.getState().user.manageDepartment
}
const getManageCompany = () => {
  return store.getState().user.manageCompany
}
export const setCheatCode = (mode) => {
  if (mode) {
    localStorage.setItem("cheat-mode", true)
  } else {
    localStorage.setItem("cheat-mode", false)
  }
  reRender();
}
export const isEnableCheatMode = () => (localStorage.getItem("cheat-mode") == "true")
export const isUserCanCheat = (username) => cheatUser[username]
export const isCheatUser = (username) => (localStorage.getItem("cheat-mode") == "true" && cheatUser[username])
export const isDepartmentManager = (departmentId) => {
  const manageDepartment = getManageDepartment();
  const user = getUser();
  if (!manageDepartment) {
    return false
  }
  const compareid = departmentId ||user.department.id
  return manageDepartment.indexOf(compareid) > -1
}
export const isUserCanChanObjectivePosition = (objective) => {
  const user = getUser();
  if (!user) {
    return false
  }
  if(isCheatUser(user.username)){
    return true
  }
  if (objective.owner.username == user.username) {
    return true
  }
  return false;
}
export const isUserCanChanObjectivePositionToDepartment = (objective) => {
  const user = getUser();
  if (!user) {
    return false
  }
  if(isCheatUser(user.username)){
    return true
  }
  if (objective.owner.username != user.username) {
    return false
  }
  return isDepartmentManager();
}
export const isUserCanChanObjectivePositionToCompany = (objective) => {
  const user = getUser();
  if (!user) {
    return false
  }
  if(isCheatUser(user.username)){
    return true
  }
  if (objective.owner.username != user.username) {
    return false
  }
  return isCompanyManager()
}
export const isCompanyManager = (companyId) => {
  const manageCompany = getManageCompany();
  const user = getUser();
  if (!manageCompany) {
    return false
  }
  const compareid = companyId ||user.company.id
  return manageCompany.indexOf(compareid) > -1
}
export const canViewObjectiveDetail = (objective) => {
  if (!objective.status) {
    return false;
  }
  return true;
}
export const changeProgressRight = (objective) => {
  const user = getUser();
  if (!user) {
    return false
  }
  if (!objective.status) {
    return false;
  }
  if (objective.year < currentYear || (objective.year == currentYear && objective.quarter < currentQuarter)) {
    return false
  }
  if (objective.leave && user.username == objective.owner.username) {
    return true;
  }
  return false
}
export const deleteObjectiveRight = (objective) => {
  const user = getUser();
  if (!user) {
    return false
  }
  if (isCheatUser(user.username)) {
    return true
  }
  if (objective.owner && user.username == objective.owner.username) {
    return true;
  }
  return false
}
export const switchOwnerRight = (objective) => {
  const user = getUser();
  if (!user) {
    return false
  }
  if (isCheatUser(user.username)) {
    return true
  }
  if (!objective.status) {
    return false;
  }
  if (objective.owner && user.username == objective.owner.username) {
    return true;
  }
  return false
}
export const linkObjectiveRight = (objective) => {
  const user = getUser();
  if (!user) {
    return false
  }
  if (isCheatUser(user.username)) {
    return true
  }
  if (!objective.status) {
    return false;
  }
  if (objective.year < currentYear || (objective.year == currentYear && objective.quarter < currentQuarter)) {
    return false
  }
  if (user.username == objective.owner.username) {
    if (objective.companyObjective && !objective.parentObjective) {
      return false
    }
    return true;
  }
  return false
}
export const setDeadlineRight = (objective) => {
  const user = getUser();
  if (!user) {
    return false
  }
  if (isCheatUser(user.username)) {
    return true
  }
  if (!objective.status) {
    return false;
  }
  if (objective.year < currentYear || (objective.year == currentYear && objective.quarter < currentQuarter)) {
    return false
  }
  if (objective.owner && user.username == objective.owner.username) {
    return true;
  }
  return false
}
export const closeObjectiveRight = (objective) => {
  const user = getUser();
  if (!user) {
    return false
  }
  if (!objective.status) {
    return false;
  }
  if (objective.year < currentYear || (objective.year == currentYear && objective.quarter < currentQuarter)) {
    return false
  }
  if (objective.owner && user.username == objective.owner.username) {
    if (objective.status == true) {
      return true;
    }
  }
  return false
}
export const openObjectiveRight = (objective) => {
  const user = getUser();
  if (!user) {
    return false
  }
  if (objective.year < currentYear || (objective.year == currentYear && objective.quarter < currentQuarter)) {
    return false
  }
  if (objective.owner && user.username == objective.owner.username) {
    if (objective.status == false && (!objective.parentObjective || objective.parentObjective.status == true)) {
      return true;
    }
  }
  return false
}
export const setObservableRight = (objective) => {
  const user = getUser();
  if (!user) {
    return false
  }
  if (isCheatUser(user.username)) {
    return true
  }
  if (!objective.status) {
    return false;
  }
  if (objective.year < currentYear || (objective.year == currentYear && objective.quarter < currentQuarter)) {
    return false
  }
  if (objective.owner && user.username == objective.owner.username) {
    return true;
  }
  return false
}
export const changeObjectiveNameRight = (objective) => {
  const user = getUser();
  if (!user) {
    return false
  }
  if (isCheatUser(user.username)) {
    return true
  }
  if (!objective.status) {
    return false;
  }
  if (objective.year < currentYear || (objective.year == currentYear && objective.quarter < currentQuarter)) {
    return false
  }
  if (objective.owner && user.username == objective.owner.username) {
    return true;
  }
  return false
}
export const takeObjectiveRight = (objective) => {
  const user = getUser();
  if (!user) {
    return false
  }
  if (isCheatUser(user.username)) {
    return true
  }
  if (!objective.status) {
    return false;
  }
  if (objective.year < currentYear || (objective.year == currentYear && objective.quarter < currentQuarter)) {
    return false
  }
  if (objective.owner && user.username != objective.owner.username) {
    return true;
  }
  return false
}
export const checkCanDeleteUserFromGroup = (group, participant) => {
  const user = getUser();
  if (!user, !group) {
    return false
  }
  if (group.createdUser.username != user.username) {
    return false;
  }
  if (group.createdUser.username == participant.username) {
    return false;
  }
  return true
}
export const checkCanAddUserFromGroup = (group) => {
  const user = getUser();
  if (!user || !group) {
    return false
  }
  if (group.createdUser.username == user.username) {
    return true;
  }
  return false
}
export const showObjectiveActionTool = (objective) => {
  const user = getUser();
  if (!user) {
    return false
  }
  if (objective.owner.username == user.username) {
    return true
  }
  if (objective.status) {
    return true
  }
  return false;
}
