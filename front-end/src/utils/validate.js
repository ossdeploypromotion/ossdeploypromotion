/**
 * Created by W540 on 3/6/2017.
 */
export const validateString = (data,validation,errorMessage,fieldName,error) => {
  if(!error[fieldName] && validation.required){
    if(!data[fieldName] || !data[fieldName].trim()){
      error[fieldName] = errorMessage;
    }
  }
  if(!error[fieldName] && validation.maxLength){
    if(!data[fieldName] || data[fieldName].length > validation.maxLength){
      error[fieldName] = errorMessage;
    }
  }
}
