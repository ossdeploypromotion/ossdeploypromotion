package com.oss.platform.checklist.data;

import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;
import java.util.Date;
import java.util.List;

/**
 * Created by haocpq on 6/9/2017.
 */
@Document(collection = "CL_Detail")
public class CL_DetailEntity {

    @Id
    private String id;

    private String title;
    private String description;
    private String modunid;
    private Integer companyid;
    private Object relate;
    private List<CL_ItemEntity> itemps;
    private String createduser;
    private Date createddate;
    private Object metadata;

    private Date startdate;
    private Date enddate;
    private List<String> assignto;

    public CL_DetailEntity() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getModunid() {
        return modunid;
    }

    public void setModunid(String modunid) {
        this.modunid = modunid;
    }

    public Integer getCompanyid() {
        return companyid;
    }

    public void setCompanyid(Integer companyid) {
        this.companyid = companyid;
    }

    public Object getRelate() {
        return relate;
    }

    public void setRelate(Object relate) {
        this.relate = relate;
    }

    public List<CL_ItemEntity> getItemps() {
        return itemps;
    }

    public void setItemps(List<CL_ItemEntity> itemps) {
        this.itemps = itemps;
    }

    public String getCreateduser() {
        return createduser;
    }

    public void setCreateduser(String createduser) {
        this.createduser = createduser;
    }

    public Date getCreateddate() {
        return createddate;
    }

    public void setCreateddate(Date createddate) {
        this.createddate = createddate;
    }

    public List<String> getAssignto() {
        return assignto;
    }

    public void setAssignto(List<String> assignto) {
        this.assignto = assignto;
    }

    public Object getMetadata() {
        return metadata;
    }

    public void setMetadata(Object metadata) {
        this.metadata = metadata;
    }

    public Date getStartdate() {
        return startdate;
    }

    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }

    public Date getEnddate() {
        return enddate;
    }

    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }
}
