package com.oss.platform.checklist.data;

import com.oss.platform.checklist.dto.CL_Resource;
import com.oss.platform.checklist.dto.UserUpdate;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;
import java.util.Date;
import java.util.List;

/**
 * Created by haocpq on 6/9/2017.
 */
@Document(collection = "CL_Item")
public class CL_ItemEntity {

    @Id
    private String id;
    private String title;
    private String description;
    private Integer companyid;
    private List<CL_Resource> resource;
    private List<String> modunid;
    private Integer order;
    private Object metadata;
    private String path;
    // collect info in detail action
    private Object status;
    private List<CL_Resource> evident;
    private UserUpdate lastupdate;

    private String createduser;
    private Date createddate;

    public CL_ItemEntity() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getCompanyid() {
        return companyid;
    }

    public void setCompanyid(Integer companyid) {
        this.companyid = companyid;
    }

    public List<String> getModunid() {
        return modunid;
    }

    public void setModunid(List<String> modunid) {
        this.modunid = modunid;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public Object getMetadata() {
        return metadata;
    }

    public void setMetadata(Object metadata) {
        this.metadata = metadata;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Object getStatus() {
        return status;
    }

    public void setStatus(Object status) {
        this.status = status;
    }

    public String getCreateduser() {
        return createduser;
    }

    public void setCreateduser(String createduser) {
        this.createduser = createduser;
    }

    public Date getCreateddate() {
        return createddate;
    }

    public void setCreateddate(Date createddate) {
        this.createddate = createddate;
    }

    public List<CL_Resource> getResource() {
        return resource;
    }

    public void setResource(List<CL_Resource> resource) {
        this.resource = resource;
    }

    public List<CL_Resource> getEvident() {
        return evident;
    }

    public void setEvident(List<CL_Resource> evident) {
        this.evident = evident;
    }

    public UserUpdate getLastupdate() {
        return lastupdate;
    }

    public void setLastupdate(UserUpdate lastupdate) {
        this.lastupdate = lastupdate;
    }
}
