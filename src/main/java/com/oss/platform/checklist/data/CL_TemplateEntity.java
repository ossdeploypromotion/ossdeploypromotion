package com.oss.platform.checklist.data;

import com.oss.platform.checklist.dto.CL_TemplateItempOrder;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;
import java.util.Date;
import java.util.List;

/**
 * Created by haocpq on 6/9/2017.
 */
@Document(collection = "CL_Template")
public class CL_TemplateEntity {
    @Id
    private String id;
    private String title;
    private String description;
    private Integer companyid;
    private List<String> modunid;

    private List<CL_TemplateItempOrder> itemps;
    private String createduser;
    private Date createdddate;

    public CL_TemplateEntity() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getCompanyid() {
        return companyid;
    }

    public void setCompanyid(Integer companyid) {
        this.companyid = companyid;
    }

    public List<String> getModunid() {
        return modunid;
    }

    public void setModunid(List<String> modunid) {
        this.modunid = modunid;
    }

    public List<CL_TemplateItempOrder> getItemps() {
        return itemps;
    }

    public void setItemps(List<CL_TemplateItempOrder> itemps) {
        this.itemps = itemps;
    }

    public String getCreateduser() {
        return createduser;
    }

    public void setCreateduser(String createduser) {
        this.createduser = createduser;
    }

    public Date getCreatedddate() {
        return createdddate;
    }

    public void setCreatedddate(Date createdddate) {
        this.createdddate = createdddate;
    }
}
