package com.oss.platform.checklist.dto;

import java.util.List;

/**
 * Created by haocpq on 6/9/2017.
 */
public class CL_DetailDto {

    private String id;
    private String title;
    private String description;
    private Object relate;
    private List<CL_ItemDto> itemps;

    private Object metadata;
    private Long startdate;
    private Long enddate;
    private List<String> assignto;

    public CL_DetailDto() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Object getRelate() {
        return relate;
    }

    public void setRelate(Object relate) {
        this.relate = relate;
    }

    public List<CL_ItemDto> getItemps() {
        return itemps;
    }

    public void setItemps(List<CL_ItemDto> itemps) {
        this.itemps = itemps;
    }

    public List<String> getAssignto() {
        return assignto;
    }

    public void setAssignto(List<String> assignto) {
        this.assignto = assignto;
    }

    public Object getMetadata() {
        return metadata;
    }

    public void setMetadata(Object metadata) {
        this.metadata = metadata;
    }

    public Long getStartdate() {
        return startdate;
    }

    public void setStartdate(Long startdate) {
        this.startdate = startdate;
    }

    public Long getEnddate() {
        return enddate;
    }

    public void setEnddate(Long enddate) {
        this.enddate = enddate;
    }
}
