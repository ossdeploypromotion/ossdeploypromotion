package com.oss.platform.checklist.dto;

import java.util.List;

/**
 * Created by haocpq on 6/15/2017.
 */
public class CL_DetailInDto {
    private Integer type;
    private Object relate;
    private String templateid;
    private Long startdate;
    private Long enddate;
    private List<String> assignto;
    private Object metadata;

    public CL_DetailInDto() {
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Object getRelate() {
        return relate;
    }

    public void setRelate(Object relate) {
        this.relate = relate;
    }

    public String getTemplateid() {
        return templateid;
    }

    public void setTemplateid(String templateid) {
        this.templateid = templateid;
    }

    public Long getStartdate() {
        return startdate;
    }

    public void setStartdate(Long startdate) {
        this.startdate = startdate;
    }

    public Long getEnddate() {
        return enddate;
    }

    public void setEnddate(Long enddate) {
        this.enddate = enddate;
    }

    public List<String> getAssignto() {
        return assignto;
    }

    public void setAssignto(List<String> assignto) {
        this.assignto = assignto;
    }

    public Object getMetadata() {
        return metadata;
    }

    public void setMetadata(Object metadata) {
        this.metadata = metadata;
    }
}
