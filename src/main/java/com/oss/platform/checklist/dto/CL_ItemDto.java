package com.oss.platform.checklist.dto;

import java.util.List;

/**
 * Created by haocpq on 6/9/2017.
 */

public class CL_ItemDto {

    private String id;
    private String title;
    private String description;
    private List<CL_Resource> resource;
    private Integer order;
    private Object metadata;
    private String path;

    private Object status;
    private List<CL_Resource> evident;
    private UserUpdateDto lastupdate;

    public CL_ItemDto() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<CL_Resource> getResource() {
        return resource;
    }

    public void setResource(List<CL_Resource> resource) {
        this.resource = resource;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public Object getMetadata() {
        return metadata;
    }

    public void setMetadata(Object metadata) {
        this.metadata = metadata;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Object getStatus() {
        return status;
    }

    public void setStatus(Object status) {
        this.status = status;
    }

    public List<CL_Resource> getEvident() {
        return evident;
    }

    public void setEvident(List<CL_Resource> evident) {
        this.evident = evident;
    }

    public UserUpdateDto getLastupdate() {
        return lastupdate;
    }

    public void setLastupdate(UserUpdateDto lastupdate) {
        this.lastupdate = lastupdate;
    }
}

