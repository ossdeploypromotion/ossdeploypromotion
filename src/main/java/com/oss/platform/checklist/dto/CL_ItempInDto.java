package com.oss.platform.checklist.dto;

import java.util.List;

/**
 * Created by haocpq on 6/9/2017.
 */
public class CL_ItempInDto {

    private String id;
    private String title;
    private String description;
    private List<CL_Resource> resources;
    private List<String> modunid;
    private Object metadata;
    private String parentid;

    private Object status;
    private List<CL_Resource> evident;

    public CL_ItempInDto() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<CL_Resource> getResources() {
        return resources;
    }

    public void setResources(List<CL_Resource> resources) {
        this.resources = resources;
    }

    public List<String> getModunid() {
        return modunid;
    }

    public void setModunid(List<String> modunid) {
        this.modunid = modunid;
    }

    public Object getMetadata() {
        return metadata;
    }

    public void setMetadata(Object metadata) {
        this.metadata = metadata;
    }

    public String getParentid() {
        return parentid;
    }

    public void setParentid(String parentid) {
        this.parentid = parentid;
    }

    public Object getStatus() {
        return status;
    }

    public void setStatus(Object status) {
        this.status = status;
    }

    public List<CL_Resource> getEvident() {
        return evident;
    }

    public void setEvident(List<CL_Resource> evident) {
        this.evident = evident;
    }
}
