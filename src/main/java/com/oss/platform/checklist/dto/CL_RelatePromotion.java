package com.oss.platform.checklist.dto;

import java.util.List;

/**
 * Created by haocpq on 6/19/2017.
 */
public class CL_RelatePromotion {

    private String promotionid;
    private List<Integer> shops;

    public CL_RelatePromotion() {
    }

    public String getPromotionid() {
        return promotionid;
    }

    public void setPromotionid(String promotionid) {
        this.promotionid = promotionid;
    }

    public List<Integer> getShops() {
        return shops;
    }

    public void setShops(List<Integer> shops) {
        this.shops = shops;
    }
}
