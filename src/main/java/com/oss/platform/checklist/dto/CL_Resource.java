package com.oss.platform.checklist.dto;

/**
 * Created by haocpq on 6/9/2017.
 */
public class CL_Resource {

    private String type;
    private String path;
    private String filepattern;
    private String name;

    public CL_Resource() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getFilepattern() {
        return filepattern;
    }

    public void setFilepattern(String filepattern) {
        this.filepattern = filepattern;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
