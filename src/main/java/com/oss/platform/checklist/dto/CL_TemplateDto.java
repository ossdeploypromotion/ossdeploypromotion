package com.oss.platform.checklist.dto;

import java.util.List;

/**
 * Created by haocpq on 6/9/2017.
 */
public class CL_TemplateDto {

    private String id;
    private String title;
    private String description;
    private List<CL_TemplateItempOrder> itemps;

    public CL_TemplateDto() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<CL_TemplateItempOrder> getItemps() {
        return itemps;
    }

    public void setItemps(List<CL_TemplateItempOrder> itemps) {
        this.itemps = itemps;
    }
}
