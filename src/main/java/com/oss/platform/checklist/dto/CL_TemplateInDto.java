package com.oss.platform.checklist.dto;

import java.util.List;

/**
 * Created by haocpq on 6/15/2017.
 */
public class CL_TemplateInDto {

    private String id;
    private String title;
    private String description;
    private List<String> modunid;
    private List<String> itemps;

    public CL_TemplateInDto() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getModunid() {
        return modunid;
    }

    public void setModunid(List<String> modunid) {
        this.modunid = modunid;
    }

    public List<String> getItemps() {
        return itemps;
    }

    public void setItemps(List<String> itemps) {
        this.itemps = itemps;
    }
}
