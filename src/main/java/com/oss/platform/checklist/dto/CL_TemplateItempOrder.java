package com.oss.platform.checklist.dto;

/**
 * Created by haocpq on 6/16/2017.
 */
public class CL_TemplateItempOrder {

    private String id;
    private Integer level;

    public CL_TemplateItempOrder() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }
}
