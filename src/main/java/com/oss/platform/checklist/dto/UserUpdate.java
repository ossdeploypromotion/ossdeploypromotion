package com.oss.platform.checklist.dto;

import java.util.Date;

/**
 * Created by haocpq on 6/9/2017.
 */
public class UserUpdate {

    private Date time;
    private String username;

    public UserUpdate() {
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
