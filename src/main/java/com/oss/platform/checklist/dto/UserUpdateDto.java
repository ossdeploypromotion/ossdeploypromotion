package com.oss.platform.checklist.dto;

/**
 * Created by haocpq on 6/20/2017.
 */
public class UserUpdateDto {
    private Long time;
    private String username;

    public UserUpdateDto() {
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
