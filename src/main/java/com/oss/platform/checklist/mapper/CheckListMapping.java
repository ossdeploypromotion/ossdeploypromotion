package com.oss.platform.checklist.mapper;

import com.oss.platform.checklist.data.CL_DetailEntity;
import com.oss.platform.checklist.data.CL_ItemEntity;
import com.oss.platform.checklist.data.CL_TemplateEntity;
import com.oss.platform.checklist.dto.CL_DetailDto;
import com.oss.platform.checklist.dto.CL_ItemDto;
import com.oss.platform.checklist.dto.CL_TemplateDto;
import com.oss.platform.checklist.dto.UserUpdateDto;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by haocpq on 6/9/2017.
 */
public class CheckListMapping {

    public static CL_ItemDto mappingItem (CL_ItemEntity entity){
        CL_ItemDto itemDto = new CL_ItemDto();
        if(entity == null){
            return null;
        }
        itemDto.setId(entity.getId());
        itemDto.setTitle(entity.getTitle());
        itemDto.setDescription(entity.getDescription());
        itemDto.setOrder(entity.getOrder());
        itemDto.setResource(entity.getResource());
        itemDto.setMetadata(entity.getMetadata());
        itemDto.setStatus(entity.getStatus());
        if(entity.getLastupdate() != null){
            UserUpdateDto lastupdate = new UserUpdateDto();
            lastupdate.setUsername(entity.getLastupdate().getUsername());
            lastupdate.setTime(entity.getLastupdate().getTime().getTime());

            itemDto.setLastupdate(lastupdate);
        }
        itemDto.setEvident(entity.getEvident());
        itemDto.setPath(entity.getPath());
        return itemDto;
    }

//    public static List<CL_ItemDto> recursionChildItemp( List<CL_ItemEntity> childs){
//
//        if(childs == null){
//            return null;
//        }
//        CL_ItemEntity entity = new CL_ItemEntity();
//        List<CL_ItemDto>  childsDto = new ArrayList<CL_ItemDto>();
//
//        for (CL_ItemEntity childentity: childs){
//            CL_ItemDto item = new CL_ItemDto();
//            item.setId(childentity.getId());
//            item.setTitle(childentity.getTitle());
//            item.setDescription(childentity.getDescription());
//            item.setOrder(childentity.getOrder());
//            item.setResource(childentity.getResource());
//            item.setMetadata(childentity.getMetadata());
//            item.setStatus(childentity.getStatus());
//            item.setLastupdate(childentity.getLastupdate());
//            item.setEvident(childentity.getEvident());
//            item.setChildrend(recursionChildItemp(childentity.getChildrend()));
//            childsDto.add(item);
//        }
//
//        return childsDto;
//    }

    public static List<CL_ItemDto> mappingItem (List<CL_ItemEntity> entities){
        return entities.stream().map(entity -> mappingItem(entity)).collect(Collectors.toList());
    }

    public static CL_TemplateDto mappingTemplate (CL_TemplateEntity entities){

        if(entities == null){
            return null;
        }

        CL_TemplateDto cl_templateDto = new CL_TemplateDto();
        cl_templateDto.setId(entities.getId());
        cl_templateDto.setDescription(entities.getDescription());
        cl_templateDto.setTitle(entities.getTitle());
        cl_templateDto.setItemps(entities.getItemps());
        return cl_templateDto;
    }

    public static List<CL_TemplateDto> mappingTemplate(List<CL_TemplateEntity> entities){
        return entities.stream().map(t -> mappingTemplate(t)).collect(Collectors.toList());
    }


    public static CL_DetailDto mappingDetail (CL_DetailEntity entity){

        if(entity == null){
            return null;
        }

        CL_DetailDto cl_detailDto = new CL_DetailDto();
        cl_detailDto.setId(entity.getId());
        cl_detailDto.setTitle(entity.getTitle());
        cl_detailDto.setDescription(entity.getDescription());
        cl_detailDto.setMetadata(entity.getMetadata());
        cl_detailDto.setAssignto(entity.getAssignto());
        cl_detailDto.setRelate(entity.getRelate());
        if(entity.getStartdate() != null) {
            cl_detailDto.setStartdate(entity.getStartdate().getTime());
        }
        if(entity.getEnddate() != null) {
            cl_detailDto.setEnddate(entity.getEnddate().getTime());
        }
        cl_detailDto.setItemps(mappingItem(entity.getItemps()));

        return cl_detailDto;
    }

    public static List<CL_DetailDto> mappingDetail (List<CL_DetailEntity> entities){
        return entities.stream().map(t -> mappingDetail(t)).collect(Collectors.toList());
    }
}
