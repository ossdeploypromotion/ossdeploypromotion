package com.oss.platform.checklist.repository;

import com.oss.platform.checklist.data.CL_DetailEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

/**
 * Created by haocpq on 6/15/2017.
 */
public interface CheckListDetailResponsitory extends MongoRepository<CL_DetailEntity, String> {

    @Query("{'modunid': ?0,'relate.promotionid': ?1, 'companyid': ?3}")
    List<CL_DetailEntity> getDetail(String modun, String promotion, String username, Integer companyid);


}
