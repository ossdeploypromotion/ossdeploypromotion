package com.oss.platform.checklist.repository;

import com.oss.platform.checklist.data.CL_ItemEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

/**
 * Created by haocpq on 6/9/2017.
 */
public interface CheckListItempResonsitory extends MongoRepository<CL_ItemEntity, String> {

    @Query("{'modunid': { $in: [ ?0 ] }, 'companyid': ?1 }")
    List<CL_ItemEntity> findWithModun(String modun, Integer companyid);

    List<CL_ItemEntity> findByIdIn(List<String> id);
}
