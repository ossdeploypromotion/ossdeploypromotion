package com.oss.platform.checklist.repository;

import com.oss.platform.checklist.data.CL_TemplateEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

/**
 * Created by haocpq on 6/15/2017.
 */
public interface CheckListTemplateResponsitory extends MongoRepository<CL_TemplateEntity, String> {

    @Query("{'modunid': { $in: [ ?0 ] }, 'companyid': ?1 }")
    List<CL_TemplateEntity> getList(String modun, Integer companyid);

}
