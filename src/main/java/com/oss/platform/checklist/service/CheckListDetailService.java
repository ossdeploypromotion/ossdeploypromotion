package com.oss.platform.checklist.service;

import com.oss.platform.checklist.dto.CL_DetailInDto;
import com.oss.platform.checklist.dto.CL_ItempInDto;

/**
 * Created by haocpq on 6/9/2017.
 */
public interface CheckListDetailService {
    Object saveDetail(String modun, CL_DetailInDto detail, String username, Integer companyid);

    Object getDetail(String modun, String promotion, String username, Integer companyid);

    Object editDetail(String modun, String promotiondetail, CL_ItempInDto detail, String username, Integer companyid);
}
