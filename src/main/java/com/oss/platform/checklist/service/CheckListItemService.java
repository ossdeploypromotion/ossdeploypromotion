package com.oss.platform.checklist.service;

import com.oss.platform.checklist.dto.CL_ItemDto;
import com.oss.platform.checklist.dto.CL_ItempInDto;

import java.util.List;

/**
 * Created by haocpq on 6/7/2017.
 */
public interface CheckListItemService {

    List<CL_ItemDto> findWithModun(String modun, Integer companyid);

    CL_ItemDto save(CL_ItempInDto itemp, String modun, String username, Integer companyid);
}
