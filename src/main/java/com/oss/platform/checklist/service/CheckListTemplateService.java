package com.oss.platform.checklist.service;

import com.oss.platform.checklist.dto.CL_TemplateDto;
import com.oss.platform.checklist.dto.CL_TemplateInDto;

import java.util.List;

/**
 * Created by haocpq on 6/9/2017.
 */
public interface CheckListTemplateService {
    CL_TemplateDto save(CL_TemplateInDto template, String modun, String username, Integer companyid);

    List<CL_TemplateDto> getList(String modun, Integer companyid);
}
