package com.oss.platform.checklist.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.oss.platform.checklist.data.CL_DetailEntity;
import com.oss.platform.checklist.data.CL_ItemEntity;
import com.oss.platform.checklist.data.CL_TemplateEntity;
import com.oss.platform.checklist.dto.*;
import com.oss.platform.checklist.mapper.CheckListMapping;
import com.oss.platform.checklist.repository.CheckListDetailResponsitory;
import com.oss.platform.checklist.repository.CheckListItempResonsitory;
import com.oss.platform.checklist.repository.CheckListTemplateResponsitory;
import com.oss.platform.checklist.service.CheckListDetailService;
import com.oss.platform.common.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by hacpq on 6/15/2017.
 */
@Service
public class CheckListDetailServiceImpl implements CheckListDetailService {

    @Autowired
    private CheckListDetailResponsitory checkListDetailResponsitory;

    @Autowired
    private CheckListTemplateResponsitory checkListTemplateResponsitory;

    @Autowired
    private CheckListItempResonsitory checkListItempResonsitory;

    @Override
    public Object saveDetail(String modun, CL_DetailInDto detail, String username, Integer companyid) {

        Date now = new Date();
        if(Constant.CHECKLIST_DETAIL_PROMOTIONSETUP.equals(modun)){
            List<CL_DetailEntity> listReturn = new ArrayList<>();

            CL_TemplateEntity template = checkListTemplateResponsitory.findOne(detail.getTemplateid());
            if(template != null){
                List<CL_TemplateItempOrder> itempTemplate = template.getItemps();
                if(itempTemplate != null){
                    List<String> listitemp = new ArrayList<>();

                    Map<String, CL_TemplateItempOrder> mapItemp = itempTemplate.stream()
                            .collect(Collectors.toMap(i-> i.getId(), i-> i));

                    for( CL_TemplateItempOrder item : itempTemplate){
                        listitemp.add(item.getId());
                    }
                    List<CL_ItemEntity> entities = checkListItempResonsitory.findByIdIn(listitemp);

                    // order check list detail
                    for (CL_ItemEntity entity: entities){
                        CL_TemplateItempOrder check = mapItemp.get(entity.getId());
                        if( check != null){
                            entity.setOrder(check.getLevel());
                        }
                    }
                    // clone detail
                    ObjectMapper mapper = new ObjectMapper();
                    CL_RelatePromotion relate = mapper.convertValue(detail.getRelate(),CL_RelatePromotion.class);
                    if( relate != null){
                        for(Integer shop : relate.getShops()){
                            CL_DetailEntity entity = new CL_DetailEntity();
                            entity.setId(relate.getPromotionid()+ shop);

                            Map<String, Object> relatedetail = new HashMap<>();
                            relatedetail.put("promotionid",relate.getPromotionid());
                            relatedetail.put("shopid",shop);
                            entity.setRelate(relatedetail);

                            entity.setTitle(template.getTitle());
                            entity.setDescription(template.getDescription());
                            entity.setModunid(modun);
                            entity.setCompanyid(companyid);
                            entity.setItemps(entities);
                            entity.setCreateddate(now);
                            entity.setCreateduser(username);

                            if(detail.getStartdate() != null) {
                                entity.setStartdate(new Date(detail.getStartdate()));
                            }
                            if(detail.getEnddate() != null){
                                entity.setEnddate(new Date(detail.getEnddate()));
                            }
                            if(detail.getAssignto() != null){
                                entity.setAssignto(detail.getAssignto());
                            }
                            listReturn.add(checkListDetailResponsitory.save(entity));
                        }
                    }
                }
            }

            return CheckListMapping.mappingDetail(listReturn);
        }

        return null;
    }

    @Override
    public Object getDetail(String modun, String promotion, String username, Integer companyid) {

        List<CL_DetailEntity> entities = checkListDetailResponsitory.getDetail(modun, promotion, username, companyid);
        return CheckListMapping.mappingDetail(entities);
    }

    @Override
    public Object editDetail(String modun, String promotiondetail, CL_ItempInDto detail, String username, Integer companyid) {

        CL_DetailEntity entity = checkListDetailResponsitory.findOne(promotiondetail);
        if( Constant.CHECKLIST_DETAIL_PROMOTIONSETUP.equals(modun) ){
            List<CL_ItemEntity> itemps = entity.getItemps();

            for (CL_ItemEntity item : itemps){
                if(item.getId().equals(detail.getId())){
                    item.setDescription(detail.getDescription());
                    item.setTitle(detail.getTitle());
                    item.setResource(detail.getResources());
                    item.setMetadata(detail.getMetadata());
                    item.setEvident(detail.getEvident());
                    item.setStatus(detail.getStatus());
                    if(detail.getStatus() != null){
                        if(detail.getStatus().equals(1)) {
                            Date now = new Date();
                            UserUpdate lastupdate = new UserUpdate();
                            lastupdate.setUsername(username);
                            lastupdate.setTime(now);
                            item.setLastupdate(lastupdate);
                        } else {
                            item.setLastupdate(null);
                        }
                    }
                }
            }
        }

        CL_DetailEntity update = checkListDetailResponsitory.save(entity);
        return CheckListMapping.mappingDetail(update);
    }
}
