package com.oss.platform.checklist.service.impl;

import com.oss.platform.checklist.data.CL_ItemEntity;
import com.oss.platform.checklist.dto.CL_ItemDto;
import com.oss.platform.checklist.dto.CL_ItempInDto;
import com.oss.platform.checklist.mapper.CheckListMapping;
import com.oss.platform.checklist.repository.CheckListItempResonsitory;
import com.oss.platform.checklist.service.CheckListItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by haocpq on 6/9/2017.
 */
@Service
public class CheckListItemServiceImpl implements CheckListItemService {

    @Autowired
    CheckListItempResonsitory checkListItempResonsitory;


    @Override
    public List<CL_ItemDto> findWithModun(String modun, Integer companyid) {
        List<CL_ItemEntity> cl_itemEntityList = checkListItempResonsitory.findWithModun(modun, companyid);

        return CheckListMapping.mappingItem(cl_itemEntityList);
    }

    @Override
    public CL_ItemDto save(CL_ItempInDto itemp, String modun, String username, Integer companyid) {

        CL_ItemEntity result;
        CL_ItemEntity entity = new CL_ItemEntity();
        Date now = new Date();

        if(itemp.getId() == null) {
            entity.setId(username + now.getTime());
            entity.setCreateduser(username);
            entity.setCreateddate(now);
            entity.setCompanyid(companyid);
        } else {
            entity = checkListItempResonsitory.findOne(itemp.getId());
            if(entity == null){
                return null;
            }
        }

        entity.setTitle(itemp.getTitle());
        entity.setDescription(itemp.getDescription());

        if(itemp.getResources()!= null){
            entity.setResource(itemp.getResources());
        }
        if(itemp.getMetadata()!= null){
            entity.setMetadata(itemp.getMetadata());
        }

        if(itemp.getModunid() != null){
            entity.setModunid(itemp.getModunid());
        } else {
            List<String> listModun = new ArrayList<>();
            listModun.add(modun);

            entity.setModunid(listModun);
        }

        if(itemp.getParentid() != null){
            CL_ItemEntity parent = checkListItempResonsitory.findOne(itemp.getParentid());
            String path = "";
            if (parent != null && !"".equals(parent.getPath())){
                path = parent.getPath();
                if (path == null) {
                    path = "," + parent.getId() + ",";
                } else {
                    path = path + parent.getId() + ",";
                }
                entity.setPath(path);
            }
        }

        result = checkListItempResonsitory.save(entity);

        return CheckListMapping.mappingItem(result);
    }
}
