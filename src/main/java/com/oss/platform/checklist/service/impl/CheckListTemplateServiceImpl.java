package com.oss.platform.checklist.service.impl;

import com.oss.platform.checklist.data.CL_TemplateEntity;
import com.oss.platform.checklist.dto.CL_TemplateDto;
import com.oss.platform.checklist.dto.CL_TemplateInDto;
import com.oss.platform.checklist.dto.CL_TemplateItempOrder;
import com.oss.platform.checklist.mapper.CheckListMapping;
import com.oss.platform.checklist.repository.CheckListTemplateResponsitory;
import com.oss.platform.checklist.service.CheckListTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by haocpq on 6/9/2017.
 */
@Service
public class CheckListTemplateServiceImpl implements CheckListTemplateService {

    @Autowired
    CheckListTemplateResponsitory checkListTemplateResponsitory;

    @Override
    public CL_TemplateDto save(CL_TemplateInDto template, String modun, String username, Integer companyid) {
        CL_TemplateEntity entity = new CL_TemplateEntity();
        Date now = new Date();

        if(template.getId() == null){
            entity.setId(username+ now.getTime());
            entity.setCompanyid(companyid);
            entity.setCreatedddate(now);
            entity.setCreateduser(username);

        } else {
            entity = checkListTemplateResponsitory.findOne(template.getId());
            if(entity == null){
                return null;
            }
        }

        if(template.getModunid() != null){
            entity.setModunid(template.getModunid());
        } else {
            List<String> listModun = new ArrayList<>();
            listModun.add(modun);
            entity.setModunid(listModun);
        }
        entity.setTitle(template.getTitle());
        entity.setDescription(template.getDescription());

        List<String> itemps = template.getItemps();
        if(itemps != null){
            List<CL_TemplateItempOrder> itempOrder = new ArrayList<>();
            Integer level = 0;
            for(String item: itemps){
                CL_TemplateItempOrder order = new CL_TemplateItempOrder();
                order.setId(item);
                order.setLevel( level++ );
                itempOrder.add(order);
            }
            entity.setItemps(itempOrder);
        }

        CL_TemplateEntity result = checkListTemplateResponsitory.save(entity);
        return CheckListMapping.mappingTemplate(result);
    }

    @Override
    public List<CL_TemplateDto> getList(String modun, Integer companyid) {

        List<CL_TemplateEntity> cl_templateEntity = checkListTemplateResponsitory.getList(modun, companyid);
        return CheckListMapping.mappingTemplate(cl_templateEntity);
    }


}
