package com.oss.platform.common;

import java.io.File;

/**
 * Created by haocpq on 4/13/2017.
 */
public class Constant {

    public static final int ROOM_ENABLE = 1;
    public static final int ROOM_DISABLE = 0;

    public static final Integer LUNCHCHECK = 1 ;
    public static final Integer LUNCHUNCHECK = 0 ;

    public static final int ENABLE = 1;
    public static final Integer DISABLE = 0;

    public static final String SUCCESS = "success";
    public static final String ERROR = "error";

    // STATUS HTTP HEADER RETURN

    /**
     * Status code (100) indicating the client can continue.
     */
    public static final int SC_CONTINUE = 100;
    /**
     * Status code (200) indicating the request succeeded normally.
     */
    public static final int SC_OK = 200;
    /**
     * Status code (201) indicating the request succeeded and created
     * a new resource on the server.
     */
    public static final int SC_CREATED = 201;
    /**
     * Status code (202) indicating that a request was accepted for
     * processing, but was not completed.
     */
    public static final int SC_ACCEPTED = 202;
    /**
     * Status code (204) indicating that the request succeeded but that
     * there was no new information to return.
     */
    public static final int SC_NO_CONTENT = 204;
    /**
     * Status code (400) indicating the request sent by the client was
     * syntactically incorrect.
     */
    public static final int SC_BAD_REQUEST = 400;
    /**
     * Status code (404) indicating that the requested resource is not
     * available.
     */
    public static final int SC_NOT_FOUND = 404;

    public static final Integer LUNCHPRICE = 15000;


    public static final String CHECKLIST_CONTENT_PREFIX = "cl";
    public static final String CHECKLIST_FILE_PATH= "upload";

    public static final String CHECKLIST_DETAIL_PROMOTIONSETUP = "59434d1652137b36e4d8e1ac";
}
