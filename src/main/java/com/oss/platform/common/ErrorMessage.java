package com.oss.platform.common;

/**
 * Created by haocqp on 1/12/2017.
 */
public class ErrorMessage {

    private String errorMessage;
    private String errorCode;

    public ErrorMessage() {
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
}
