package com.oss.platform.common;

/**
 * Created by hao.cpq on 1/9/2017.
 */
public class ResultMessage {

    private String status;

    private Long total;

    private Object records;

    public ResultMessage() {

    }

    public String getStatus() {
        if(this.status==null)
            return "";
        else
            return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getTotal() {
        if(this.total==null)
            return 0L;
        else
            return this.total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public Object getRecords() {

        if(this.records==null)
            return "";
        else
            return this.records;
    }

    public void setRecords(Object records) {
        this.records = records;
    }
}
