package com.oss.platform.config;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.context.support.ResourceBundleMessageSource;

import com.oss.platform.common.CurrentTimeDateTimeService;
import com.oss.platform.common.DateTimeService;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@Configuration
@ComponentScan({"com.oss.platform.security","com.oss.platform.web"})
@Import({WebMvcContext.class, PersistenceContext.class, SecurityContext.class})
public class ApplicationContext {

    private static final String MESSAGE_SOURCE_BASE_NAME = "messages/messages";

    /**
     * These static classes are required because it makes it possible to use
     * different properties files for every Spring profile.
     *
     * See: <a href="http://stackoverflow.com/a/14167357/313554">This StackOverflow answer</a> for more details.
     */
    @Profile(Profiles.DEFAULT)
    @Configuration
    @PropertySource("classpath:production.properties")
    static class ApplicationProperties {}

    @Bean
    DateTimeService currentTimeDateTimeService() {
        return new CurrentTimeDateTimeService();
    }

    @Profile(Profiles.DEVELOPMENT)
    @Configuration
    @PropertySource("classpath:application.properties")
    static class IntegrationTestProperties {}

    @Bean
    MessageSource messageSource() {
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        messageSource.setBasename(MESSAGE_SOURCE_BASE_NAME);
        messageSource.setUseCodeAsDefaultMessage(true);
        return messageSource;
    }

    @Bean
    PropertySourcesPlaceholderConfigurer propertyPlaceHolderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }



//    @Bean
//    public InitializingBean insertDefaultUsers() {
//        return new InitializingBean() {
//            @Autowired
//            private UserRepository userRepository;
//
//            @Override
//            public void afterPropertiesSet() {
//                addUser("admin", "admin");
//                addUser("user", "user");
//            }
//
//            private void addUser(String username, String password) {
//                UserEntity user = new UserEntity();
//                user.setUsername(username);
//                user.setPassword(new BCryptPasswordEncoder().encode(password));
//                user.grantRole(username.equals("admin") ? UserRoleEntity.ADMIN : UserRoleEntity.USER);
//                userRepository.save(user);
//            }
//        };
//    }
}
