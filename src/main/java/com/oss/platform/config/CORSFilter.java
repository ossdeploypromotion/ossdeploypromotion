package com.oss.platform.config;

import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;


public class CORSFilter extends OncePerRequestFilter {
    String[] acceptedDomail = {
            "http://localhost:3000","http://localhost:8080","http://oss.seedcom.vn","http://oss.juno.vn","http://oss.caudatfarm.vn"
            ,"https://oss.seedcom.vn","https://oss.juno.vn","https://oss.caudatfarm.vn"
            ,"https://sub.oss.seedcom.vn","https://sub.oss.juno.vn","https://sub.oss.caudatfarm.vn"
            ,"http://sub.oss.seedcom.vn","http://sub.oss.juno.vn","http://sub.oss.caudatfarm.vn"};
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if(request.getHeader("referer") != null){
            URL incom = new URL(request.getHeader("referer"));
            String domain = incom.getProtocol() + "://" +   // "http" + "://
                    incom.getHost() + (incom.getPort() > 0 ? (":" + incom.getPort()):"");

            if (Arrays.stream(acceptedDomail).anyMatch(x -> x.equals(domain))){
                response.addHeader("Access-Control-Allow-Origin",domain);
            }
        }

        if (request.getHeader("Access-Control-Request-Method") != null && "OPTIONS".equals(request.getMethod())) {
            //LOG.trace("Sending Header....");
            // CORS "pre-flight" request
            response.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
            response.addHeader("Access-Control-Allow-Headers", "Authorization,x-auth-token");
            response.addHeader("Access-Control-Allow-Headers", "Content-Type");
            response.addHeader("Access-Control-Max-Age", "1");
            if (request.getMethod().equals("OPTIONS")) {
                response.setStatus(HttpServletResponse.SC_ACCEPTED);
                return;
            }
        }

        filterChain.doFilter(request, response);
    }

}