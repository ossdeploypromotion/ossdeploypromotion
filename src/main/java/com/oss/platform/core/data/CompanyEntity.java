package com.oss.platform.core.data;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "company")
public class CompanyEntity {
	@Id
	private int id;
	private String name;
	@Column(name = "minincome4tax", columnDefinition = "number")
	private Double minincome4tax;
	@Column(name = "excludesituation", columnDefinition = "binary_double")
	private Double excludesituation;
	private String moneyname;
	private int isroot;
	private String email;
	private String address;
	private String taxcode;
	private String phone;
	private String bookstoreid;
	private String phoneserviceurl;
	private Integer ismultidepartment;
	private String logo;
	private String domain;
	@ManyToOne
	@JoinColumn(name = "parent")
	private CompanyEntity parent;
	@OneToMany(mappedBy = "company")
	private List<DepartmentEntity> listChild;

	public CompanyEntity(int companyId) {
		this.id = companyId;
	}


	public String getBookstoreid() {
		return bookstoreid;
	}

	public void setBookstoreid(String bookstoreid) {
		this.bookstoreid = bookstoreid;
	}

	public CompanyEntity() {
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public Double getMinincome4tax() {
		return minincome4tax;
	}

	public void setMinincome4tax(Double minincome4tax) {
		this.minincome4tax = minincome4tax;
	}

	public Double getExcludesituation() {
		return excludesituation;
	}

	public void setExcludesituation(Double excludesituation) {
		this.excludesituation = excludesituation;
	}

	public String getMoneyname() {
		return moneyname;
	}

	public void setMoneyname(String moneyname) {
		this.moneyname = moneyname;
	}

	public int getIsroot() {
		return isroot;
	}

	public void setIsroot(int isroot) {
		this.isroot = isroot;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getTaxcode() {
		return taxcode;
	}

	public void setTaxcode(String taxcode) {
		this.taxcode = taxcode;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

    public String getPhoneserviceurl() {
        return phoneserviceurl;
    }

    public void setPhoneserviceurl(String phoneserviceurl) {
        this.phoneserviceurl = phoneserviceurl;
    }

	public Integer getIsmultidepartment() {
		return ismultidepartment;
	}

	public void setIsmultidepartment(Integer ismultidepartment) {
		this.ismultidepartment = ismultidepartment;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public CompanyEntity getParent() {
		return parent;
	}

	public void setParent(CompanyEntity parent) {
		this.parent = parent;
	}

	public List<DepartmentEntity> getListChild() {
		return listChild;
	}

	public void setListChild(List<DepartmentEntity> listChild) {
		this.listChild = listChild;
	}
}
