package com.oss.platform.core.data;

import javax.persistence.*;

@Entity
@Table(name = "departmentem")
public class DepartmentEmEntity {
    @Id
    private Long id;
    private Integer weight;

    @ManyToOne
    @JoinColumn(name = "departmentid")
    private DepartmentEntity department;
    @ManyToOne
    @JoinColumn(name = "username")
    private UserEntity user;
    @ManyToOne
    @JoinColumn(name = "jobtitle")
    private JobTitleEntity jobTitle;

    public DepartmentEmEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public DepartmentEntity getDepartment() {
        return department;
    }

    public void setDepartment(DepartmentEntity department) {
        this.department = department;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public JobTitleEntity getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(JobTitleEntity jobTitle) {
        this.jobTitle = jobTitle;
    }
}
