package com.oss.platform.core.data;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "department")
public class DepartmentEntity {
	@Id
	private Integer id;
	private String name;
	private Integer isshop;
	private String regionid;
	private String address;
	private String pickupid;
	private String areaid;
	private Integer visible;
	private Integer isdelete;
	@ManyToOne(fetch = FetchType.LAZY)
	
	@JsonIgnore
	@JoinColumn(name = "companyid")
	private CompanyEntity company;
	
	// cannot map parent department because data is not consistan

	@ManyToOne(optional = true, fetch=FetchType.LAZY)
	@JoinColumn(name = "parentid")
	@NotFound(action= NotFoundAction.IGNORE)
	private DepartmentEntity parent;
	
	@OneToMany(mappedBy = "parent")
	private List<DepartmentEntity> listChild;
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "departmentem", joinColumns = {
			@JoinColumn(name = "departmentid", nullable = false, updatable = false) },
			inverseJoinColumns = { @JoinColumn(name = "username",
					nullable = false, updatable = false) })
	private List<UserEntity> listUser;

	public DepartmentEntity(){}

	public DepartmentEntity(Integer departmentId) {
		this.id = departmentId;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof DepartmentEntity) {
			return id.equals(((DepartmentEntity)obj).getId());
		}
		return false;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getIsshop() {
		return isshop;
	}

	public void setIsshop(Integer isshop) {
		this.isshop = isshop;
	}

	public String getRegionid() {
		return regionid;
	}

	public void setRegionid(String regionid) {
		this.regionid = regionid;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPickupid() {
		return pickupid;
	}

	public void setPickupid(String pickupid) {
		this.pickupid = pickupid;
	}

	public String getAreaid() {
		return areaid;
	}

	public void setAreaid(String areaid) {
		this.areaid = areaid;
	}

	public Integer getVisible() {
		return visible;
	}

	public void setVisible(Integer visible) {
		this.visible = visible;
	}

	public Integer getIsdelete() {
		return isdelete;
	}

	public void setIsdelete(Integer isdelete) {
		this.isdelete = isdelete;
	}

	public CompanyEntity getCompany() {
		return company;
	}

	public void setCompany(CompanyEntity company) {
		this.company = company;
	}

	public DepartmentEntity getParent() {
		return parent;
	}

	public void setParent(DepartmentEntity parent) {
		this.parent = parent;
	}

	public List<DepartmentEntity> getListChild() {
		return listChild;
	}

	public void setListChild(List<DepartmentEntity> listChild) {
		this.listChild = listChild;
	}

	public List<UserEntity> getListUser() {
		return listUser;
	}

	public void setListUser(List<UserEntity> listUser) {
		this.listUser = listUser;
	}
}
