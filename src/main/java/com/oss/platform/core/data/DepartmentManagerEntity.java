package com.oss.platform.core.data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "departmentmanager")
public class DepartmentManagerEntity {
    @Id
    private Long id;
    private String username;
    @Column(name = "starttime")
    private Date startTime;
    @Column(name = "endtime")
    private Date endTime;
    @ManyToOne
    @JoinColumn(name = "departmentid")
    private DepartmentEntity department;
    @ManyToOne
    @JoinColumn(name = "companyid")
    private CompanyEntity company;
    private Integer type;
    @Column(name = "isreal")
    private Integer isReal;

    public DepartmentManagerEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public DepartmentEntity getDepartment() {
        return department;
    }

    public void setDepartment(DepartmentEntity department) {
        this.department = department;
    }

    public CompanyEntity getCompany() {
        return company;
    }

    public void setCompany(CompanyEntity company) {
        this.company = company;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getIsReal() {
        return isReal;
    }

    public void setIsReal(Integer isReal) {
        this.isReal = isReal;
    }
}
