package com.oss.platform.core.data;

import javax.persistence.*;

/**
 * Created by W540 on 2/7/2017.
 */
@Entity
@Table(name = "jobtitle")
public class JobTitleEntity {
    @Id
    Long ID;
    String name;
    @ManyToOne
    @JoinColumn(name = "companyid")
    private CompanyEntity company;

    @Column(name = "prioritylevel")
    Integer priorityLevel;

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CompanyEntity getCompany() {
        return company;
    }

    public void setCompany(CompanyEntity company) {
        this.company = company;
    }

    public Integer getPriorityLevel() {
        return priorityLevel;
    }

    public void setPriorityLevel(Integer priorityLevel) {
        this.priorityLevel = priorityLevel;
    }
}
