package com.oss.platform.core.data;

import javax.persistence.*;

/**
 * Created by Vu on 02/09/2017.
 */
@Entity
@Table(name = "LEFTMENU")
public class LeftMenuEntity {

    @Id
    @GeneratedValue(generator="SEQ_LEFTMENU")
    @SequenceGenerator(name="SEQ_LEFTMENU",sequenceName="SEQ_LEFTMENU")
    @Column(name = "id")
    private Long id;
    private String name;
    private String link;
    private String icon;
    @Column(name = "parentid")
    private Long parentId;
    @Column(name = "ordermenu")
    private Integer orderMenu;
    @Column(name = "isexpanded")
    private Integer isExpanded;
    private Integer type;
    @Column(name = "companyid")
    private Integer companyId;
    @Column(name = "isnewtab")
    private Integer isNewTab;
    @Column(name = "subdomain")
    private String subDomain;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Integer getOrderMenu() {
        return orderMenu;
    }

    public void setOrderMenu(Integer orderMenu) {
        this.orderMenu = orderMenu;
    }

    public Integer getIsExpanded() {
        return isExpanded;
    }

    public void setIsExpanded(Integer isExpanded) {
        this.isExpanded = isExpanded;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public Integer getIsNewTab() {
        return isNewTab;
    }

    public void setIsNewTab(Integer isNewTab) {
        this.isNewTab = isNewTab;
    }

    public String getSubDomain() {
        return subDomain;
    }

    public void setSubDomain(String subDomain) {
        this.subDomain = subDomain;
    }

    public LeftMenuEntity() {

    }

    public LeftMenuEntity(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(!obj.getClass().equals(LeftMenuEntity.class)){
            return false;
        }
        LeftMenuEntity com = (LeftMenuEntity) obj;
        return com.getId().equals(this.getId());
    }
}
