package com.oss.platform.core.data;


import javax.persistence.*;

/**
 * Created by Vu on 02/09/2017.
 */
@Entity
@Table(name = "LEFTMENUJOBTITLE")
public class LeftMenuJobTitleEntity {

    @Id
    @GeneratedValue(generator="SEQ_LEFTMENUJOBTITLE")
    @SequenceGenerator(name="SEQ_LEFTMENUJOBTITLE",sequenceName="SEQ_LEFTMENUJOBTITLE")
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "jobtitleid", referencedColumnName = "id")
    private JobTitleEntity jobtitle;

    @ManyToOne
    @JoinColumn(name = "leftmenuid", referencedColumnName = "id")
    private LeftMenuEntity leftmenu;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public JobTitleEntity getJobtitle() {
        return jobtitle;
    }

    public void setJobtitle(JobTitleEntity jobtitle) {
        this.jobtitle = jobtitle;
    }

    public LeftMenuEntity getLeftmenu() {
        return leftmenu;
    }

    public void setLeftmenu(LeftMenuEntity leftmenu) {
        this.leftmenu = leftmenu;
    }

    public LeftMenuJobTitleEntity() {
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(!obj.getClass().equals(LeftMenuJobTitleEntity.class)){
            return false;
        }
        LeftMenuJobTitleEntity com = (LeftMenuJobTitleEntity) obj;
        return com.getId().equals(this.getId());
    }
}
