package com.oss.platform.core.data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "USERS")
public class UserEntity {
	//type != 4 status =1 enable=1 => active user
	@Id
	private String username;
	private String password;
	@JsonIgnore
	private boolean enabled;
	private Integer groupid;
	private String fullname;
	@JsonIgnore
	private String agentcode;
	@OneToMany(mappedBy = "user")
	private List<UserRoleEntity> userRole;

	private String email;
	private Integer type;
	private Integer status;
	private String photo;
	@ManyToOne
	@JoinColumn(name = "jobtitleid", referencedColumnName = "id")
	@NotFound(action= NotFoundAction.IGNORE)
	private JobTitleEntity jobTitle;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "departmentid", referencedColumnName = "id")
	@NotFound(action= NotFoundAction.IGNORE)
	DepartmentEntity department;
	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "listUser")
	List<DepartmentEntity> departments;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "companyid", referencedColumnName = "id")
	@NotFound(action= NotFoundAction.IGNORE)
	CompanyEntity company;


	@Transient
	private long expires;



	public UserEntity() {
	}

	public UserEntity(String username, String password, boolean enabled) {
		this.username = username;
		this.password = password;
		this.enabled = enabled;
	}

	public UserEntity(String username, String password, boolean enabled, List<UserRoleEntity> userRole) {
		this.username = username;
		this.password = password;
		this.enabled = enabled;
		this.userRole = userRole;
	}

	public UserEntity(String username) {
		this.username = username;
	}

    public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isEnabled() {
		return this.enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public List<UserRoleEntity> getUserRole() {
		return this.userRole;
	}

	public void setUserRole(List<UserRoleEntity> userRole) {
		this.userRole = userRole;
	}

	public Integer getGroupid() {
		return groupid;
	}

	public void setGroupid(Integer groupid) {
		this.groupid = groupid;
	}

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getAgentcode() {
        return agentcode;
    }

    public void setAgentcode(String agentcode) {
        this.agentcode = agentcode;
    }

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public JobTitleEntity getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(JobTitleEntity jobTitle) {
		this.jobTitle = jobTitle;
	}

	public DepartmentEntity getDepartment() {
		return department;
	}

	public void setDepartment(DepartmentEntity department) {
		this.department = department;
	}

	public CompanyEntity getCompany() {
		return company;
	}

	public void setCompany(CompanyEntity company) {
		this.company = company;
	}

	public void setExpires(long expires) {
		this.expires = expires;
	}

	public long getExpires() {
		return expires;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		UserEntity that = (UserEntity) o;

		return username.equals(that.username);
	}

	@Override
	public int hashCode() {
		return username.hashCode();
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public List<DepartmentEntity> getDepartments() {
		return departments;
	}

	public void setDepartments(List<DepartmentEntity> departments) {
		this.departments = departments;
	}
	
}
