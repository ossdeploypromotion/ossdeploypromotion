package com.oss.platform.core.dto;

import com.oss.platform.core.data.UserEntity;

/**
 * Created by W540 on 4/18/2017.
 */
public class AttachFileDTO {
    private Long attachFileId;
    private String fileName;
    private String path;
    private String path_medium;
    private String path_min;
    private String extension;
    private Long size;
    private String owner;


    public AttachFileDTO() {
    }

    public AttachFileDTO(Long attachFileId, String fileName, String path, String fileExtension, Long fileSize, UserEntity user) {
        this.attachFileId = attachFileId;
        this.fileName = fileName;
        this.path = path;
        this.extension = fileExtension;
        this.size = fileSize;
        if(user!=null){
            this.owner = user.getUsername();
        }

    }

    public Long getAttachFileId() {
        return attachFileId;
    }

    public void setAttachFileId(Long attachFileId) {
        this.attachFileId = attachFileId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getExtension() {
        return extension;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public Long getSize() {
        return size;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getPath_medium() {
        return path_medium;
    }

    public void setPath_medium(String path_medium) {
        this.path_medium = path_medium;
    }

    public String getPath_min() {
        return path_min;
    }

    public void setPath_min(String path_min) {
        this.path_min = path_min;
    }
}
