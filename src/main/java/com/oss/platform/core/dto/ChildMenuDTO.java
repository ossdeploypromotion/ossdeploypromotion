package com.oss.platform.core.dto;


/**
 * Created by Vu on 02/13/2017.
 */
public class ChildMenuDTO {
    private String title;
    private String url;
    private Long parentid;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getParentid() {
        return parentid;
    }

    public void setParentid(Long parentid) {
        this.parentid = parentid;
    }
}
