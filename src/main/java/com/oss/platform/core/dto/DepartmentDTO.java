package com.oss.platform.core.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.oss.platform.core.data.CompanyEntity;

import java.util.List;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by W540 on 1/24/2017.
 */
public class DepartmentDTO {
    private Integer id;
    private String name;
    private Integer isshop;
    private String regionid;
    private String address;
    private String pickupid;
    private String areaid;
    private Integer visible;
    private Integer isdelete;
    private CompanyDTO company;
    private List<UserDTO> listUser;
    private List<DepartmentDTO> listChild;
    private DepartmentDTO parent;
    private Integer parentId;

    public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public DepartmentDTO() {
    }

    public DepartmentDTO(int id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIsshop() {
        return isshop;
    }

    public void setIsshop(Integer isshop) {
        this.isshop = isshop;
    }

    public String getRegionid() {
        return regionid;
    }

    public void setRegionid(String regionid) {
        this.regionid = regionid;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPickupid() {
        return pickupid;
    }

    public void setPickupid(String pickupid) {
        this.pickupid = pickupid;
    }

    public String getAreaid() {
        return areaid;
    }

    public void setAreaid(String areaid) {
        this.areaid = areaid;
    }

    public Integer getVisible() {
        return visible;
    }

    public void setVisible(Integer visible) {
        this.visible = visible;
    }

    public Integer getIsdelete() {
        return isdelete;
    }

    public void setIsdelete(Integer isdelete) {
        this.isdelete = isdelete;
    }

    public CompanyDTO getCompany() {
        return company;
    }

    public void setCompany(CompanyDTO company) {
        this.company = company;
    }

	public List<UserDTO> getListUser() {
		return listUser;
	}

	public void setListUser(List<UserDTO> listUser) {
		this.listUser = listUser;
	}

	public List<DepartmentDTO> getListChild() {
		return listChild;
	}

	public void setListChild(List<DepartmentDTO> listChild) {
		this.listChild = listChild;
	}

	public DepartmentDTO getParent() {
		return parent;
	}

	public void setParent(DepartmentDTO parent) {
		this.parent = parent;
	}

    
}
