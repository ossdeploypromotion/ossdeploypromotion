package com.oss.platform.core.dto;

/**
 * Created by W540 on 2/7/2017.
 */
public class JobTitleDTO {
    Long ID;
    String name;

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
