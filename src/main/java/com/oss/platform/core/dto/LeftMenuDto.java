package com.oss.platform.core.dto;


public class LeftMenuDto {

	private Integer id;
	private String name;
	private String link;
	private String icon;
	private Integer parentid;
	private Integer ordermenu;
	private Integer isexpanded;
	private Integer type;
	private Integer isnewtab;
	
	public LeftMenuDto() {
	}


	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}


	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}


	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * @return the link
	 */
	public String getLink() {
		return link;
	}


	/**
	 * @param link the link to set
	 */
	public void setLink(String link) {
		this.link = link;
	}


	/**
	 * @return the icon
	 */
	public String getIcon() {
		return icon;
	}


	/**
	 * @param icon the icon to set
	 */
	public void setIcon(String icon) {
		this.icon = icon;
	}


	/**
	 * @return the parentid
	 */
	public Integer getParentid() {
		return parentid;
	}


	/**
	 * @param parentid the parentid to set
	 */
	public void setParentid(Integer parentid) {
		this.parentid = parentid;
	}


	/**
	 * @return the isexpanded
	 */
	public Integer getIsexpanded() {
		return isexpanded;
	}


	/**
	 * @param isexpanded the isexpanded to set
	 */
	public void setIsexpanded(Integer isexpanded) {
		this.isexpanded = isexpanded;
	}


	/**
	 * @return the type
	 */
	public Integer getType() {
		return type;
	}


	/**
	 * @param type the type to set
	 */
	public void setType(Integer type) {
		this.type = type;
	}


	/**
	 * @return the ordermenu
	 */
	public Integer getOrdermenu() {
		return ordermenu;
	}


	/**
	 * @param ordermenu the ordermenu to set
	 */
	public void setOrdermenu(Integer ordermenu) {
		this.ordermenu = ordermenu;
	}


	public Integer getIsnewtab() {
		return isnewtab;
	}


	public void setIsnewtab(Integer isnewtab) {
		this.isnewtab = isnewtab;
	}

}
