package com.oss.platform.core.dto;


import java.util.List;

/**
 * Created by Vu on 02/13/2017.
 */
public class ParentMenuDTO {
    private String title;
    private String faIconClass;
    private List<ChildMenuDTO> subMenu;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFaIconClass() {
        return faIconClass;
    }

    public void setFaIconClass(String faIconClass) {
        this.faIconClass = faIconClass;
    }

    public List<ChildMenuDTO> getSubMenu() {
        return subMenu;
    }

    public void setSubMenu(List<ChildMenuDTO> subMenu) {
        this.subMenu = subMenu;
    }
}
