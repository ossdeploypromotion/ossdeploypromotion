package com.oss.platform.core.dto;

/**
 * Created by W540 on 4/14/2017.
 */
public class SearchEmployeeDTO {
    String username;
    String fullName;
    String jobTitle;
    String photo;
    Integer companyId;

    public SearchEmployeeDTO() {
    }

    public SearchEmployeeDTO(String username, String fullName, String jobTitle, String photo, Integer companyId) {
        this.username = username;
        this.fullName = fullName;
        this.jobTitle = jobTitle;
        this.photo = photo;
        this.companyId = companyId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
