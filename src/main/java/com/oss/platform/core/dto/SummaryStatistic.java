package com.oss.platform.core.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Ballard
 */

public class SummaryStatistic implements Serializable{
	private Date lastUpdated;
	private Integer finishPercent;
	private Integer linkedPercent;
	private Integer	TotalObjective;
	public Date getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	public Integer getFinishPercent() {
		return finishPercent;
	}
	public void setFinishPercent(Integer finishPercent) {
		this.finishPercent = finishPercent;
	}
	public Integer getLinkedPercent() {
		return linkedPercent;
	}
	public void setLinkedPercent(Integer linkedPercent) {
		this.linkedPercent = linkedPercent;
	}
	public Integer getTotalObjective() {
		return TotalObjective;
	}
	public void setTotalObjective(Integer totalObjective) {
		TotalObjective = totalObjective;
	}
 }