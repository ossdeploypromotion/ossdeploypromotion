package com.oss.platform.core.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.List;

/**
 * Created by W540 on 1/24/2017.
 */
public class UserDTO {
    private String username;
    @JsonIgnore
    private String password;
    private boolean enabled;
    private Integer groupid;
    private String fullname;
    private String agentcode;
    private List<UserRoleDTO> userRole;
    private long expires;
    private String email;
    private String photo;
    private JobTitleDTO jobTitle;
    private DepartmentDTO department;
    private CompanyDTO company;
    private boolean needSlectCompany;

    public UserDTO() {

    }
    public UserDTO(String username, String photo) {
        this.username = username;
        this.photo = photo;
    }

    public UserDTO(String name) {
        this.username = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Integer getGroupid() {
        return groupid;
    }

    public void setGroupid(Integer groupid) {
        this.groupid = groupid;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getAgentcode() {
        return agentcode;
    }

    public void setAgentcode(String agentcode) {
        this.agentcode = agentcode;
    }

    public List<UserRoleDTO> getUserRole() {
        return userRole;
    }

    public void setUserRole(List<UserRoleDTO> userRole) {
        this.userRole = userRole;
    }

    public long getExpires() {
        return expires;
    }

    public void setExpires(long expires) {
        this.expires = expires;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public JobTitleDTO getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(JobTitleDTO jobTitle) {
        this.jobTitle = jobTitle;
    }

    public DepartmentDTO getDepartment() {
        return department;
    }

    public void setDepartment(DepartmentDTO department) {
        this.department = department;
    }

    public CompanyDTO getCompany() {
        return company;
    }

    public void setCompany(CompanyDTO company) {
        this.company = company;
    }

    public void setNeedSlectCompany(boolean needSlectCompany) {
        this.needSlectCompany = needSlectCompany;
    }

    public boolean isNeedSlectCompany() {
        return needSlectCompany;
    }
}
