package com.oss.platform.core.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Ballard
 */

public class UserSimp implements Serializable{
	private String fullNameUser;
	private String userName;
	private String avatar;
	private Date createdDate;
	
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	public String getFullNameUser() {
		return fullNameUser;
	}
	public void setFullNameUser(String fullNameUser) {
		this.fullNameUser = fullNameUser;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public static UserSimp copyFrom(UserDTO userDTO){
		UserSimp userSimp = new UserSimp();
		userSimp.setAvatar(userDTO.getPhoto());
		userSimp.setFullNameUser(userDTO.getFullname());
		userSimp.setUserName(userDTO.getUsername());
		userSimp.setCreatedDate(new Date());
		return userSimp;
	}
 }
