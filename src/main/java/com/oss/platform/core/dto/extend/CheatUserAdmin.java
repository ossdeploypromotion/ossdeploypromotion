package com.oss.platform.core.dto.extend;

import com.oss.platform.security.OssUserDetails;
import com.oss.platform.security.OssUserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by W540 on 4/3/2017.
 */
public class CheatUserAdmin {
    List<String> cheatUsernameList = new ArrayList<>();
    public void addCheatAdmin(String userName){
        cheatUsernameList.add(userName);
    }
    public Boolean isCheatAdmin(){
        OssUserPrincipal currentUser = (OssUserPrincipal)SecurityContextHolder.getContext().getAuthentication().getDetails();
        if (cheatUsernameList.indexOf(currentUser.getUsername())>-1){
            return true;
        }
        return false;
    }
}
