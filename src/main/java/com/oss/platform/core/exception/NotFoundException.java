package com.oss.platform.core.exception;

/**
 * Created by W540 on 1/23/2017.
 */
public class NotFoundException extends Exception{
    public NotFoundException(String message) {
        super(message);
    }
}
