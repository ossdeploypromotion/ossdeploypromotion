package com.oss.platform.core.exception;

/**
 * Created by W540 on 1/25/2017.
 */
public class UserNotFoundException extends Throwable {
    public UserNotFoundException(String objectiveId) {
        super("Objective not found exception");
    }
}
