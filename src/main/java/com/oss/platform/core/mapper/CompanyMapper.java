package com.oss.platform.core.mapper;

import com.oss.platform.core.data.CompanyEntity;
import com.oss.platform.core.dto.CompanyDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by W540 on 2/7/2017.
 */
public class CompanyMapper {

    public static CompanyDTO map(CompanyEntity company) {
        // TODO: add more properties
        if(company == null){
            return null;
        }
        CompanyDTO companyDTO =  new CompanyDTO();
        companyDTO.setId(company.getId());
        companyDTO.setAddress(company.getAddress());
        companyDTO.setName(company.getName());
        companyDTO.setBookstoreid(company.getBookstoreid());
        companyDTO.setDomain(company.getDomain());
        companyDTO.setEmail(company.getEmail());
        companyDTO.setLogo(company.getLogo());
        companyDTO.setIsroot(company.getIsroot());
        return companyDTO;
    }

    public static List<CompanyDTO> map(List<CompanyEntity> companyEntities) {
        List<CompanyDTO> companyDTOS = new ArrayList<>();
        for(CompanyEntity entity: companyEntities){
            companyDTOS.add(CompanyMapper.map(entity));
        }
        return companyDTOS;
    }

    public static CompanyDTO mapFull1LevelDep(CompanyEntity company) {
        if(company == null){
            return null;
        }
        CompanyDTO companyDTO =  new CompanyDTO();
        companyDTO.setId(company.getId());
        companyDTO.setAddress(company.getAddress());
        companyDTO.setName(company.getName());
        companyDTO.setBookstoreid(company.getBookstoreid());
        companyDTO.setDomain(company.getDomain());
        companyDTO.setEmail(company.getEmail());
        companyDTO.setLogo(company.getLogo());
        companyDTO.setIsroot(company.getIsroot());
        companyDTO.setDepartment(DepartmentMapper.mapBasicTwoLevel(company.getListChild()));
        return companyDTO;
    }
}
