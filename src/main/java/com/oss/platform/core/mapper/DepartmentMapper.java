package com.oss.platform.core.mapper;

import com.oss.platform.core.data.DepartmentEntity;
import com.oss.platform.core.dto.DepartmentDTO;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * Created by W540 on 1/24/2017.
 */
public class DepartmentMapper {
    public static List<DepartmentDTO> map(List<DepartmentEntity> listUserDepartMent) {
        return listUserDepartMent.stream().map(entity -> map(entity)).collect(Collectors.toList());
    }
    public static DepartmentDTO map(DepartmentEntity entity){
        if(entity == null){
            return null;
        }
        DepartmentDTO result = new DepartmentDTO();
        result.setId(entity.getId());
        result.setAddress(entity.getAddress());
        result.setName(entity.getName());
        return result;
    }
    
    public static List<DepartmentDTO> mapFull(List<DepartmentEntity> listDep) {
        return listDep.stream().map(entity -> mapFull(entity)).collect(Collectors.toList());
    }

    public static DepartmentDTO mapFull(DepartmentEntity entity){
        if(entity == null){
            return null;
        }
        DepartmentDTO result = new DepartmentDTO();
        result.setId(entity.getId());
        result.setName(entity.getName());
//        if(entity.getListChild().se)
        result.setListChild(mapFull(entity.getListChild()));
        result.setListUser(UserMapper.map(entity.getListUser()));
        result.setParent(map(entity.getParent()));
        return result;
    }
    
    public static List<DepartmentDTO> mapBasicTwoLevel(List<DepartmentEntity> listChild) {
        return  listChild.stream().map(DepartmentMapper::mapBasicTwoLevel).collect(Collectors.toList());
    }
    public static DepartmentDTO mapBasicTwoLevel(DepartmentEntity entity){
        DepartmentDTO result = new DepartmentDTO();
        result.setId(entity.getId());
        result.setName(entity.getName());
        List<DepartmentEntity> lstChildL2 = entity.getListChild();
        if(lstChildL2 != null){
        	result.setListChild(lstChildL2.stream().map(DepartmentMapper::mapBasic).collect(Collectors.toList()));
        }
        return result;
    }

    public static List<DepartmentDTO> mapBasic(List<DepartmentEntity> listChild) {
        return  listChild.stream().map(DepartmentMapper::mapBasic).collect(Collectors.toList());
    }
    public static DepartmentDTO mapBasic(DepartmentEntity entity){
        DepartmentDTO result = new DepartmentDTO();
        result.setId(entity.getId());
        result.setName(entity.getName());
        return result;
    }
    
    public static List<DepartmentDTO> mapBasicHaveParent(List<DepartmentEntity> listChild) {
        return  listChild.stream().map(DepartmentMapper::mapBasicHaveParent).collect(Collectors.toList());
    }
    public static DepartmentDTO mapBasicHaveParent(DepartmentEntity entity){
        DepartmentDTO result = new DepartmentDTO();
        result.setId(entity.getId());
        result.setName(entity.getName());
        Integer parentId = entity.getParent() == null ? 0 : entity.getParent().getId();
        result.setParentId(parentId);
        result.setIsshop(entity.getIsshop());

        return result;
    }

}
