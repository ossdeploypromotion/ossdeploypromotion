package com.oss.platform.core.mapper;

import com.oss.platform.core.data.JobTitleEntity;
import com.oss.platform.core.dto.JobTitleDTO;

/**
 * Created by W540 on 2/7/2017.
 */
public class JobTitleMapper {

    public static JobTitleDTO map(JobTitleEntity jobTitle) {
        if (jobTitle == null){
            return null;
        }
        JobTitleDTO result =  new JobTitleDTO();
        result.setID(jobTitle.getID());
        result.setName(jobTitle.getName());
        return result;
    }
}
