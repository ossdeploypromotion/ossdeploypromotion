package com.oss.platform.core.mapper;

import com.oss.platform.core.data.*;
import com.oss.platform.core.dto.CompanyDTO;
import com.oss.platform.core.dto.DepartmentDTO;
import com.oss.platform.core.dto.JobTitleDTO;
import com.oss.platform.core.dto.UserDTO;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;

/**
 * Created by W540 on 1/24/2017.
 */
public class UserMapper {
    public static UserDTO map(UserEntity user) {
        if(user==null){
            return null;
        }
        UserDTO userDTO = new UserDTO();
        userDTO.setUsername(user.getUsername());
        userDTO.setAgentcode(user.getAgentcode());
        userDTO.setEnabled(user.isEnabled());
        userDTO.setExpires(user.getExpires());
        userDTO.setFullname(user.getFullname());
        userDTO.setPassword(user.getPassword());
        userDTO.setPhoto(user.getPhoto());
        if(user.getUserRole() != null){
            userDTO.setUserRole(UserRoleMapper.map(user.getUserRole()));
        }
        if(user.getCompany() !=null){
            userDTO.setCompany(CompanyMapper.map(user.getCompany()));
        }
        if(user.getDepartment() != null){
            userDTO.setDepartment(DepartmentMapper.map(user.getDepartment()));
        }
        if(user.getJobTitle() != null){
            userDTO.setJobTitle(JobTitleMapper.map(user.getJobTitle()));
        }
        return userDTO;
    }

    
    public static List<UserDTO> map(List<UserEntity> listResult) {
        List<UserDTO> result = new ArrayList<>();
        for (UserEntity entity: listResult){
            result.add(map(entity));
        }
        return result;
    }

    public static UserDTO mapInfo(UserEntity owner) {
        if (owner == null){
            return null;
        }
        UserDTO userDTO = new UserDTO();
        userDTO.setFullname(owner.getFullname());
        userDTO.setUsername(owner.getUsername());
        userDTO.setPhoto(owner.getPhoto());
        return userDTO;
    }

    public static UserDTO mapForListParticipantOfGroup(UserEntity user) {
        UserDTO userDTO = new UserDTO();
        userDTO.setUsername(user.getUsername());
        userDTO.setFullname(user.getFullname());
        userDTO.setPhoto(user.getPhoto());
        if(user.getJobTitle() != null){
            userDTO.setJobTitle(JobTitleMapper.map(user.getJobTitle()));
        }
        return userDTO;
    }

    public static List<UserDTO> mapForListParticipantOfGroup(List<UserEntity> listResult) {
        List<UserDTO> result = new ArrayList<>();
        for (UserEntity entity: listResult){
            result.add(mapForListParticipantOfGroup(entity));
        }
        return result;
    }

    public static UserDTO mapMiniInfor(UserEntity userEntity) {
        UserDTO userDTO = new UserDTO();
        if(userEntity == null){
            return null;
        }
        userDTO.setUsername(userEntity.getUsername());
        userDTO.setPhoto(userEntity.getPhoto());
        userDTO.setFullname(userEntity.getFullname());
        userDTO.setJobTitle(JobTitleMapper.map(userEntity.getJobTitle()));
        userDTO.setDepartment(DepartmentMapper.map(userEntity.getDepartment()));
        return userDTO;
    }

    public static List<UserDTO> mapAuthenInfo(List<UserEntity> users) {
        List<UserDTO> listUser = new ArrayList<>();
        for (UserEntity entity :users){
            listUser.add(mapAuthenInfo(entity));
        }
       return listUser;
    }
    public static UserDTO mapAuthenInfo(UserEntity userEntity, CompanyEntity company, DepartmentEntity userDepartment) {
        UserDTO result = new UserDTO();
        result.setUsername(userEntity.getUsername());
        DepartmentDTO departmentDTO = new DepartmentDTO();
        departmentDTO.setId(userDepartment.getId());
        departmentDTO.setName(userDepartment.getName());
        result.setDepartment(departmentDTO);
        CompanyDTO companyDTO = new CompanyDTO();
        companyDTO.setId(company.getId());
        companyDTO.setName(company.getName());
        result.setCompany(companyDTO);
        result.setFullname(userEntity.getFullname());
        result.setUserRole(UserRoleMapper.map(userEntity.getUserRole()));
        result.setJobTitle(JobTitleMapper.map(userEntity.getJobTitle()));
        result.setPhoto(userEntity.getPhoto());
        return result;
    }
    public static UserDTO map(UserEntity userEntity, CompanyEntity companyEntity, DepartmentEntity department, JobTitleEntity jobTitle) {
        UserDTO userDTO = map(userEntity,companyEntity,department);
        userDTO.setJobTitle(JobTitleMapper.map(jobTitle));
        return userDTO;
    }
    private static UserDTO mapAuthenInfo(UserEntity entity) {
        UserDTO result = new UserDTO();
        result.setUsername(entity.getUsername());
        DepartmentDTO departmentDTO = new DepartmentDTO();
        departmentDTO.setId(entity.getDepartment().getId());
        departmentDTO.setName(entity.getDepartment().getName());
        result.setDepartment(departmentDTO);
        CompanyDTO companyDTO = new CompanyDTO();
        companyDTO.setId(entity.getCompany().getId());
        companyDTO.setName(entity.getCompany().getName());
        result.setCompany(companyDTO);
        result.setFullname(entity.getFullname());
        result.setUserRole(UserRoleMapper.map(entity.getUserRole()));
        result.setJobTitle(JobTitleMapper.map(entity.getJobTitle()));
        result.setPhoto(entity.getPhoto());
        return result;
    }

    public static UserDTO map(UserEntity userEntity, CompanyEntity companyEntity, DepartmentEntity departmentEntity) {
        if (userEntity == null){
            return null;
        }
        UserDTO result = mapMiniInfor(userEntity);
        result.setCompany(CompanyMapper.map(companyEntity));
        result.setDepartment(DepartmentMapper.map(departmentEntity));
        return result;
    }


    public static Set<UserDTO> mapUserInfo(Set<UserEntity> followers) {
        return followers.stream().map(userEntity -> mapMiniInfor(userEntity)).collect(Collectors.toSet());
    }

    public static Set<UserDTO> mapUserInfo(List<UserEntity> allFollowUser) {
        return allFollowUser.stream().map(userEntity -> mapMiniInfor(userEntity)).collect(Collectors.toSet());
    }



}
