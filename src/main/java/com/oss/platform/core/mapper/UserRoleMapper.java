package com.oss.platform.core.mapper;

import com.oss.platform.core.data.UserRoleEntity;
import com.oss.platform.core.dto.UserDTO;
import com.oss.platform.core.dto.UserRoleDTO;

import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * Created by W540 on 1/24/2017.
 */
public class UserRoleMapper {
    public static List<UserRoleDTO> map(List<UserRoleEntity> userRole) {
        return userRole.stream().map(entity->map(entity)).collect(Collectors.toList());
    }

    private static UserRoleDTO map(UserRoleEntity entity) {
        UserRoleDTO userRoleDTO = new UserRoleDTO();
        userRoleDTO.setRole(entity.getRole());
        userRoleDTO.setUserRoleId(entity.getUserRoleId());
        return userRoleDTO;
    }
}
