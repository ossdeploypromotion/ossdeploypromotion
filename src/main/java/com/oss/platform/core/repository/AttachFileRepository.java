package com.oss.platform.core.repository;

import com.oss.platform.core.data.AttachFile;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by W540 on 4/20/2017.
 */
public interface AttachFileRepository extends MongoRepository<AttachFile,String>{
}
