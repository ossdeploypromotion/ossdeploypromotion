package com.oss.platform.core.repository;

import com.oss.platform.core.data.CompanyEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by W540 on 1/24/2017.
 */
public interface CompanyRepository extends JpaRepository<CompanyEntity, Integer> {
    @Query("select distinct (de.department.company) from DepartmentEmEntity de where de.user.type <> 4 " +
            "and de.user.status=1 and de.user.enabled = true and de.user.username =:username ")
    List<CompanyEntity> findAllCompanyForUser(@Param("username") String username);

    @Query("select count (de.department) from DepartmentEmEntity de where de.user.type <> 4 " +
            "and de.user.status=1 and de.user.enabled = true and de.user.username =:username and de.department.company.id =:companyId")
    Integer isUserOfCompany(@Param("username") String username, @Param("companyId") Integer companyId);
}
