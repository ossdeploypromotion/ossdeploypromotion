package com.oss.platform.core.repository;

import com.oss.platform.core.data.DepartmentEmEntity;
import com.oss.platform.core.data.DepartmentEntity;
import com.oss.platform.core.data.UserEntity;
import com.oss.platform.core.service.model.UserManagerModel;
import com.oss.platform.core.service.model.UserSameLevelModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.query.Param;

import javax.persistence.QueryHint;
import java.util.List;

/**
 * Created by W540 on 1/24/2017.
 */

public interface DepartmentRepository extends JpaRepository<DepartmentEntity,Integer>{
	@Query("from DepartmentEntity d where company.id = :companyid and isDelete = 0")
	List<DepartmentEntity> findAllByCompany(@Param("companyid") Integer companyId);
	
    @Query("select dem.department from DepartmentEmEntity dem where dem.user.username = :username")
    List<DepartmentEntity> findUserDepartMents(@Param("username") String username);

    @Query("from DepartmentEntity where company.id = :companyid and (parent is null or parent.id = 0) and isDelete = 0")
    List<DepartmentEntity> findTopMostDepartments(@Param("companyid") Integer companyid);

    @Query("from DepartmentEntity where parent.id = :departmentid and isDelete = 0")
    List<DepartmentEntity> findChildDepartments(@Param("departmentid") Integer departmentid);

    @Query("select d from DepartmentEntity d, DepartmentManagerEntity dm where d.id = dm.department.id and dm.username = :username and "
            + " current_date between dm.startTime and dm.endTime and dm.company.id = :companyid and dm.type = 1 and dm.isReal = 1")
    List<DepartmentEntity> findDepartmentUserIsBoss(@Param("username") String username, @Param("companyid") Integer companyid);

    @Query("select count(d) from DepartmentEntity d where company.id = :companyid and isDelete = 0")
    Long countNumOfDepartmentInCompany(@Param("companyid") Integer companyid);

    @Query("select dm.username from DepartmentEntity d , DepartmentManagerEntity dm where (d.id = dm.department.id) and d.id = :departmentId and "
            + " ((current_date between dm.startTime and dm.endTime) or dm.endTime is null) and dm.company.id = :companyid and dm.type = 1 and dm.isReal = 1")
    List<String> findListManager(@Param("departmentId") Integer departmentId, @Param("companyid") Integer companyId);


    @Query("select distinct (de.department) from DepartmentEmEntity de where de.user.type <> 4 " +
            "and de.user.status=1 and de.user.enabled = true and de.user.username =:username and de.department.company.id = :companyid")
    DepartmentEntity findDepartmentByUserAndcompany(@Param("username") String username, @Param("companyid") Integer companyid);

    @Query("select distinct (de.department) from DepartmentEmEntity de where de.user.type <> 4 " +
            "and de.user.status=1 and de.user.enabled = true and de.user.username =:username and de.department.company.domain = :domainname")
    DepartmentEntity getDepartmentByDomainAndUser(@Param("username") String username, @Param("domainname") String domainName);

    @Query("select distinct dm.username from DepartmentEntity d, DepartmentManagerEntity dm where d.id = dm.department.id and dm.username = :username and "
            + " ((current_date between dm.startTime and dm.endTime) or dm.endTime is null) and dm.company.id = :companyid and dm.type = 1")
    List<String> findBossByUsername(@Param("username") String username, @Param("companyid") int companyId);

    @Query("select user.username from DepartmentEmEntity where  department.id = :deparmentId")
    @QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value ="true") })
    List<String> findSameLevelUserByUserName(@Param("deparmentId") int deparmentId);
    @Query("select distinct  new com.oss.platform.core.service.model.UserManagerModel(de.user.id,dm.username) from DepartmentEmEntity de, DepartmentManagerEntity dm where de.department.id = dm.department.id and de.user.id <> dm.username  and ((current_date between dm.startTime and dm.endTime) or dm.endTime is null) " +
            " and de.department.company.id =:companyid and de.user.id in (:relativeUser)")
    List<UserManagerModel> findManagerForListUser(@Param("relativeUser") List<String> relativeUser, @Param("companyid") int companyid);
    @Query("select new com.oss.platform.core.service.model.UserSameLevelModel(de.user.id,desame.user.id) from DepartmentEmEntity de, DepartmentEmEntity desame where de.department.id = desame.department.id and de.user.id <> desame.user.id" +
            " and de.department.company.id =:companyid and de.user.id in (:relativeUser)")
    @QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value ="true") })
    List<UserSameLevelModel> findSameLevelForListUser(@Param("relativeUser") List<String> relativeUser, @Param("companyid") int companyid);

    @Query("select de from DepartmentEmEntity de where de.department.id =:departmentId and de.user.id=:username")
    DepartmentEmEntity getDepartmentEM(@Param("departmentId") Integer departmentId, @Param("username") String userName);

    @Query("select distinct de from DepartmentEmEntity de where de.user.type <> 4 " +
            "and de.user.status=1 and de.user.enabled = true and de.department.id =:departmentId and de.department.company.id = :companyId")
    List<DepartmentEmEntity> findUserByDepartmentId(@Param("departmentId") Integer departmentId, @Param("companyId") Integer companyId);
    @Query("select distinct department.id from DepartmentManagerEntity where username =:username and company.id =:companyid and ((current_date between startTime and endTime) or endTime is null) and type = 1")
    List<Integer> getDeparmentBeingManage(@Param("username") String username, @Param("companyid") Integer companyId);
    @Query("from DepartmentEmEntity where user.id =:username and department.company.id =:companyid and user.enabled = true and user.status = 1")
    DepartmentEmEntity getDepartmentByUsernameAndCompany(@Param("username") String username, @Param("companyid") int companyId);
    
    @Query("select distinct id from DepartmentEntity where company.id =:companyid and isshop = 1 and isdelete = 0")
    List<Integer> getAllShopId(@Param("companyid") Integer companyId);
}
