package com.oss.platform.core.repository;

import com.oss.platform.core.data.LeftMenuEntity;
import com.oss.platform.core.data.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by Vu on 02/09/2017.
 */
public interface LeftMenuRepository extends JpaRepository<UserEntity,Long> {
    @Query("SELECT lf FROM LeftMenuEntity lf, LeftMenuJobTitleEntity lj, JobTitleEntity jt, DepartmentEmEntity de "
            + " where lj.leftmenu.id = lf.id and lj.jobtitle.id = jt.id and de.jobTitle.id = jt.id "
		    + " and de.user.username = :username and jt.company.id = :companyid and type = :type and lf.parentId is null order by parentId, orderMenu")
	List<LeftMenuEntity> getParentLeftMenu(@Param("username") String username, @Param("companyid") Integer companyid, @Param("type") Integer type);

    @Query("SELECT lf FROM LeftMenuEntity lf, LeftMenuJobTitleEntity lj, JobTitleEntity jt, DepartmentEmEntity de "
            + " where lj.leftmenu.id = lf.id and lj.jobtitle.id = jt.id and de.jobTitle.id = jt.id "
            + " and de.user.username = :username and jt.company.id = :companyid and type = :type and lf.parentId is not null order by parentId, orderMenu")
    List<LeftMenuEntity> getChildLeftMenu(@Param("username") String username, @Param("companyid") Integer companyid, @Param("type") Integer type);
}
