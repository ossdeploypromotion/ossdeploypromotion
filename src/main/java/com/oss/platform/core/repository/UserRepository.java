package com.oss.platform.core.repository;

import com.oss.platform.core.data.UserEntity;

import java.util.List;

import com.oss.platform.core.dto.SearchEmployeeDTO;
import com.oss.platform.core.dto.UserDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends JpaRepository<UserEntity, String> {
    List<UserEntity> findByUsername(String username);

    @Query("select distinct u from UserEntity u,DepartmentEmEntity dem where u.id = dem.user.id and function(FN_CONVERT_TO_VN,lower(fullName))  like '%'||:place||'%' and dem.department.company.id = :companyid")
    List<UserEntity> findByUsernameIgnoreCaseContaining(@Param("place") String place,
                                                        @Param("companyid") Integer companyId, Pageable pageRequest);

    @Query("from UserEntity where username =:userId")
    UserEntity findByUsernameAndCompanyId(@Param("userId") String userId);

    @Query("select count(u) from UserEntity u, DepartmentEmEntity dem where u.username = dem.user.username and u.type <> 4 and u.status = 1 and u.enabled = 1 and dem.department.company.id = :companyid")
    Long countNumOfUser(@Param("companyid") Integer companyid);

    @Query("select count(distinct de.department.company) from DepartmentEmEntity de where de.user.type <> 4 " +
            "and de.user.status=1 and de.user.enabled = true and de.user.username =:username")
    Integer countUserCompany(@Param("username") String username);

    @Query("select de.user from DepartmentEmEntity de where de.department.id = :departmentid and de.user.type <> 4 " +
            "and de.user.status=1 and de.user.enabled = true")
    List<UserEntity> findUserByDepartmentId(@Param("departmentid") Integer departmentid);

    @Query("select new com.oss.platform.core.dto.SearchEmployeeDTO(de.user.username,de.user.fullname,de.jobTitle.name,de.user.photo,de.department.company.id) from DepartmentEmEntity de where de.user.type <> 4 " +
            " and de.department.company.id = :companyid and de.user.status=1 and de.user.enabled = true and lower(FN_CONVERT_TO_VN(de.user.fullname)) like %:keysearch%")
    List<SearchEmployeeDTO> searchUserForHeaderBar(@Param("keysearch") String key, @Param("companyid") Integer companyid, Pageable pageRequest);
    @Query("select new com.oss.platform.core.dto.UserDTO(username,photo) from UserEntity where  username in (?1)")
    List<UserDTO> findListAvatar(List<String> userId);
}
