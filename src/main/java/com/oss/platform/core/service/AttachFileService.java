package com.oss.platform.core.service;

import com.oss.platform.core.dto.AttachFileDTO;
import com.oss.platform.core.exception.NotFoundException;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * Created by W540 on 4/20/2017.
 */
public interface AttachFileService {
    void removeAttachFile(String attachFileId) throws NotFoundException;
    List<AttachFileDTO> uploadFiles(String path, MultipartFile[] files);
}
