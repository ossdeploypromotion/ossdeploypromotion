package com.oss.platform.core.service;

import com.oss.platform.core.dto.SearchEmployeeDTO;

import java.util.List;

/**
 * Created by W540 on 4/14/2017.
 */
public interface CommonService {
    List<SearchEmployeeDTO> searchEmployeeForHeaderBar(String username, Integer companyId, String key);
}
