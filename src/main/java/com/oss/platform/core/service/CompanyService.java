package com.oss.platform.core.service;

import com.oss.platform.core.data.CompanyEntity;
import com.oss.platform.core.dto.CompanyDTO;
import com.oss.platform.core.exception.NotFoundException;

/**
 * @author Ballard
 */
public interface CompanyService {
    CompanyDTO getDetail(Integer id) throws NotFoundException;
    CompanyEntity getDetailFull(Integer id) throws NotFoundException;
}
