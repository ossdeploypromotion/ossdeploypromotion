package com.oss.platform.core.service;

import com.oss.platform.core.data.DepartmentEntity;
import com.oss.platform.core.dto.DepartmentDTO;

import java.util.List;

/**
 * Created by W540 on 1/24/2017.
 */
public interface DepartmentService {
	List<DepartmentDTO> getAllByCompany(Integer companyId);
	
    List<DepartmentDTO> findUserDepartment(String username);
    Boolean validateDepartment(DepartmentEntity fromDepartment, DepartmentEntity toDepartment);

    DepartmentDTO findUserDepartment(String username, Integer companyId);

    List<Integer> getDeparmentBeingManage(String username, Integer companyId);

    List<Integer> getListParentDepartment(Integer deparmentId);
    
    List<Integer> getListChildDepartment(Integer deparmentId, Integer companyId);
}
