package com.oss.platform.core.service;

import com.oss.platform.core.dto.AttachFileDTO;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * Created by W540 on 4/18/2017.
 */

public interface FileService {
    List<AttachFileDTO> saveMuiltipartFile(String path, MultipartFile[] files) throws IOException;

    void removeFile(String path);
}
