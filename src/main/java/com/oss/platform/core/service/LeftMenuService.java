package com.oss.platform.core.service;

import com.oss.platform.core.dto.ChildMenuDTO;
import com.oss.platform.core.dto.ParentMenuDTO;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by W540 on 1/24/2017.
 */
public interface LeftMenuService {

    static final Pattern patternWeb = Pattern.compile("^(http://|https://)?(www.)?([a-zA-Z0-9]+).[a-zA-Z0-9]*.[a-z]{3}.?([a-z]+)?.*$");
    static final Integer HTTPPORT = 80;
    static final Integer HTTPSPORT = 443;
    static final String HTTP_STRING = "http";
    static final String HTTPS_STRING = "https";

    List<ParentMenuDTO> getLeftMenu(String username, Integer companyid, Integer type, HttpServletRequest request);
    List<ChildMenuDTO> getTopLeftMenu(String username, Integer companyid, Integer type, HttpServletRequest request);
}
