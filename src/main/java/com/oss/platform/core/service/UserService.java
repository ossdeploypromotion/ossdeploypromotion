package com.oss.platform.core.service;

import com.oss.platform.core.dto.CompanyDTO;
import com.oss.platform.core.dto.UserDTO;
import com.oss.platform.core.exception.NotFoundException;
import com.oss.platform.security.OssUserPrincipal;

import java.util.List;
import java.util.Map;

/**
 * Created by W540 on 1/19/2017.
 */
public interface UserService {
    UserDTO getUserByUserName(String username) throws NotFoundException;
    List<UserDTO> searchUsersByUserName(String keyword, String currentUser, Integer companyId);
    List<UserDTO> loadListUserByUsername(String name);

    List<CompanyDTO> getListCompany(String username);

    UserDTO getUserByPinciple(OssUserPrincipal details);

    boolean isUserOfCompany(String username, Integer companyId);

    UserDTO getUserByUserNameAndDomain(String username, String serverName) throws NotFoundException;

    Integer countUserCompany(String username);

    Map<String,String> getAvartarMap(List<String> userId);
}
