package com.oss.platform.core.service.impl;


import com.oss.platform.core.data.AttachFile;
import com.oss.platform.core.dto.AttachFileDTO;
import com.oss.platform.core.exception.NotFoundException;
import com.oss.platform.core.repository.AttachFileRepository;
import com.oss.platform.core.service.AttachFileService;

import com.oss.platform.core.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;


/**
 * Created by W540 on 4/20/2017.
 */
@Service
public class AttachFileServiceImpl implements AttachFileService{

    AttachFileRepository attachFileRepository;
    FileService fileService;

    @Autowired
    public AttachFileServiceImpl(AttachFileRepository attachFileRepository, FileService fileService) {
        this.attachFileRepository = attachFileRepository;
        this.fileService = fileService;
    }

    @Override
    public void removeAttachFile(String attachFileId) throws NotFoundException {
        AttachFile attachFileEntity = attachFileRepository.findOne(attachFileId);
        if(attachFileEntity == null){
            throw new NotFoundException("Not Found");
        }
        fileService.removeFile(attachFileEntity.getPath());
        attachFileRepository.delete(attachFileEntity);
    }
    
    @Override
    public List<AttachFileDTO> uploadFiles(String path, MultipartFile[] files) {
    	try {
			List<AttachFileDTO> result= fileService.saveMuiltipartFile(path, files);
			return result;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
    }
}
