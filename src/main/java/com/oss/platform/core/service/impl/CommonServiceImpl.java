package com.oss.platform.core.service.impl;

import com.oss.platform.core.dto.SearchEmployeeDTO;
import com.oss.platform.core.repository.UserRepository;
import com.oss.platform.core.service.CommonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by W540 on 4/14/2017.
 */
@Service
public class CommonServiceImpl implements CommonService {
    UserRepository userRepository;

    @Autowired
    public CommonServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }
    @Override
    public List<SearchEmployeeDTO> searchEmployeeForHeaderBar(String username, Integer companyId, String key) {
        List<SearchEmployeeDTO> searchUserResult = userRepository.searchUserForHeaderBar(key.toLowerCase(), companyId, new PageRequest(0, 20));
        return searchUserResult;
    }
}

