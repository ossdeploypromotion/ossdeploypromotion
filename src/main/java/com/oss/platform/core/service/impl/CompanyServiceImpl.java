package com.oss.platform.core.service.impl;

import com.oss.platform.core.data.CompanyEntity;
import com.oss.platform.core.data.DepartmentEntity;
import com.oss.platform.core.dto.CompanyDTO;
import com.oss.platform.core.exception.NotFoundException;
import com.oss.platform.core.mapper.CompanyMapper;
import com.oss.platform.core.repository.CompanyRepository;
import com.oss.platform.core.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Ballard
 */
@Service
@Transactional
public class CompanyServiceImpl implements CompanyService {
    CompanyRepository companyRepository;

    @Autowired
    public CompanyServiceImpl(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    @Override
    public CompanyDTO getDetail(Integer id) throws NotFoundException {
        CompanyEntity company = companyRepository.findOne(id);
        if(company == null){
            throw new NotFoundException("company Not Found");
        }
        List<DepartmentEntity> depts = company.getListChild();
        if(depts != null){
        	List<DepartmentEntity> filterDepts = depts.stream().filter(d -> (d.getParent() == null && d.getIsshop() != 1)).collect(Collectors.toList());
            Collections.sort(filterDepts, new Comparator<DepartmentEntity>() {
                @Override
                public int compare(DepartmentEntity o1, DepartmentEntity o2) {
                    return o1.getName().compareTo(o2.getName());
                }
            });
            company.setListChild(filterDepts);
        }

        return CompanyMapper.mapFull1LevelDep(company);
    }
    
    @Override
    public CompanyEntity getDetailFull(Integer id) throws NotFoundException {
        CompanyEntity company = companyRepository.findOne(id);
        if(company == null){
            throw new NotFoundException("company Not Found");
        }
       
        return company;
    }
}
