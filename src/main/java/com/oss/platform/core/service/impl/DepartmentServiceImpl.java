package com.oss.platform.core.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.oss.platform.core.data.DepartmentEntity;
import com.oss.platform.core.dto.DepartmentDTO;
import com.oss.platform.core.mapper.DepartmentMapper;
import com.oss.platform.core.repository.DepartmentRepository;
import com.oss.platform.core.repository.UserRepository;
import com.oss.platform.core.service.DepartmentService;

/**
 * Created by W540 on 1/24/2017.
 */
@Service
@Transactional
public class DepartmentServiceImpl implements DepartmentService {
	public static final int DEPARTMENT_ALL_SHOP = -1;
	
	DepartmentRepository departmentRepository;
	UserRepository userRepository;

	@Autowired
	public DepartmentServiceImpl(DepartmentRepository departmentRepository, UserRepository userRepository) {
		this.departmentRepository = departmentRepository;
		this.userRepository = userRepository;
	}

	@Override
	public List<DepartmentDTO> findUserDepartment(String username) {
		List<DepartmentEntity> listUserDepartMent = departmentRepository.findUserDepartMents(username);
		return DepartmentMapper.map(listUserDepartMent);
	}

	@Override
	public Boolean validateDepartment(DepartmentEntity fromDepartment, DepartmentEntity toDepartment) {
		Integer fromId = fromDepartment.getId();
		Integer toId = toDepartment.getId();

		Integer fromParentId = fromDepartment.getParent().getId();
		Integer toParentId = toDepartment.getParent().getId();

		if (fromParentId == toParentId || fromParentId == toId) {
			return true;
		} else {
			DepartmentEntity fromParent = departmentRepository.findOne(fromParentId);
			validateDepartment(fromParent, toDepartment);
		}
		return false;
	}

	@Override
	public DepartmentDTO findUserDepartment(String username, Integer companyId) {
		return DepartmentMapper.map(departmentRepository.findDepartmentByUserAndcompany(username, companyId));
	}

	@Override
	public List<Integer> getDeparmentBeingManage(String username, Integer companyId) {
		List<Integer> listId = departmentRepository.getDeparmentBeingManage(username, companyId);
		return listId;
	}

	@Transactional(readOnly = true)
	@Override
	public List<Integer> getListParentDepartment(Integer deparmentId) {
		List<Integer> idList = new ArrayList<>();
		DepartmentEntity department = departmentRepository.findOne(deparmentId);
		idList.add(department.getId());
		DepartmentEntity loopTemp = department.getParent();
		while (loopTemp != null) {
			idList.add(loopTemp.getId());
			loopTemp = loopTemp.getParent();
		}

		return idList;
	}

	@Override
	public List<DepartmentDTO> getAllByCompany(Integer companyId) {
		List<DepartmentEntity> result = departmentRepository.findAllByCompany(companyId);
		return DepartmentMapper.mapBasicHaveParent(result);
	}

	@Override
	public List<Integer> getListChildDepartment(Integer deparmentId, Integer companyId) {
		List<Integer> result = new ArrayList<>();
		if(deparmentId == DEPARTMENT_ALL_SHOP){
			result = departmentRepository.getAllShopId(companyId);
		}
		DepartmentEntity entity = departmentRepository.findOne(deparmentId);
		if(entity == null){
			return result;
		}
		result.add(deparmentId);
		DepartmentEntity parent = entity.getParent();
		if(parent != null){
			return result;
		}
		
		List<DepartmentEntity> child = entity.getListChild();
		for (DepartmentEntity item : child) {
			result.add(item.getId());
		}
		
		return result;
	}
}
