package com.oss.platform.core.service.impl;

import com.oss.platform.common.Constant;
import com.oss.platform.core.dto.AttachFileDTO;
import com.oss.platform.core.service.FileService;
import net.coobird.thumbnailator.Thumbnails;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.activation.MimetypesFileTypeMap;
import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by W540 on 4/18/2017.
 */
@Service
public class FileServiceImpl implements FileService {
    final static Logger logger = Logger.getLogger(FileServiceImpl.class);
    String uploadDir;
    static final String annPrefix = Constant.CHECKLIST_CONTENT_PREFIX;

    @Autowired
    FileServiceImpl(Environment env) {
        uploadDir = env.getRequiredProperty("uploadfile.filedir");
    }

    public String saveMultipathFileAutoAppendTimestamp(String path, MultipartFile file, String extension) throws IOException {
        String finalName = annPrefix + File.separator + path + File.separator + Long.toString(System.currentTimeMillis()) + "-" + file.getOriginalFilename();
        File saveFile = new File(uploadDir + File.separator + finalName);

        if (!saveFile.getParentFile().exists()) {
            saveFile.mkdirs();
        }
        file.transferTo(saveFile);
        return finalName;
    }

    @Override
    public List<AttachFileDTO> saveMuiltipartFile(String path, MultipartFile[] files) throws IOException {
        List<AttachFileDTO> listFinalPath = new ArrayList<>();
        for (MultipartFile file : files) {
            AttachFileDTO fileInfo = new AttachFileDTO();
            String[] sl = file.getOriginalFilename().split("\\.");
            String extension = sl.length > 1 ? sl[sl.length - 1] : "";
            fileInfo.setFileName(sl[0].equals("") ? "noname" : sl[0]);
            fileInfo.setExtension(extension);
            fileInfo.setSize(file.getSize());
            fileInfo.setPath(saveMultipathFileAutoAppendTimestamp(path, file, extension));

            //Fix image rotation
            File saveFile = new File(uploadDir + File.separator + fileInfo.getPath());
            String mimetype= new MimetypesFileTypeMap().getContentType(saveFile);
            String type = mimetype.split("/")[0];
            if(type.equals("image")) {
                Thumbnails.of(new FileInputStream(saveFile)).scale(1).toFile(saveFile);
                // clone new resize image for show on mobile
                BufferedImage minpng = null;
                BufferedImage mediumpng = null;
                File newFileMin = null;
                File newFileMedium = null;
                String filename = FilenameUtils.removeExtension(fileInfo.getPath());
                Image img = ImageIO.read(saveFile);
                BufferedImage bimg = ImageIO.read(saveFile);
                int width = bimg.getWidth();
                //int height = bimg.getHeight();

                if (width > 640) {
                    minpng = resizeImage(img, 200, 160);
                    mediumpng = resizeImage(img, 640, 640);
                    newFileMin = new File(uploadDir + File.separator + filename+ "_min.png");
                    newFileMedium =  new File(uploadDir + File.separator + filename + "_medium.png");
                    ImageIO.write(minpng, extension, newFileMin);
                    ImageIO.write(mediumpng, extension, newFileMedium);

                    fileInfo.setPath_medium( filename+ "_medium.png");
                    fileInfo.setPath_min(filename+ "_min.png");
                } else {
                    if (width > 200) {
                        minpng = resizeImage(img, 200, 160);
                        newFileMin =  new File(uploadDir + File.separator + filename+ "_min.png");
                        ImageIO.write(minpng, extension, newFileMin);
                    }
                    fileInfo.setPath_min(filename+ "_min.png");
                }

            }

            listFinalPath.add(fileInfo);
        }
        return listFinalPath;
    }

    @Override
    public void removeFile(String path) {
        String finalName = uploadDir + File.separator  + path;
        File deleting = new File(finalName);
        if (deleting.delete()) {
            logger.debug(String.format("Deleted file with path %s", finalName));
        } else {
            logger.error(String.format("Delete operation is failed with path %s", finalName));
        }
    }

    /**
     * This function resize the image file and returns the BufferedImage object that can be saved to file system.
     */
    public static BufferedImage resizeImage(final Image image, int width, int height) {
        final BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        final Graphics2D graphics2D = bufferedImage.createGraphics();
        graphics2D.setComposite(AlphaComposite.Src);
        //below three lines are for RenderingHints for better image quality at cost of higher processing time
        graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION,RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        graphics2D.setRenderingHint(RenderingHints.KEY_RENDERING,RenderingHints.VALUE_RENDER_QUALITY);
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
        graphics2D.drawImage(image, 0, 0, width, height, null);
        graphics2D.dispose();
        return bufferedImage;
    }
}
