package com.oss.platform.core.service.impl;

import com.oss.platform.core.data.LeftMenuEntity;
import com.oss.platform.core.dto.ChildMenuDTO;
import com.oss.platform.core.dto.ParentMenuDTO;
import com.oss.platform.core.repository.LeftMenuRepository;
import com.oss.platform.core.service.LeftMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

/**
 * Created by Vu on 02/13/2017.
 */
@Service
@Transactional
public class LeftMenuServiceImpl implements LeftMenuService {
    LeftMenuRepository leftMenuRepository;
    private MessageSource messageSource;
    private static Matcher matcher;

    @Autowired
    public LeftMenuServiceImpl(LeftMenuRepository leftMenuRepository, MessageSource messageSource) {
        this.leftMenuRepository = leftMenuRepository;
        this.messageSource = messageSource;
    }

    @Override
    public List<ParentMenuDTO> getLeftMenu(String username, Integer companyid, Integer type, HttpServletRequest request) {
        // get all parent left menu
        List<LeftMenuEntity> listParent = leftMenuRepository.getParentLeftMenu(username, companyid, type);
        // get all child left menu
        List<LeftMenuEntity> listChild = leftMenuRepository.getChildLeftMenu(username, companyid, type);

        //Start get domain of current web client
        String serverName = request.getServerName();
        Integer serverPort = request.getServerPort();
        String http = HTTP_STRING;
        if (request.isSecure()) { //https
            http = HTTPS_STRING;
        }
        String domain = http + "://" + serverName;
        if (serverPort != null && serverPort != HTTPPORT && serverPort != HTTPSPORT) domain += ":" + serverPort;
        //End get domain of current web client

        List<ParentMenuDTO> result = new ArrayList<ParentMenuDTO>();
        for (LeftMenuEntity p : listParent) {
            ParentMenuDTO pat = new ParentMenuDTO();
            pat.setTitle(getMessage(p.getName()));
            pat.setFaIconClass(p.getIcon());

            List<ChildMenuDTO> cList = new ArrayList<ChildMenuDTO>();
            for (LeftMenuEntity c : listChild) {
                if (p.getId().equals(c.getParentId())) {
                    ChildMenuDTO child = new ChildMenuDTO();
                    child.setTitle(getMessage(c.getName()));
                    String link = c.getLink();
                    matcher = patternWeb.matcher(link);
                    boolean isValid = matcher.matches();

                    if (isValid) { // if link is start with http or https, keep its prime
                        child.setUrl(link);
                    } else { //else finalLink = domain + subdomain + link
                        String linkDisplay = domain;
                        String subDomain = c.getSubDomain();
                        if (subDomain != null) linkDisplay += "/" + subDomain;
                        linkDisplay += link;
                        child.setUrl(linkDisplay);
                    }
                    cList.add(child);
                }
            }
            pat.setSubMenu(cList);
            result.add(pat);
        }
        return result;
    }

    @Override
    public List<ChildMenuDTO> getTopLeftMenu(String username, Integer companyid, Integer type, HttpServletRequest request) {
        // get all child left menu
        List<LeftMenuEntity> listTopLeft = leftMenuRepository.getChildLeftMenu(username, companyid, type);

        //Start get domain of current web client
        String serverName = request.getServerName();
        Integer serverPort = request.getServerPort();
        String http = HTTP_STRING;
        if (request.isSecure()) { //https
            http = HTTPS_STRING;
        }
        String domain = http + "://" + serverName;
        if (serverPort != null && serverPort != HTTPPORT && serverPort != HTTPSPORT) domain += ":" + serverPort;
        //End get domain of current web client

        List<ChildMenuDTO> cList = new ArrayList<ChildMenuDTO>();
        for (LeftMenuEntity c : listTopLeft) {
            ChildMenuDTO child = new ChildMenuDTO();
            child.setTitle(getMessage(c.getName()));
            child.setParentid(c.getParentId());
            String link = c.getLink();
            matcher = patternWeb.matcher(link);
            boolean isValid = matcher.matches();

            if (isValid) { // if link is start with http or https, keep its prime
                child.setUrl(link);
            } else { //else finalLink = domain + subdomain + link
                String linkDisplay = domain;
                String subDomain = c.getSubDomain();
                if (subDomain != null) linkDisplay += "/" + subDomain;
                linkDisplay += link;
                child.setUrl(linkDisplay);
            }
            cList.add(child);
        }
        return cList;
    }

    private String getMessage(String code) {
        try {
            return messageSource.getMessage(code,new Object[]{}, LocaleContextHolder.getLocale());
        } catch (NoSuchMessageException e) {
            System.out.println(e.getMessage());
            return code;
        }
    }
}
