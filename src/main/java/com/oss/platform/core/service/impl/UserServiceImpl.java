package com.oss.platform.core.service.impl;

import com.oss.platform.core.data.CompanyEntity;
import com.oss.platform.core.data.DepartmentEmEntity;
import com.oss.platform.core.data.DepartmentEntity;
import com.oss.platform.core.data.UserEntity;
import com.oss.platform.core.dto.CompanyDTO;
import com.oss.platform.core.dto.UserDTO;
import com.oss.platform.core.exception.NotFoundException;
import com.oss.platform.core.mapper.CompanyMapper;
import com.oss.platform.core.mapper.UserMapper;
import com.oss.platform.core.repository.CompanyRepository;
import com.oss.platform.core.repository.DepartmentRepository;
import com.oss.platform.core.repository.UserRepository;
import com.oss.platform.core.service.UserService;
import com.oss.platform.security.OssUserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by W540 on 1/19/2017.
 */
@Service
@Transactional
public class UserServiceImpl implements UserService{
    UserRepository userRepository;
    CompanyRepository companyRepository;
    DepartmentRepository departmentRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, CompanyRepository companyRepository, DepartmentRepository departmentRepository) {
        this.userRepository = userRepository;
        this.companyRepository = companyRepository;
        this.departmentRepository = departmentRepository;
    }




    @Override
    public UserDTO getUserByUserName(String username) throws NotFoundException {
        UserEntity user = userRepository.getOne(username);
        if(user == null){
            throw new NotFoundException("User Not Found");
        }

        return UserMapper.map(user);
    }
    
    @Override
    public List<UserDTO> searchUsersByUserName(String keyword, String currentUserName, Integer companyId){
    	List<UserEntity> users = userRepository.findByUsernameIgnoreCaseContaining(keyword,companyId,new PageRequest(0,10));
    	return UserMapper.map(users);
    }

    @Override
    public List<UserDTO> loadListUserByUsername(String name) {
        List<UserEntity> users = userRepository.findByUsername(name);

        return UserMapper.mapAuthenInfo(users);
    }

    @Override
    public List<CompanyDTO> getListCompany(String username) {
        List<CompanyEntity> companyEntities = companyRepository.findAllCompanyForUser(username);
        return CompanyMapper.map(companyEntities);
    }

    @Override
    public UserDTO getUserByPinciple(OssUserPrincipal details) {
        CompanyEntity companyEntity = companyRepository.getOne(details.getCompanyId());
        //DepartmentEntity departmentEntity = departmentRepository.findDepartmentByUserAndcompany(details.getUsername(),details.getCompanyId());
        UserEntity userEntity = userRepository.findOne(details.getUsername());
        DepartmentEmEntity departmentEmEntity = departmentRepository.getDepartmentByUsernameAndCompany(details.getUsername(),details.getCompanyId());
        UserDTO result = UserMapper.map(userEntity,companyEntity,departmentEmEntity.getDepartment(),departmentEmEntity.getJobTitle());
        return result;
    }

    @Override
    public boolean isUserOfCompany(String username, Integer companyId) {
        Integer countUserCompany =  companyRepository.isUserOfCompany(username,companyId);
        return countUserCompany == 1;
    }

    @Override
    public UserDTO getUserByUserNameAndDomain(String username, String domainName) throws NotFoundException {
        DepartmentEntity userDepartment = departmentRepository.getDepartmentByDomainAndUser(username,domainName);
        if(userDepartment == null){
            throw new NotFoundException("not found user with company domain name");
        }
        UserEntity userEntity = userRepository.getOne(username);
        return UserMapper.mapAuthenInfo(userEntity,userDepartment.getCompany(),userDepartment);
    }

    @Override
    public Integer countUserCompany(String username) {
        return userRepository.countUserCompany(username);
    }

    @Override
    public Map<String, String> getAvartarMap(List<String> userId) {
        List<UserDTO> listRecord= userRepository.findListAvatar(userId);
        return listRecord.stream().collect(Collectors.toMap(UserDTO::getUsername, UserDTO::getPhoto));
    }
}
