package com.oss.platform.core.service.model;

/**
 * Created by W540 on 3/10/2017.
 */
public class UserManagerModel {
    String user;
    String manager;

    public UserManagerModel(String user, String manager) {
        this.user = user;
        this.manager = manager;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getManager() {
        return manager;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }
}
