package com.oss.platform.core.service.model;

/**
 * Created by W540 on 3/10/2017.
 */
public class UserSameLevelModel {
    String user;
    String sameLevelUser;

    public UserSameLevelModel(String user, String sameLevelUser) {
        this.user = user;
        this.sameLevelUser = sameLevelUser;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getSameLevelUser() {
        return sameLevelUser;
    }

    public void setSameLevelUser(String sameLevelUser) {
        this.sameLevelUser = sameLevelUser;
    }
}
