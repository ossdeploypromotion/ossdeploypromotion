package com.oss.platform.modun.data;

import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;
import java.util.Date;

/**
 * Created by haocpq on 6/8/2017.
 */
@Document(collection = "modun")
public class ModunEntity {

    @Id
    private String id;
    private String title;
    private String description;
    private String createduser;
    private Date createddate;
    private Integer companyid;

    public ModunEntity() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreateduser() {
        return createduser;
    }

    public void setCreateduser(String createduser) {
        this.createduser = createduser;
    }

    public Date getCreateddate() { return createddate; }

    public void setCreateddate(Date createddate) {  this.createddate = createddate; }

    public Integer getCompanyid() {
        return companyid;
    }

    public void setCompanyid(Integer companyid) {
        this.companyid = companyid;
    }
}
