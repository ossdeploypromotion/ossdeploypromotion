package com.oss.platform.modun.dto;

/**
 * Created by haocpq on 6/8/2017.
 */
public class ModunDto {

    private String id;
    private String title;
    private String description;

    public ModunDto() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
