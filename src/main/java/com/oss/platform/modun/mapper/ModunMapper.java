package com.oss.platform.modun.mapper;

import com.oss.platform.modun.data.ModunEntity;
import com.oss.platform.modun.dto.ModunDto;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by haocpq on 6/8/2017.
 */
public class ModunMapper {

    public static ModunDto map (ModunEntity entity) {
        ModunDto modunDto = new ModunDto();
        if (entity == null) {
            return null;
        }
        modunDto.setId(entity.getId());
        modunDto.setTitle(entity.getTitle());
        modunDto.setDescription(entity.getDescription());
        return modunDto;
    }

    public static List<ModunDto> map(List<ModunEntity> listEntity){
        return listEntity.stream().map(t->map(t)).collect(Collectors.toList());
    }

}
