package com.oss.platform.modun.repository;

import com.oss.platform.modun.data.ModunEntity;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by laptopone on 6/8/2017.
 */

public interface ModunResponsitory extends MongoRepository<ModunEntity, String> {

    List<ModunEntity> findByCompanyid(Pageable pageable, Integer companyid);

}
