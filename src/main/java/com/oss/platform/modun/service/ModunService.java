package com.oss.platform.modun.service;

import com.oss.platform.modun.dto.ModunDto;
import com.oss.platform.modun.dto.ModunInDto;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Created by haoqpc on 6/8/2017.
 */
public interface ModunService {

    List<ModunDto> findByCompanyid (Pageable pageable, Integer companyid);

    ModunDto save(ModunInDto modun, String username, Integer companyid);

    void delete(String modunid);
}
