package com.oss.platform.modun.service.impl;

import com.oss.platform.modun.data.ModunEntity;
import com.oss.platform.modun.dto.ModunDto;
import com.oss.platform.modun.dto.ModunInDto;
import com.oss.platform.modun.mapper.ModunMapper;
import com.oss.platform.modun.repository.ModunResponsitory;
import com.oss.platform.modun.service.ModunService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.Date;
import java.util.List;

/**
 * Created by haocpq on 6/8/2017.
 */
@Service
public class ModunServiceImpl implements ModunService {

    @Autowired
    private ModunResponsitory modunResponsitory;

    @Override
    public List<ModunDto> findByCompanyid(Pageable pageable, Integer companyid) {

        List<ModunEntity> modunEntities = modunResponsitory.findByCompanyid(pageable, companyid);

        return ModunMapper.map(modunEntities);
    }

    @Override
    public ModunDto save (ModunInDto modun, String username, Integer companyid) {

        ModunEntity entity = new ModunEntity();
        if(modun.getId()!= null && !"".equals(modun.getId())){
            entity.setId(modun.getId());
        }
        else{
            entity.setCreateduser(username);
            entity.setCreateddate(new Date());
            entity.setCompanyid(companyid);
        }
        entity.setTitle(modun.getTitle());
        entity.setDescription(modun.getDescription());

        ModunEntity newEntity = modunResponsitory.save(entity);

        if(newEntity == null){
            return null;
        }
        return ModunMapper.map(newEntity);
    }

    @Override
    public void delete(String modunid) {
        modunResponsitory.delete(modunid);
    }
}
