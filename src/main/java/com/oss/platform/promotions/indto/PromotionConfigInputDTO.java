package com.oss.platform.promotions.indto;

import java.util.Date;
import java.util.List;

public class PromotionConfigInputDTO {
	private String moduleCode;
	private String moduleName;
	private String description;
	private Date deadlineImpl;
	private String userUpdate;
	private Date lastUpdated;
	private String urlImg;
	private List<String> promotionShopIds;
	public String getModuleCode() {
		return moduleCode;
	}
	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getDeadlineImpl() {
		return deadlineImpl;
	}
	public void setDeadlineImpl(Date deadlineImpl) {
		this.deadlineImpl = deadlineImpl;
	}
	public String getUserUpdate() {
		return userUpdate;
	}
	public void setUserUpdate(String userUpdate) {
		this.userUpdate = userUpdate;
	}
	public Date getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	public String getUrlImg() {
		return urlImg;
	}
	public void setUrlImg(String urlImg) {
		this.urlImg = urlImg;
	}
	public List<String> getPromotionShopIds() {
		return promotionShopIds;
	}
	public void setPromotionShopIds(List<String> promotionShopIds) {
		this.promotionShopIds = promotionShopIds;
	}
	
	
}
