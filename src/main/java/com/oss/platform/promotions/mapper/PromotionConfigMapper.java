package com.oss.platform.promotions.mapper;

import java.util.List;
import java.util.stream.Collectors;

import com.oss.platform.promotions.data.PromotionConfigEntity;
import com.oss.platform.promotions.outdto.PromotionConfigOutputDTO;

public class PromotionConfigMapper {
	public static PromotionConfigOutputDTO map(PromotionConfigEntity entity){
		PromotionConfigOutputDTO outputDTO = new PromotionConfigOutputDTO();
		try {
			outputDTO.setModuleCode(entity.getModuleCode());
			outputDTO.setModuleName(entity.getModuleName());
			outputDTO.setDescription(entity.getDescription());
			outputDTO.setDeadlineImpl(entity.getDeadlineImpl());
			outputDTO.setUserUpdate(entity.getUserUpdate());
			outputDTO.setLastUpdated(entity.getLastUpdated());
			outputDTO.setUrlImg(entity.getUrlImg());
			
		} catch (NullPointerException e) {
			// TODO: handle exception
		}
		return outputDTO;
	}
	public static List<PromotionConfigOutputDTO> map(List<PromotionConfigEntity> entities){
		return entities.stream().map(t->map(t)).collect(Collectors.toList());
	}
}
