package com.oss.platform.promotions.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.oss.platform.promotions.data.PromotionConfigEntity;
import com.oss.platform.promotions.indto.PromotionConfigInputDTO;

public interface PromotionConfigRepository extends MongoRepository<PromotionConfigEntity, String> {
}
