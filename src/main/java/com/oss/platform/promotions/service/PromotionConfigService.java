package com.oss.platform.promotions.service;

import org.springframework.data.jpa.repository.Query;

import com.oss.platform.promotions.indto.PromotionConfigInputDTO;
import com.oss.platform.promotions.outdto.PromotionConfigOutputDTO;

public interface PromotionConfigService {
	PromotionConfigOutputDTO getPromotionConfig(String codePromotion);

	PromotionConfigOutputDTO savePromotionConfig(PromotionConfigInputDTO promotionConfigInputDTO);
}
