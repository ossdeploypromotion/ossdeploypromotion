package com.oss.platform.promotions.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oss.platform.promotions.data.PromotionConfigEntity;
import com.oss.platform.promotions.indto.PromotionConfigInputDTO;
import com.oss.platform.promotions.mapper.PromotionConfigMapper;
import com.oss.platform.promotions.outdto.PromotionConfigOutputDTO;
import com.oss.platform.promotions.repository.PromotionConfigRepository;
import com.oss.platform.promotions.service.PromotionConfigService;

@Service
public class PromotionConfigServiceImpl implements PromotionConfigService {
	@Autowired
	private PromotionConfigRepository promotionConfigRepo;

	@Override
	public PromotionConfigOutputDTO getPromotionConfig(String codePromotion) {
		PromotionConfigEntity promotionImplEntity = promotionConfigRepo.findOne(codePromotion);

		return PromotionConfigMapper.map(promotionImplEntity);
	}

	@Override
	public PromotionConfigOutputDTO savePromotionConfig(PromotionConfigInputDTO promotionConfigInputDTO) {
		PromotionConfigEntity promotionConfigEntity = new PromotionConfigEntity();
		promotionConfigEntity.setModuleCode(promotionConfigInputDTO.getModuleCode());
		promotionConfigEntity.setModuleName(promotionConfigInputDTO.getModuleName());
		promotionConfigEntity.setDeadlineImpl(promotionConfigInputDTO.getDeadlineImpl());
		promotionConfigEntity.setDescription(promotionConfigInputDTO.getDescription());
		promotionConfigEntity.setUserUpdate(promotionConfigInputDTO.getUserUpdate());
		promotionConfigEntity.setLastUpdated(promotionConfigInputDTO.getLastUpdated());
		promotionConfigEntity.setUrlImg(promotionConfigInputDTO.getUrlImg());

		PromotionConfigEntity newEntity = promotionConfigRepo.save(promotionConfigEntity);
		if (newEntity == null) {
			return null;
		}
		return PromotionConfigMapper.map(newEntity);
	}

}
