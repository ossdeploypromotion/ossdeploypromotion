package com.oss.platform.security;

import com.oss.platform.core.data.UserRoleEntity;
import com.oss.platform.core.dto.UserRoleDTO;
import org.springframework.security.core.GrantedAuthority;



/**
 * Created by W540 on 1/19/2017.
 */
public class Authority implements GrantedAuthority {
    String role;

    public Authority(String role) {
        this.role = role;
    }

    @Override
    public String getAuthority() {
        return role;
    }
}
