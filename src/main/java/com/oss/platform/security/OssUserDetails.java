package com.oss.platform.security;

import com.oss.platform.core.dto.UserDTO;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.security.auth.Subject;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * Created by W540 on 1/19/2017.
 */
public class OssUserDetails implements UserDetails,Authentication {
    OssUserPrincipal principal;
    String password;
    boolean authentication = true;


    public OssUserDetails(UserDTO user) {
        principal = new OssUserPrincipal();
        principal.setUsername(user.getUsername());
        principal.setCompanyId(user.getCompany().getId());
        principal.setDepartmentId(user.getDepartment().getId());
        principal.setFullName(user.getFullname());
        if(user.getJobTitle() != null){
            principal.setJobTitle(user.getJobTitle().getName());
        }
        if(user.getUserRole() !=null){
            principal.setRoles(user.getUserRole().stream().map(userRoleDTO -> userRoleDTO.getRole()).collect(Collectors.toList()));
        }
        password = user.getPassword();
    }

    public OssUserDetails(OssUserPrincipal user) {
        this.principal = user;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return principal.getRoles().stream().map(role -> {return new Authority(role);}).collect(Collectors.toList());
    }

    @Override
    public String getCredentials() {
        return password;
    }

    @Override
    public OssUserPrincipal getDetails() {
        return principal;
    }

    @Override
    public String getPrincipal() {
        return principal.getUsername();
    }

    @Override
    public boolean isAuthenticated() {
        return authentication;
    }

    @Override
    public void setAuthenticated(boolean b) throws IllegalArgumentException {
        this.authentication = b;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return principal.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return principal.enable;
    }


    @Override
    public String getName() {
        return principal.getUsername();
    }

    public void requestSelectedCompany() {
        this.principal.setNeedSelectCompany(true);
    }
}
