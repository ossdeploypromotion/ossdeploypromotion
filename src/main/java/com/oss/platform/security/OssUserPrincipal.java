package com.oss.platform.security;

import com.oss.platform.core.dto.UserRoleDTO;

import java.util.List;

/**
 * Created by W540 on 3/1/2017.
 */
public class OssUserPrincipal {
    String username;
    Integer companyId;
    Integer departmentId;
    String fullName;
    String jobTitle;
    List<String> roles;
    String appName;
    Boolean enable = true;
    Boolean needSelectCompany = false;
    private long expires;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public Integer getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public void setExpires(long expires) {
        this.expires = expires;
    }

    public long getExpires() {
        return expires;
    }

    public Boolean getNeedSelectCompany() {
        return needSelectCompany;
    }

    public void setNeedSelectCompany(Boolean needSelectCompany) {
        this.needSelectCompany = needSelectCompany;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }	
}
