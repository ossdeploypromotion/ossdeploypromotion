package com.oss.platform.security;

import java.io.IOException;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oss.platform.core.data.UserEntity;
import com.oss.platform.core.dto.UserDTO;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.fasterxml.jackson.databind.ObjectMapper;

public class StatelessLoginFilter extends AbstractAuthenticationProcessingFilter {

	private final TokenAuthenticationService tokenAuthenticationService;
	private final UserDetailsService userDetailsService;

	public StatelessLoginFilter(String urlMapping, TokenAuthenticationService tokenAuthenticationService,
			UserDetailsService userDetailsService, AuthenticationManager authManager) {
		super(new AntPathRequestMatcher(urlMapping));
		this.userDetailsService = userDetailsService;
		this.tokenAuthenticationService = tokenAuthenticationService;
		setAuthenticationManager(authManager);
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException, ServletException, IOException {
		final UserEntity user;
		try {
			user = new ObjectMapper().readValue(request.getInputStream(), UserEntity.class);
		} catch (IOException e) {
			try {
				response.getWriter().print("OK");
				response.getWriter().flush();
				return null;
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			response.getWriter().write("log in failed");
			return null;
		}

		final UsernamePasswordAuthenticationToken loginToken = new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword());
		return getAuthenticationManager().authenticate(loginToken);
	}

	@Override
	public void successfulAuthentication(HttpServletRequest request, HttpServletResponse response,
			FilterChain chain, Authentication authentication) throws IOException, ServletException {

		// Lookup the complete UserEntity object from the database and create an Authentication for it
		final List<UserDTO> userDTOS = userDetailsService.loadListUserByUsername(authentication.getName());
		final OssUserDetails userDetails = new OssUserDetails(userDTOS.get(0));
			// Add the custom token as HTTP header to the response
		tokenAuthenticationService.addAuthentication(response, userDetails);
		// Add the authentication to the Security context
		SecurityContextHolder.getContext().setAuthentication(userDetails);


	}
}