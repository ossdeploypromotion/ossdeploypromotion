package com.oss.platform.security;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

@Service
public class 	TokenAuthenticationService {
	Logger logger = Logger.getLogger(TokenAuthenticationService.class);
	private static final String AUTH_HEADER_NAME = "X-AUTH-TOKEN";
	private static final String AUTH_HEADER_SELECT_COMPANY = "X-AUTH-SELECT-COMPANY";
	private static final long TEN_DAYS = 1000 * 60 * 60 * 24 * 10;

	private final TokenHandler tokenHandler;
	String ssoAppName;


	@Autowired
	public TokenAuthenticationService(@Value("${token.secret}") String secret, @Value("${sso.appname}") String appName) {
//		tokenHandler = new TokenHandler(DatatypeConverter.parseBase64Binary(secret));
		tokenHandler = new TokenHandler(secret,appName);
		this.ssoAppName = appName;
	}

	public void addAuthentication(HttpServletResponse response, OssUserDetails authentication) {
		final OssUserPrincipal user = authentication.getDetails();
		response.addHeader(AUTH_HEADER_NAME, getToken(user));
	}

	public Authentication getAuthentication(HttpServletRequest request) {

			final String token = request.getHeader(AUTH_HEADER_NAME);
		if (token != null) {
			final OssUserPrincipal user;
			try {
				user = tokenHandler.parseUserFromToken(token);
			} catch (IOException e) {
				logger.debug("parse user fail for token: "+token);
				return null;
			}
			if (user != null) {
				if(user.getAppName() == null || !user.getAppName().equals(this.ssoAppName)){
					return null;
				}
				return new OssUserDetails(user);
			}
		}
		return null;
	}
	public void redirectWithToken(HttpServletResponse response,OssUserPrincipal ossUserPrincipal, String url, String serverName) {
		ossUserPrincipal.setExpires(System.currentTimeMillis() + TEN_DAYS);
		String token = getToken(ossUserPrincipal);
		String full = null;
		try {
			full = String.format("http://%s%s?token=%s",serverName,url, URLEncoder.encode(token, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		response.setStatus(response.SC_MOVED_TEMPORARILY);
		response.setHeader("Location", full);
	}

	public String getToken(OssUserPrincipal userPrincipal) {
		userPrincipal.setExpires(System.currentTimeMillis() + TEN_DAYS);
		userPrincipal.setAppName(this.ssoAppName);
		String token = tokenHandler.createTokenForUser(userPrincipal);
		return token;
	}

}
