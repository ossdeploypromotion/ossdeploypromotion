package com.oss.platform.security;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.*;
import io.jsonwebtoken.impl.DefaultClaims;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Mac;
import javax.xml.bind.DatatypeConverter;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Date;

public final class TokenHandler {
	Logger logger = LoggerFactory.getLogger(TokenHandler.class);

	private static final String HMAC_ALGO = "HmacSHA256";
	private static final String SEPARATOR = ".";
	private static final String SEPARATOR_SPLITTER = "\\.";

	private Mac hmac = null;
	private String secretKey;
	private String ssoAppName;

//	public TokenHandler(byte[] secretKey) {
//		try {
//			hmac = Mac.getInstance(HMAC_ALGO);
//			hmac.init(new SecretKeySpec(secretKey, HMAC_ALGO));
//		} catch (NoSuchAlgorithmException | InvalidKeyException e) {
//			throw new IllegalStateException("failed to initialize HMAC: " + e.getMessage(), e);
//		}
//	}
	public TokenHandler(String secretKey, String ssoAppName) {
		this.secretKey = secretKey;
		this.ssoAppName = ssoAppName;
	}

	public OssUserPrincipal parseUserFromToken(String token) throws IOException {
//		final String[] parts = token.split(SEPARATOR_SPLITTER);
//		if (parts.length == 2 && parts[0].length() > 0 && parts[1].length() > 0) {
//			try {
//				final byte[] userBytes = fromBase64(parts[0]);
//				final byte[] hash = fromBase64(parts[1]);
//
//				boolean validHash = Arrays.equals(createHmac(userBytes), hash);
//				if (validHash) {
//					final OssUserPrincipal user = fromJSON(userBytes);
//					if (new Date().getTime() < user.getExpires()) {
//						return user;
//					}
//				}
//			} catch (IllegalArgumentException e) {
//				//log tempering attempt here
//			}
//		}
		try {
			Jws<Claims> claims = Jwts.parser().requireSubject("seedcom-token")
					.setSigningKey(secretKey)
					.parseClaimsJws(token);
			OssUserPrincipal principal = new OssUserPrincipal();
			principal.setUsername(claims.getBody().get("username",String.class));
			principal.setCompanyId(claims.getBody().get("companyId",Integer.class));
			principal.setDepartmentId(claims.getBody().get("departmentId",Integer.class));
			principal.setAppName(claims.getBody().get("appName",String.class));
			return principal;
		} catch (MissingClaimException e) {
			// we get here if the required claim is not present
			logger.debug("missing claim");
		} catch (IncorrectClaimException e) {
			// we get here if the required claim has the wrong value
			logger.debug("incorrect claim");
		} catch (Exception e){
			logger.debug("could not parse token");
		}
		return null;
	}

	public String createTokenForUser(OssUserPrincipal user) {
//		byte[] userBytes = toJSON(user);
//		byte[] hash = createHmac(userBytes);
//		final StringBuilder sb = new StringBuilder(170);
//		sb.append(toBase64(userBytes));
//		sb.append(SEPARATOR);
//		sb.append(toBase64(hash));
		Claims claims = new DefaultClaims();
		claims.setSubject("seedcom-token");
		claims.setIssuedAt(new Date());
		claims.setId(user.getUsername());
		claims.put("companyId",user.getCompanyId());
		claims.put("departmentId",user.getDepartmentId());
		claims.put("username",user.getUsername());
		claims.put("appName",ssoAppName);
		return Jwts.builder().setIssuedAt(new Date()).setClaims(claims).signWith(SignatureAlgorithm.HS512,secretKey).compact();
	}

	private OssUserPrincipal fromJSON(final byte[] userBytes) throws IOException {

			return new ObjectMapper().readValue(new ByteArrayInputStream(userBytes), OssUserPrincipal.class);

	}

	private byte[] toJSON(OssUserPrincipal user) {
		try {
			return new ObjectMapper().writeValueAsBytes(user);
		} catch (JsonProcessingException e) {
			throw new IllegalStateException(e);
		}
	}

	private String toBase64(byte[] content) {
		return DatatypeConverter.printBase64Binary(content);
	}

	private byte[] fromBase64(String content) {
		return DatatypeConverter.parseBase64Binary(content);
	}

	// synchronized to guard internal hmac object
	private synchronized byte[] createHmac(byte[] content) {
		return hmac.doFinal(content);
	}
/*
	public static void main(String[] args) {
		Date start = new Date();
		byte[] secret = new byte[70];
		new java.security.SecureRandom().nextBytes(secret);

		TokenHandler tokenHandler = new TokenHandler(secret);
		for (int i = 0; i < 1000; i++) {
			final UserEntity user = new UserEntity(java.util.UUID.randomUUID().toString().substring(0, 8), new Date(
					new Date().getTime() + 10000));
			user.grantRole(UserRoleEntity.ADMIN);
			final String token = tokenHandler.createTokenForUser(user);
			final UserEntity parsedUser = tokenHandler.parseUserFromToken(token);
			if (parsedUser == null || parsedUser.getUsername() == null) {
				System.out.println("error");
			}
		}
		System.out.println(System.currentTimeMillis() - start.getTime());
	}
*/
}
