package com.oss.platform.security;//package com.oss.platform.security;
//
//import java.util.Collection;
//import java.util.stream.Collectors;
//
//import com.oss.platform.core.data.UserEntity;
//import com.oss.platform.core.dto.UserDTO;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.GrantedAuthority;
//
//class UserAuthentication implements Authentication {
//
//	private final UserDTO user;
//	private boolean authenticated = true;
//
//	public UserAuthentication(UserDTO user) {
//		this.user = user;
//	}
//
//	@Override
//	public String getName() {
//		return user.getUsername();
//	}
//
//	@Override
//	public Collection<? extends GrantedAuthority> getAuthorities() {
//		return user.getUserRole().stream().map(role -> {return new Authority(role);}).collect(Collectors.toList());
//	}
//
//	@Override
//	public Object getCredentials() {
//		return user.getPassword();
//	}
//
//	@Override
//	public UserDTO getDetails() {
//		return user;
//	}
//
//	@Override
//	public Object getPrincipal() {
//		return user.getUsername();
//	}
//
//	@Override
//	public boolean isAuthenticated() {
//		return authenticated;
//	}
//
//	@Override
//	public void setAuthenticated(boolean authenticated) {
//		this.authenticated = authenticated;
//	}
//}
