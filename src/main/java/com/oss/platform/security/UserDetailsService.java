package com.oss.platform.security;

import com.oss.platform.core.data.UserEntity;
import com.oss.platform.core.dto.UserDTO;
import com.oss.platform.core.exception.NotFoundException;
import com.oss.platform.core.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AccountStatusUserDetailsChecker;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

	@Autowired
	private UserService userService;

	private final AccountStatusUserDetailsChecker detailsChecker = new AccountStatusUserDetailsChecker();

	@Override
	public final OssUserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		final UserDTO user;
		try {
			user = userService.getUserByUserName(username);
		} catch (NotFoundException e) {
			throw new UsernameNotFoundException("user not found");
		}
		OssUserDetails userDetails = new OssUserDetails(user);
		if (user == null) {
			throw new UsernameNotFoundException("user not found");
		}
		detailsChecker.check( userDetails);
		return userDetails;
	}

	public List<UserDTO> loadListUserByUsername(String name) {
		return userService.loadListUserByUsername(name);
	}
}
