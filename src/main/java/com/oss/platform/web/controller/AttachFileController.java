package com.oss.platform.web.controller;


import com.oss.platform.common.Constant;
import com.oss.platform.common.ResultMessage;
import com.oss.platform.core.dto.AttachFileDTO;
import com.oss.platform.core.exception.NotFoundException;
import com.oss.platform.core.service.AttachFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * Created by W540 on 4/20/2017.
 */
@RestController
public class AttachFileController {
	@Autowired
    AttachFileService attachFileService;

    @Autowired
    public AttachFileController(AttachFileService attachFileService) {
        this.attachFileService = attachFileService;
    }
    @RequestMapping(value = "/api/attachfile/delete", method = RequestMethod.DELETE)
    ResponseEntity deleteAttachFile (@RequestParam String attachFileId){
        try {
            attachFileService.removeAttachFile(attachFileId);
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
        return ResponseEntity.ok("Success");
    }
    
    @RequestMapping(value = "/api/attachfile/upload", method = RequestMethod.POST)
    ResponseEntity uploadAttachFile (@RequestParam(value = "file", required = false) MultipartFile[] file){
    	List<AttachFileDTO> result= attachFileService.uploadFiles(Constant.CHECKLIST_FILE_PATH, file);

    	ResultMessage resultMessage = new ResultMessage();
        resultMessage.setStatus(Constant.SUCCESS);
        resultMessage.setRecords(result);
        return ResponseEntity.ok(resultMessage);
    }
}
