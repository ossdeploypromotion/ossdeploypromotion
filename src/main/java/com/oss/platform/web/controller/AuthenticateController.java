package com.oss.platform.web.controller;

import com.oss.platform.core.dto.DepartmentDTO;
import com.oss.platform.core.dto.UserDTO;
import com.oss.platform.core.exception.NotFoundException;
import com.oss.platform.core.service.DepartmentService;
import com.oss.platform.core.service.UserService;
import com.oss.platform.security.OssUserDetails;
import com.oss.platform.security.OssUserPrincipal;
import com.oss.platform.security.TokenAuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by W540 on 2/28/2017.
 */
@Controller
public class AuthenticateController {
    Environment env;
    UserService userService;
    private TokenAuthenticationService tokenAuthenticationService;
    private DepartmentService departmentService;

    @Autowired
    public AuthenticateController(Environment env, UserService userService, TokenAuthenticationService tokenAuthenticationService, DepartmentService departmentService) {
        this.env = env;
        this.userService = userService;
        this.tokenAuthenticationService = tokenAuthenticationService;
        this.departmentService = departmentService;
    }




    @RequestMapping(value = "/accesstoken", method = RequestMethod.GET)
    void ssoAuthentication(@RequestParam String token, @RequestParam String username, HttpServletRequest request, HttpServletResponse response){
        String ssoServer = env.getProperty("sso.server.url");
        String appToken = env.getProperty("sso.redirect2app");
        String responstring = null;
        try {
            responstring = sendAuthority(token,username,ssoServer);
            System.out.println(responstring);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (responstring.equals("OK")){
            UserDTO userDTO = null;
            OssUserDetails userDetails = null;
            try {
                userDTO = userService.getUserByUserNameAndDomain(username,request.getServerName());
                userDetails = new OssUserDetails(userDTO);
            } catch (NotFoundException e) {
                Integer countUserCompany = userService.countUserCompany(username);
                try {
                    userDTO = userService.getUserByUserName(username);
                } catch (NotFoundException e1) {
                    e1.printStackTrace();
                }
                userDetails = new OssUserDetails(userDTO);
                if(countUserCompany >1){
                    userDetails.requestSelectedCompany();
                }
            }
            tokenAuthenticationService.redirectWithToken(response,userDetails.getDetails(),appToken,request.getServerName());
        }
    }
    @RequestMapping(value = "/api/changeusercompany", method = RequestMethod.POST)
    ResponseEntity changeUserCompany(OssUserDetails ossUserDetails, @RequestParam Integer companyId){
        OssUserPrincipal userPrincipal = ossUserDetails.getDetails();
        if(userService.isUserOfCompany(userPrincipal.getUsername(),companyId)){
            userPrincipal.setCompanyId(companyId);
            DepartmentDTO departmentDTO = departmentService.findUserDepartment(ossUserDetails.getUsername(),companyId);
            userPrincipal.setNeedSelectCompany(false);
            userPrincipal.setDepartmentId(departmentDTO.getId());
        }
        String newToken = tokenAuthenticationService.getToken(userPrincipal);
        return ResponseEntity.ok(newToken);
    }
//    @RequestMapping("")
//    public void checkToken(@RequestParam String token, String userName){
//        //
//        // send request with token params token,username
//        // to http://oss.seedcom.vn/sso/no_auth/checktoken
//        // callbak OK
//
//    }
    private String sendAuthority(String token,String username,String ssoServer) throws Exception {

        String url = ssoServer;
        String query = String.format("?%s=%s&%s=%s",
                URLEncoder.encode("token", "UTF-8"),URLEncoder.encode(token,"UTF-8"),
                URLEncoder.encode("username", "UTF-8"),URLEncoder.encode(username,"UTF-8"));

        URL obj = new URL(url+query);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("POST");

        //add request header
        con.setRequestProperty("User-Agent", "Mozilla/5.0");

        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'GET' request to URL : " + url);
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        return response.toString();

    }

}
