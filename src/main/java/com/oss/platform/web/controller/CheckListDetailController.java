package com.oss.platform.web.controller;

import com.oss.platform.checklist.dto.CL_DetailInDto;
import com.oss.platform.checklist.dto.CL_ItempInDto;
import com.oss.platform.checklist.service.CheckListDetailService;
import com.oss.platform.common.Constant;
import com.oss.platform.common.ErrorMessage;
import com.oss.platform.common.ResultMessage;
import com.oss.platform.security.OssUserDetails;
import com.oss.platform.security.OssUserPrincipal;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by haocpq on 6/15/2017.
 */

@RestController
public class CheckListDetailController {


    Logger logger = Logger.getLogger(CheckListDetailController.class);

    @Autowired
    CheckListDetailService checkListDetailService;

    @RequestMapping(value = "/api/{modun}/detail", method = {RequestMethod.POST})
    public ResponseEntity createdetail (OssUserDetails authentication, @PathVariable("modun") String modun, @RequestBody CL_DetailInDto detail){

        OssUserPrincipal principal = authentication.getDetails();
        Integer companyid = principal.getCompanyId();
        String username = principal.getUsername();

        ResultMessage resultMessage = new ResultMessage();
        ErrorMessage errorMessage = new ErrorMessage();

        try {
            Object result =  checkListDetailService.saveDetail(modun, detail, username, companyid);
            resultMessage.setStatus(Constant.SUCCESS);
            resultMessage.setRecords(result);

            return ResponseEntity.ok(resultMessage);
        } catch (Exception ex){
            logger.error("[POST] /checklist/detail"+ ex);

            resultMessage.setStatus(Constant.ERROR);
            errorMessage.setErrorCode("not.found");
            errorMessage.setErrorMessage("Lỗi hệ thống");
            resultMessage.setRecords(errorMessage);

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(resultMessage);
        }
    }

    @RequestMapping(value = "/api/{modun}/detail/{promotion}", method = {RequestMethod.GET})
    public ResponseEntity getdetail (OssUserDetails authentication, @PathVariable("modun") String modun, @PathVariable("promotion") String promotion){

        OssUserPrincipal principal = authentication.getDetails();
        Integer companyid = principal.getCompanyId();
        String username = principal.getUsername();

        ResultMessage resultMessage = new ResultMessage();
        ErrorMessage errorMessage = new ErrorMessage();

        try {
            Object result =  checkListDetailService.getDetail(modun, promotion, username, companyid);
            resultMessage.setStatus(Constant.SUCCESS);
            resultMessage.setRecords(result);

            return ResponseEntity.ok(resultMessage);
        } catch (Exception ex){
            logger.error("[GET] /checklist/detail"+ ex);

            resultMessage.setStatus(Constant.ERROR);
            errorMessage.setErrorCode("not.found");
            errorMessage.setErrorMessage("Lỗi hệ thống");
            resultMessage.setRecords(errorMessage);

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(resultMessage);
        }
    }

    @RequestMapping(value = "/api/{modun}/detail/{promotiondetail}", method = {RequestMethod.PUT})
    public ResponseEntity editdetail (OssUserDetails authentication, @PathVariable("modun") String modun,
                                      @PathVariable("promotiondetail") String promotiondetail, @RequestBody CL_ItempInDto detail){

        OssUserPrincipal principal = authentication.getDetails();
        Integer companyid = principal.getCompanyId();
        String username = principal.getUsername();

        ResultMessage resultMessage = new ResultMessage();
        ErrorMessage errorMessage = new ErrorMessage();

        try {
            Object result =  checkListDetailService.editDetail(modun, promotiondetail, detail, username, companyid);
            resultMessage.setStatus(Constant.SUCCESS);
            resultMessage.setRecords(result);

            return ResponseEntity.ok(resultMessage);
        } catch (Exception ex){
            logger.error("[GET] /checklist/detail"+ ex);

            resultMessage.setStatus(Constant.ERROR);
            errorMessage.setErrorCode("not.found");
            errorMessage.setErrorMessage("Lỗi hệ thống");
            resultMessage.setRecords(errorMessage);

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(resultMessage);
        }
    }


}
