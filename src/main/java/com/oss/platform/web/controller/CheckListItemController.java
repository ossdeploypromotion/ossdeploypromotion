package com.oss.platform.web.controller;

import com.oss.platform.checklist.dto.CL_ItemDto;
import com.oss.platform.checklist.dto.CL_ItempInDto;
import com.oss.platform.checklist.service.CheckListItemService;
import com.oss.platform.common.Constant;
import com.oss.platform.common.ErrorMessage;
import com.oss.platform.common.ResultMessage;
import com.oss.platform.security.OssUserDetails;
import com.oss.platform.security.OssUserPrincipal;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by haocpq on 6/7/2017.
 */

@RestController
public class CheckListItemController {

    Logger logger = Logger.getLogger(CheckListItemController.class);

    @Autowired
    CheckListItemService checklistItemService;

   // @Autowired
   // CheckListTemplateService checkListTemplateService;

   // @Autowired
  //  CheckListDetailService checkListDetailService;

    @RequestMapping(value = "/api/{modun}/itemp",method = {RequestMethod.GET})
    ResponseEntity list_CL_Item(OssUserDetails authentication, @PathVariable("modun") String modun){
        OssUserPrincipal principal = authentication.getDetails();
        Integer companyid = principal.getCompanyId();
        String username = principal.getUsername();

        ResultMessage resultMessage = new ResultMessage();
        ErrorMessage errorMessage = new ErrorMessage();

        try {
            List<CL_ItemDto> listItemp = checklistItemService.findWithModun(modun,companyid);

            resultMessage.setStatus(Constant.SUCCESS);
            resultMessage.setRecords(listItemp);

            return ResponseEntity.ok(resultMessage);
        } catch (Exception ex){
            logger.error("[GET] /checklist/itemp"+ ex);

            resultMessage.setStatus(Constant.ERROR);
            errorMessage.setErrorCode("not.found");
            errorMessage.setErrorMessage("Lỗi hệ thống");
            resultMessage.setRecords(errorMessage);

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(resultMessage);
        }
    }

    @RequestMapping(value = "/api/{modun}/itemp", method = {RequestMethod.POST})
    public ResponseEntity insert_CL_itemp(OssUserDetails authentication, @PathVariable("modun") String modun, @RequestBody CL_ItempInDto itemp){

        OssUserPrincipal principal = authentication.getDetails();
        Integer companyid = principal.getCompanyId();
        String username = principal.getUsername();

        ResultMessage resultMessage = new ResultMessage();
        ErrorMessage errorMessage = new ErrorMessage();

        try {
            CL_ItemDto item = checklistItemService.save(itemp, modun, username, companyid);

            if(item == null){
                resultMessage.setStatus(Constant.ERROR);
                errorMessage.setErrorCode("checklist.item.not.found");
                errorMessage.setErrorMessage("Không tìm thấy item");
                resultMessage.setRecords(errorMessage);
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(resultMessage);
            } else {
                resultMessage.setStatus(Constant.SUCCESS);
                resultMessage.setRecords(item);
                return ResponseEntity.ok(resultMessage);
            }
        } catch (Exception ex){
            logger.error("[GET] /checklist/itemp"+ ex);

            resultMessage.setStatus(Constant.ERROR);
            errorMessage.setErrorCode("not.found");
            errorMessage.setErrorMessage("Lỗi hệ thống");
            resultMessage.setRecords(errorMessage);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(resultMessage);
        }
    }


}
