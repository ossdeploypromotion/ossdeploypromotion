package com.oss.platform.web.controller;

import com.oss.platform.checklist.dto.CL_TemplateDto;
import com.oss.platform.checklist.dto.CL_TemplateInDto;
import com.oss.platform.checklist.service.CheckListTemplateService;
import com.oss.platform.common.Constant;
import com.oss.platform.common.ErrorMessage;
import com.oss.platform.common.ResultMessage;
import com.oss.platform.security.OssUserDetails;
import com.oss.platform.security.OssUserPrincipal;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by haocpq on 6/15/2017.
 */

@RestController
public class CheckListTemplateController {

    @Autowired
    CheckListTemplateService checkListTemplateService;

    Logger logger = Logger.getLogger(CheckListTemplateController.class);

    @RequestMapping(value = "/api/{modun}/template", method = {RequestMethod.POST})
    public ResponseEntity createTemplate(OssUserDetails authentication, @PathVariable("modun") String modun, @RequestBody CL_TemplateInDto template){
        OssUserPrincipal principal = authentication.getDetails();
        Integer companyid = principal.getCompanyId();
        String username = principal.getUsername();

        ResultMessage resultMessage = new ResultMessage();
        ErrorMessage errorMessage = new ErrorMessage();

        try {
            CL_TemplateDto cl_templateDto = checkListTemplateService.save(template, modun, username,companyid);

            if(cl_templateDto == null){
                resultMessage.setStatus(Constant.ERROR);
                errorMessage.setErrorCode("checklist.template.not.found");
                errorMessage.setErrorMessage("Không tìm thấy template");
                resultMessage.setRecords(errorMessage);
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(resultMessage);
            } else {
                resultMessage.setStatus(Constant.SUCCESS);
                resultMessage.setRecords(cl_templateDto);
                return ResponseEntity.ok(resultMessage);
            }
        } catch (Exception ex){
            logger.error("[POST] /checklist/TEMPLATE"+ ex);

            resultMessage.setStatus(Constant.ERROR);
            errorMessage.setErrorCode("not.found");
            errorMessage.setErrorMessage("Lỗi hệ thống");
            resultMessage.setRecords(errorMessage);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(resultMessage);
        }
    }

    @RequestMapping(value = "/api/{modun}/template", method = {RequestMethod.GET})
    public ResponseEntity getTemplate(OssUserDetails authentication, @PathVariable("modun") String modun){
        OssUserPrincipal principal = authentication.getDetails();
        Integer companyid = principal.getCompanyId();
        String username = principal.getUsername();

        ResultMessage resultMessage = new ResultMessage();
        ErrorMessage errorMessage = new ErrorMessage();

        try {
            List<CL_TemplateDto> cl_templateDto = checkListTemplateService.getList(modun, companyid);
            resultMessage.setStatus(Constant.SUCCESS);
            resultMessage.setRecords(cl_templateDto);
            return ResponseEntity.ok(resultMessage);
        } catch (Exception ex){
            logger.error("[GET] /checklist/TEMPLATE"+ ex);

            resultMessage.setStatus(Constant.ERROR);
            errorMessage.setErrorCode("not.found");
            errorMessage.setErrorMessage("Lỗi hệ thống");
            resultMessage.setRecords(errorMessage);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(resultMessage);
        }
    }

}
