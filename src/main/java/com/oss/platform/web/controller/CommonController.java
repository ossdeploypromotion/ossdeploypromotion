package com.oss.platform.web.controller;

import com.oss.platform.core.dto.SearchEmployeeDTO;
import com.oss.platform.core.service.CommonService;
import com.oss.platform.security.OssUserDetails;
import com.oss.platform.security.OssUserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by W540 on 4/14/2017.
 */
@RestController
public class CommonController {

    CommonService commonService;

    @Autowired
    public CommonController(CommonService commonService) {
        this.commonService = commonService;
    }

    @RequestMapping(value = "/api/search/employee")
    ResponseEntity searchEmployee(OssUserDetails user, @RequestParam String keyword) {
        OssUserPrincipal principal = user.getDetails();
        List<SearchEmployeeDTO> listResult = commonService.searchEmployeeForHeaderBar(principal.getUsername(), principal.getCompanyId(), keyword);
        return ResponseEntity.ok(listResult);
    }
}
