package com.oss.platform.web.controller;

import com.oss.platform.security.OssUserDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.oss.platform.common.Constant;
import com.oss.platform.common.ErrorMessage;
import com.oss.platform.common.ResultMessage;
import com.oss.platform.core.data.CompanyEntity;
import com.oss.platform.core.dto.CompanyDTO;
import com.oss.platform.core.dto.UserDTO;
import com.oss.platform.core.exception.NotFoundException;
import com.oss.platform.core.service.CompanyService;

/**
 * @author Ballard
 */
@RestController
final class CompanyController {
	private static final Logger LOGGER = LoggerFactory.getLogger(CompanyController.class);
	CompanyService companyService;

	@Autowired
	public CompanyController(CompanyService companyService) {
		this.companyService = companyService;
	}

	@RequestMapping(value = "/api/company/getDetail", method = RequestMethod.GET)
	public ResponseEntity getDetail(OssUserDetails authentication) throws NotFoundException {
		ResultMessage message = new ResultMessage();
		ErrorMessage error = new ErrorMessage();
		CompanyDTO company = companyService.getDetail(authentication.getDetails().getCompanyId());
		if (company != null) {
			message.setRecords(company);
			message.setStatus(Constant.SUCCESS);
			return ResponseEntity.ok(message);
		} else {
			message.setStatus(Constant.ERROR);
			error.setErrorCode("not.found");
			error.setErrorMessage("Lỗi hệ thống");
			message.setRecords(error);

			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message);
		}
	}
	@RequestMapping(value = "/api/company/getDetailFull", method = RequestMethod.GET)
	public ResponseEntity getDetailFull(OssUserDetails authentication) throws NotFoundException {
		ResultMessage message = new ResultMessage();
		ErrorMessage error = new ErrorMessage();
		CompanyEntity company = companyService.getDetailFull(authentication.getDetails().getCompanyId());
		if (company != null) {
			message.setRecords(company);
			message.setStatus(Constant.SUCCESS);
			return ResponseEntity.ok(message);
		} else {
			message.setStatus(Constant.ERROR);
			error.setErrorCode("not.found");
			error.setErrorMessage("Lỗi hệ thống");
			message.setRecords(error);

			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message);
		}
	}
}
