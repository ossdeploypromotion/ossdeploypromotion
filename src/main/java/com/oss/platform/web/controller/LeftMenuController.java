package com.oss.platform.web.controller;

import com.oss.platform.core.dto.ChildMenuDTO;
import com.oss.platform.core.dto.ParentMenuDTO;
import com.oss.platform.core.service.LeftMenuService;
import com.oss.platform.security.OssUserDetails;
import com.oss.platform.security.OssUserPrincipal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * This controller provides the public API that is used to return the information
 * of group.
 *
 * @author Vũ Đỗ
 */
@RestController
final class LeftMenuController {
    private static final Logger LOGGER = LoggerFactory.getLogger(LeftMenuController.class);
    LeftMenuService leftMenuService;
    private static final Integer DEFAULT_LEFTMENU_TYPE = 1;

    @Autowired
    public LeftMenuController(LeftMenuService leftMenuService) {
        this.leftMenuService = leftMenuService;
    }

    /**
     *
     * @param request
     * @param authentication
     * @param leftmenutype = 1 is leftmenu only, = 2 is top left menu, default is 1
     * @return
     */
    @RequestMapping(value = "/api/okr/leftmenu", method = RequestMethod.GET)
    public ResponseEntity getLeftMenu(HttpServletRequest request,
                                      OssUserDetails authentication,
                                       @RequestParam(required = false) Integer leftmenutype) {
        LOGGER.info("Getting leftmenu.");
        OssUserPrincipal principal = authentication.getDetails();
        leftmenutype = (leftmenutype == null) ? DEFAULT_LEFTMENU_TYPE : leftmenutype;
        if (leftmenutype.equals(DEFAULT_LEFTMENU_TYPE)) {
            List<ParentMenuDTO> result = leftMenuService.getLeftMenu(principal.getUsername(),principal.getCompanyId(), leftmenutype, request);
            return ResponseEntity.ok(result);
        } else {
            List<ChildMenuDTO> result = leftMenuService.getTopLeftMenu(principal.getUsername(),principal.getCompanyId(), leftmenutype, request);
            return ResponseEntity.ok(result);
        }
    }
}
