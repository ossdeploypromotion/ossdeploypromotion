package com.oss.platform.web.controller;

import com.oss.platform.common.Constant;
import com.oss.platform.common.ErrorMessage;
import com.oss.platform.common.ResultMessage;
import com.oss.platform.modun.dto.ModunDto;
import com.oss.platform.modun.dto.ModunInDto;
import com.oss.platform.modun.service.ModunService;
import com.oss.platform.security.OssUserDetails;
import com.oss.platform.security.OssUserPrincipal;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by haocpq on 6/8/2017.
 */

@RestController
public class ModunController {

    Logger logger = Logger.getLogger(ModunController.class);


    @Autowired
    ModunService modunService;

    @RequestMapping(value="/api/modun", method = {RequestMethod.GET})
    public ResponseEntity listModun(OssUserDetails authentication, Pageable pageable){
        OssUserPrincipal principal = authentication.getDetails();
        Integer companyid = principal.getCompanyId();
        String username = principal.getUsername();

        ResultMessage resultMessage = new ResultMessage();
        ErrorMessage errorMessage = new ErrorMessage();

        try {
            List<ModunDto> listModunDto = modunService.findByCompanyid(pageable, companyid);

            resultMessage.setStatus(Constant.SUCCESS);
            resultMessage.setRecords(listModunDto);
            return ResponseEntity.ok(resultMessage);

        } catch (Exception ex){
            logger.error("[GET] /modun"+ ex);

            resultMessage.setStatus(Constant.ERROR);
            errorMessage.setErrorCode("not.found");
            errorMessage.setErrorMessage("Lỗi hệ thống");
            resultMessage.setRecords(errorMessage);

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(resultMessage);
        }
    }

    @RequestMapping(value = "/api/modun", method={RequestMethod.POST})
    public ResponseEntity insertModun(OssUserDetails authentication, @RequestBody ModunInDto modun){
        OssUserPrincipal principal = authentication.getDetails();
        Integer companyid = principal.getCompanyId();
        String username = principal.getUsername();

        ResultMessage resultMessage = new ResultMessage();
        ErrorMessage errorMessage = new ErrorMessage();
        try {
            ModunDto modunDto = modunService.save(modun, username, companyid);
            resultMessage.setStatus(Constant.SUCCESS);
            resultMessage.setRecords(modunDto);

            return ResponseEntity.ok(resultMessage);
        } catch (Exception ex){

            logger.error("[CREATE] /modun " + ex);
            resultMessage.setStatus(Constant.ERROR);
            errorMessage.setErrorCode("not.found");
            errorMessage.setErrorMessage("Lỗi hệ thống");
            resultMessage.setRecords(errorMessage);

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(resultMessage);
        }
    }

    @RequestMapping(value="/api/modun/{id}", method = {RequestMethod.DELETE})
    public ResponseEntity deleteModun(OssUserDetails authentication, @PathVariable("id") String modunid){
        OssUserPrincipal principal = authentication.getDetails();
        Integer companyid = principal.getCompanyId();
        String username = principal.getUsername();

        ResultMessage resultMessage = new ResultMessage();
        ErrorMessage errorMessage = new ErrorMessage();
        try {

            modunService.delete(modunid);
            resultMessage.setStatus(Constant.SUCCESS);
            return ResponseEntity.ok(resultMessage);

        } catch (Exception ex){
            logger.error("[DELETE] /modun/"+ modunid + " " + ex);
            resultMessage.setStatus(Constant.ERROR);
            errorMessage.setErrorCode("not.found");
            errorMessage.setErrorMessage("Lỗi hệ thống");
            resultMessage.setRecords(errorMessage);

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(resultMessage);
        }
    }
}
