package com.oss.platform.web.controller;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.oss.platform.common.Constant;
import com.oss.platform.common.ErrorMessage;
import com.oss.platform.common.ResultMessage;
import com.oss.platform.promotions.indto.PromotionConfigInputDTO;
import com.oss.platform.promotions.outdto.PromotionConfigOutputDTO;
import com.oss.platform.promotions.service.PromotionConfigService;


@RestController
public class PromotionController {

	@Autowired
	private PromotionConfigService promotionConfigServ;

	// @RequestMapping(value="api/promotionimpl/getPromotionImpeInfor",method=RequestMethod.GET)
	// public PromotionImpleInforInputDTO getPromotionInfoAPI(String url,String
	// id) throws JsonProcessingException, IOException {
	// url+="?id="+id;
	// RestTemplate restTemplate = new RestTemplate();
	//
	// ObjectMapper mapper = new ObjectMapper();
	// HttpHeaders header = new HttpHeaders();
	// MediaType mediaType = new MediaType("application", "json",
	// StandardCharsets.UTF_8);
	// header.setContentType(mediaType);
	// header.set("Authorization","key=" +
	// "AAAA_VZGauk:APA91bE1JHbkSVyFzM0zAokTH742A10iP9MLY-Su-bnwyMWHHzC3bZcDbnRpq7ArR6rsqNfBCo5P8tLDNnFFDyyibzePqkUYR9xlT6SkHesgvBKRKG4A90rsMNlDS9GB7RUbNvZudCtp");
	//
	// String body =
	// "{\"to\":\"dQkX694Jv5I:APA91bHPZeUEmX0CkOMVlVAjYXCCvL8W64CwnWRUlV05rfoS8HQl51vJ7-BUPguYqXHYIuTRx78yU6GF6h_xE-xGbohHyLyURZl98TczUncdryGUfkRcOLdAGgsNoDVSPVzRPHbGWOSZ\""
	// +",\"data\":{"
	// + "\"id\":\"2813\","
	// + "\"title\":\"Cu Phi Quang
	// Haohttps://techtalk.vn/tat-tan-tat-ve-api.html\","
	// + "\"body\":\"test\""
	// +"},"
	// +"\"notification\":{"
	// +"\"sound\":0"
	// +"}"
	//
	// + "}";
	//
	// HttpEntity<String> entity = new HttpEntity<>(body,header);
	//
	// String promotionInforJSON =
	// restTemplate.postForEntity("https://fcm.googleapis.com/fcm/send", entity,
	// String.class).getBody();
	//
	// PromotionImpleInforInputDTO promoImpleInfInputDTO =
	// mapper.readValue(promotionInforJSON, PromotionImpleInforInputDTO.class);
	// return promoImpleInfInputDTO;
	// }
	//
	@RequestMapping(value = "api/promotionconfig/getPromotionInfor", method = RequestMethod.GET)
	public ResponseEntity getPromotionInforAPI(@RequestParam(value = "promotionId") String promotionId,ModelMap modelMap)
			throws JsonProcessingException, IOException {
		PromotionConfigOutputDTO promotionConfigOutputDTO = new PromotionConfigOutputDTO();
		String url = "";
		// Config request
		RestTemplate restTemplate = new RestTemplate();
		MediaType mediaType = new MediaType("application", "json", StandardCharsets.UTF_8);
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(mediaType);
		httpHeaders.set("Authorization", "");

		String body = "{}";
		ResultMessage message = new ResultMessage();
		ErrorMessage error = new ErrorMessage();

		try {
			HttpEntity<String> httpEntity = new HttpEntity<String>("", httpHeaders);

			String promotionJSON = restTemplate.getForEntity(url, String.class, httpEntity).getBody();

			// Convert from JSON to PromotionCOnfigInutDto
			ObjectMapper mapper = new ObjectMapper();
			JsonNode root = mapper.readTree(promotionJSON);
			String moduleCode = root.path("").asText();
			List<String> listShopIds = new ArrayList<String>();

			Iterator<JsonNode> shopIdItr = root.path("").elements();
			while (shopIdItr.hasNext()) {
				String shopId = shopIdItr.next().asText();
				listShopIds.add(shopId);
			}

			PromotionConfigInputDTO promotionConfigInputDTO = new PromotionConfigInputDTO();
			promotionConfigInputDTO.setModuleCode(moduleCode);
			promotionConfigInputDTO.setPromotionShopIds(listShopIds);

			if (promotionConfigServ.getPromotionConfig(promotionConfigInputDTO.getModuleCode()) == null) {
				promotionConfigOutputDTO = promotionConfigServ.savePromotionConfig(promotionConfigInputDTO);
			} else {
				promotionConfigOutputDTO = promotionConfigServ
						.getPromotionConfig(promotionConfigInputDTO.getModuleCode());
			}


			modelMap.addAttribute("promotionConfigInput", new PromotionConfigInputDTO());
			message.setStatus(Constant.SUCCESS);
			message.setRecords(promotionConfigOutputDTO);
			return ResponseEntity.ok(message);

		} catch (Exception e) {
			message.setStatus(Constant.ERROR);
			error.setErrorCode("not.found");
			error.setErrorMessage("Lỗi hệ thống");
			message.setRecords(error);

			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message);
		}
	}
	
	/**
	 * update promotion config
	 * @param promotionConfigInputDTO
	 * @return
	 */
	@RequestMapping(value="api/promotionconfig",method=RequestMethod.PUT)
	public ResponseEntity updatePromotionConfig(@ModelAttribute(value="promotionConfigInput") PromotionConfigInputDTO promotionConfigInputDTO) {
		ResultMessage message = new ResultMessage();
		ErrorMessage error = new ErrorMessage();
		PromotionConfigOutputDTO promotionConfigOutputDTO = new PromotionConfigOutputDTO();
		try {
			promotionConfigOutputDTO = promotionConfigServ.savePromotionConfig(promotionConfigInputDTO);
			message.setStatus(Constant.SUCCESS);
			message.setRecords(promotionConfigInputDTO);
			return ResponseEntity.ok(message);
		} catch (Exception e) {
			message.setStatus(Constant.ERROR);
			error.setErrorCode("not.found");
			error.setErrorMessage("Lỗi hệ thống");
			message.setRecords(error);

			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message);
		}

	}
}
