package com.oss.platform.web.controller;

import com.oss.platform.core.dto.CompanyDTO;
import com.oss.platform.core.dto.UserDTO;
import com.oss.platform.core.service.DepartmentService;
import com.oss.platform.core.service.UserService;
import com.oss.platform.security.OssUserDetails;
import com.oss.platform.security.OssUserPrincipal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This controller provides the public API that is used to return the information
 * of the authenticated user.
 *
 * @author Công Thành
 */
@RestController
final class UserController {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
    UserService userService;
    DepartmentService departmentService;

    @Autowired
    public UserController(UserService userService,DepartmentService departmentService) {
        this.userService = userService;
        this.departmentService = departmentService;
    }

    /**
     *
     * @param authentication will be injected by spring security
     * @return
     */
    @RequestMapping(value = "/api/authenticated-user", method = RequestMethod.GET)
    public ResponseEntity<Map<String,Object>> getAuthenticatedUser(OssUserDetails authentication) {
        LOGGER.info("Getting authenticated user.");
        Map<String,Object> userInfoResult = new HashMap<>();
        if (authentication == null) {
            //If anonymous users can access this controller method, someone has changed
            //the security configuration and it must be fixed.
            LOGGER.error("Authenticated user is not found.");
            throw new AccessDeniedException("Anonymous users cannot request the information of the authenticated user.");
        }
        else {
            LOGGER.info("UserEntity with username: {} is authenticated", authentication.getPrincipal());
            UserDTO user = null;
            OssUserPrincipal principal = authentication.getDetails();
            user = userService.getUserByPinciple(authentication.getDetails());
            List<Integer> manageDepartment = departmentService.getDeparmentBeingManage(principal.getUsername(),principal.getCompanyId());
            userInfoResult.put("manageDepartment",manageDepartment);
            userInfoResult.put("user",user);
            //TODO: find company id for current user
            userInfoResult.put("userInfo",user);
            if (authentication.getDetails().getNeedSelectCompany()){
                user.setNeedSlectCompany(true);
            }
            return ResponseEntity.ok(userInfoResult);
        }
    }

	@RequestMapping(value = "/api/users/searchByUserName", method = RequestMethod.GET)
	public ResponseEntity searchByUserName(OssUserDetails authentication, String keyword) {
		List<UserDTO> users = userService.searchUsersByUserName(keyword,authentication.getUsername(),authentication.getDetails().getCompanyId());
		return ResponseEntity.ok(users);
	}

    @RequestMapping(value = "/api/users/listcompany",method = RequestMethod.GET)
    public ResponseEntity getListCompany(OssUserDetails authentication){
        List<CompanyDTO> listCompany = userService.getListCompany(authentication.getUsername());
        return ResponseEntity.ok(listCompany);
    }

}
